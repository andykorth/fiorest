﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class RemoveGroupPermissionsAndAuth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PermissionGroupMemberships");

            migrationBuilder.DropTable(
                name: "PermissionGroupUserEntries");

            migrationBuilder.DropTable(
                name: "PermissionGroups");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PermissionGroupMemberships",
                columns: table => new
                {
                    PermissionGroupMembershipId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AuthenticationModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    PermissionGroupName = table.Column<string>(type: "TEXT", nullable: true),
                    State = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionGroupMemberships", x => x.PermissionGroupMembershipId);
                    table.ForeignKey(
                        name: "FK_PermissionGroupMemberships_AuthenticationModels_AuthenticationModelId",
                        column: x => x.AuthenticationModelId,
                        principalTable: "AuthenticationModels",
                        principalColumn: "AuthenticationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PermissionGroups",
                columns: table => new
                {
                    PermissionGroupId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BuildingData = table.Column<bool>(type: "INTEGER", nullable: false),
                    ContractData = table.Column<bool>(type: "INTEGER", nullable: false),
                    ExpertsData = table.Column<bool>(type: "INTEGER", nullable: false),
                    FlightData = table.Column<bool>(type: "INTEGER", nullable: false),
                    OwnerUserName = table.Column<string>(type: "TEXT", nullable: true),
                    PermissionGroupName = table.Column<string>(type: "TEXT", nullable: true),
                    ProductionData = table.Column<bool>(type: "INTEGER", nullable: false),
                    ShipmentTrackingData = table.Column<bool>(type: "INTEGER", nullable: false),
                    StorageData = table.Column<bool>(type: "INTEGER", nullable: false),
                    WorkforceData = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionGroups", x => x.PermissionGroupId);
                });

            migrationBuilder.CreateTable(
                name: "PermissionGroupUserEntries",
                columns: table => new
                {
                    PermissionGroupUserEntryId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PermissionGroupId = table.Column<int>(type: "INTEGER", nullable: false),
                    UserName = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionGroupUserEntries", x => x.PermissionGroupUserEntryId);
                    table.ForeignKey(
                        name: "FK_PermissionGroupUserEntries_PermissionGroups_PermissionGroupId",
                        column: x => x.PermissionGroupId,
                        principalTable: "PermissionGroups",
                        principalColumn: "PermissionGroupId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PermissionGroupMemberships_AuthenticationModelId",
                table: "PermissionGroupMemberships",
                column: "AuthenticationModelId");

            migrationBuilder.CreateIndex(
                name: "IX_PermissionGroupUserEntries_PermissionGroupId",
                table: "PermissionGroupUserEntries",
                column: "PermissionGroupId");
        }
    }
}
