﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddAPIKeySupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "APIKeys",
                columns: table => new
                {
                    APIKeyId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AuthAPIKey = table.Column<Guid>(type: "TEXT", nullable: false),
                    Application = table.Column<string>(type: "TEXT", nullable: true),
                    LastAccessTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    AuthenticationModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_APIKeys", x => x.APIKeyId);
                    table.ForeignKey(
                        name: "FK_APIKeys_AuthenticationModels_AuthenticationModelId",
                        column: x => x.AuthenticationModelId,
                        principalTable: "AuthenticationModels",
                        principalColumn: "AuthenticationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_APIKeys_AuthenticationModelId",
                table: "APIKeys",
                column: "AuthenticationModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "APIKeys");
        }
    }
}
