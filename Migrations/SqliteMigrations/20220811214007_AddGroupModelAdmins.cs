﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddGroupModelAdmins : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupModelAdmin_GroupModels_GroupModelId",
                table: "GroupModelAdmin");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupModelAdmin",
                table: "GroupModelAdmin");

            migrationBuilder.RenameTable(
                name: "GroupModelAdmin",
                newName: "GroupModelAdmins");

            migrationBuilder.RenameIndex(
                name: "IX_GroupModelAdmin_GroupModelId",
                table: "GroupModelAdmins",
                newName: "IX_GroupModelAdmins_GroupModelId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupModelAdmins",
                table: "GroupModelAdmins",
                column: "GroupModelAdminId");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupModelAdmins_GroupModels_GroupModelId",
                table: "GroupModelAdmins",
                column: "GroupModelId",
                principalTable: "GroupModels",
                principalColumn: "GroupModelId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupModelAdmins_GroupModels_GroupModelId",
                table: "GroupModelAdmins");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupModelAdmins",
                table: "GroupModelAdmins");

            migrationBuilder.RenameTable(
                name: "GroupModelAdmins",
                newName: "GroupModelAdmin");

            migrationBuilder.RenameIndex(
                name: "IX_GroupModelAdmins_GroupModelId",
                table: "GroupModelAdmin",
                newName: "IX_GroupModelAdmin_GroupModelId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupModelAdmin",
                table: "GroupModelAdmin",
                column: "GroupModelAdminId");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupModelAdmin_GroupModels_GroupModelId",
                table: "GroupModelAdmin",
                column: "GroupModelId",
                principalTable: "GroupModels",
                principalColumn: "GroupModelId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
