﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddFirstStatsModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PriceIndexModels",
                columns: table => new
                {
                    PriceIndexModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TimeStamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    PriceIndexLabel = table.Column<string>(type: "TEXT", nullable: true),
                    PriceIndexValue = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceIndexModels", x => x.PriceIndexModelId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PriceIndexModels");
        }
    }
}
