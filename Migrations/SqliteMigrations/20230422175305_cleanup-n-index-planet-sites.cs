﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class cleanupnindexplanetsites : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM \"PlanetSites\" WHERE rowid NOT IN ( SELECT MIN(rowid) FROM \"PlanetSites\" GROUP BY \"PlanetId\", \"PlotId\" );");

            migrationBuilder.CreateIndex(
                name: "IX_PlanetSites_PlanetId_PlotId",
                table: "PlanetSites",
                columns: new[] { "PlanetId", "PlotId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PlanetSites_PlanetId_PlotId",
                table: "PlanetSites");
        }
    }
}
