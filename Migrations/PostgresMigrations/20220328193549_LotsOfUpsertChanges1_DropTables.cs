﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class LotsOfUpsertChanges1_DropTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BUIBuildingCosts");

            migrationBuilder.DropTable(
                name: "BuildingDegradationReclaimableMaterialModels");

            migrationBuilder.DropTable(
                name: "BuildingDegradationRepairMaterialModels");

            migrationBuilder.DropTable(
                name: "BUIRecipeInputs");

            migrationBuilder.DropTable(
                name: "BUIRecipeOutputs");

            migrationBuilder.DropTable(
                name: "BuyingAds");

            migrationBuilder.DropTable(
                name: "ComexExchanges");

            migrationBuilder.DropTable(
                name: "CompanyDataCurrencyBalances");

            migrationBuilder.DropTable(
                name: "ContractDependencies");

            migrationBuilder.DropTable(
                name: "CountryRegistryCountries");

            migrationBuilder.DropTable(
                name: "CXBuyOrders");

            migrationBuilder.DropTable(
                name: "CXOSTrades");

            migrationBuilder.DropTable(
                name: "CXPCDataEntries");

            migrationBuilder.DropTable(
                name: "CXSellOrders");

            migrationBuilder.DropTable(
                name: "ExpertModels");

            migrationBuilder.DropTable(
                name: "FLIGHTSDestinationLines");

            migrationBuilder.DropTable(
                name: "FLIGHTSOriginLines");

            migrationBuilder.DropTable(
                name: "FXDataModels");

            migrationBuilder.DropTable(
                name: "InfrastructureModelReports");

            migrationBuilder.DropTable(
                name: "InfrastructureProjectContributions");

            migrationBuilder.DropTable(
                name: "InfrastructureProjectUpgradeCosts");

            migrationBuilder.DropTable(
                name: "InfrastructureProjectUpkeeps");

            migrationBuilder.DropTable(
                name: "JumpCacheRoutes");

            migrationBuilder.DropTable(
                name: "MATModels");

            migrationBuilder.DropTable(
                name: "ProductionLineInputs");

            migrationBuilder.DropTable(
                name: "ProductionLineOutputs");

            migrationBuilder.DropTable(
                name: "SellingAds");

            migrationBuilder.DropTable(
                name: "ShippingAds");

            migrationBuilder.DropTable(
                name: "SHIPSRepairMaterial");

            migrationBuilder.DropTable(
                name: "SimulationData");

            migrationBuilder.DropTable(
                name: "SITESReclaimableMaterials");

            migrationBuilder.DropTable(
                name: "SITESRepairMaterials");

            migrationBuilder.DropTable(
                name: "Stations");

            migrationBuilder.DropTable(
                name: "StorageItems");

            migrationBuilder.DropTable(
                name: "SubSectorVertices");

            migrationBuilder.DropTable(
                name: "SystemConnections");

            migrationBuilder.DropTable(
                name: "SystemStars");

            migrationBuilder.DropTable(
                name: "UserDataModels");

            migrationBuilder.DropTable(
                name: "UserSettingsBurnRateExclusions");

            migrationBuilder.DropTable(
                name: "WarehouseModels");

            migrationBuilder.DropTable(
                name: "WorkforceNeeds");

            migrationBuilder.DropTable(
                name: "WorkforcePerOneHundredNeeds");

            migrationBuilder.DropTable(
                name: "BuildingDegradationModels");

            migrationBuilder.DropTable(
                name: "BUIRecipes");

            migrationBuilder.DropTable(
                name: "CompanyDataModels");

            migrationBuilder.DropTable(
                name: "ContractConditions");

            migrationBuilder.DropTable(
                name: "CXOSTradeOrders");

            migrationBuilder.DropTable(
                name: "CXPCData");

            migrationBuilder.DropTable(
                name: "CXDataModels");

            migrationBuilder.DropTable(
                name: "FLIGHTSFlightSegments");

            migrationBuilder.DropTable(
                name: "InfrastructureInfos");

            migrationBuilder.DropTable(
                name: "JumpCacheModels");

            migrationBuilder.DropTable(
                name: "ProductionLineOrders");

            migrationBuilder.DropTable(
                name: "LocalMarketModels");

            migrationBuilder.DropTable(
                name: "SHIPSShips");

            migrationBuilder.DropTable(
                name: "SITESBuildings");

            migrationBuilder.DropTable(
                name: "StorageModels");

            migrationBuilder.DropTable(
                name: "SubSectors");

            migrationBuilder.DropTable(
                name: "SystemStarsModels");

            migrationBuilder.DropTable(
                name: "UserSettingsBurnRates");

            migrationBuilder.DropTable(
                name: "WorkforceDescriptions");

            migrationBuilder.DropTable(
                name: "WorkforcePerOneHundreds");

            migrationBuilder.DropTable(
                name: "BUIModels");

            migrationBuilder.DropTable(
                name: "ContractModels");

            migrationBuilder.DropTable(
                name: "CXOSTradeOrderModels");

            migrationBuilder.DropTable(
                name: "FLIGHTSFlights");

            migrationBuilder.DropTable(
                name: "InfrastructureModels");

            migrationBuilder.DropTable(
                name: "ProductionLines");

            migrationBuilder.DropTable(
                name: "SHIPSModels");

            migrationBuilder.DropTable(
                name: "SITESSites");

            migrationBuilder.DropTable(
                name: "WorldSectorsModels");

            migrationBuilder.DropTable(
                name: "UserSettingsModels");

            migrationBuilder.DropTable(
                name: "WorkforceModels");

            migrationBuilder.DropTable(
                name: "FLIGHTSModels");

            migrationBuilder.DropTable(
                name: "PRODLinesModels");

            migrationBuilder.DropTable(
                name: "SITESModels");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BuildingDegradationModels",
                columns: table => new
                {
                    BuildingDegradationModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BuildingCreated = table.Column<long>(type: "bigint", nullable: false),
                    BuildingTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Condition = table.Column<double>(type: "double precision", nullable: false),
                    DaysSinceLastRepair = table.Column<double>(type: "double precision", nullable: false),
                    HasBeenRepaired = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationModels", x => x.BuildingDegradationModelId);
                });

            migrationBuilder.CreateTable(
                name: "BUIModels",
                columns: table => new
                {
                    BUIModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AreaCost = table.Column<int>(type: "integer", nullable: false),
                    Engineers = table.Column<int>(type: "integer", nullable: false),
                    Expertise = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    Pioneers = table.Column<int>(type: "integer", nullable: false),
                    Scientists = table.Column<int>(type: "integer", nullable: false),
                    Settlers = table.Column<int>(type: "integer", nullable: false),
                    Technicians = table.Column<int>(type: "integer", nullable: false),
                    Ticker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIModels", x => x.BUIModelId);
                });

            migrationBuilder.CreateTable(
                name: "ComexExchanges",
                columns: table => new
                {
                    ComexExchangeId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CurrencyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CurrencyDecimals = table.Column<int>(type: "integer", nullable: false),
                    CurrencyName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CurrencyNumericCode = table.Column<int>(type: "integer", nullable: false),
                    ExchangeCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ExchangeId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ExchangeName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    ExchangeOperatorCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ExchangeOperatorId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ExchangeOperatorName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    LocationId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    LocationName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    LocationNaturalId = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComexExchanges", x => x.ComexExchangeId);
                });

            migrationBuilder.CreateTable(
                name: "CompanyDataModels",
                columns: table => new
                {
                    CompanyDataModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ActivityRating = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CompanyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CompanyId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CompanyName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    CountryId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CreatedEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    CurrencyCode = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    HighestTier = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    OverallRating = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Pioneer = table.Column<bool>(type: "boolean", nullable: false),
                    ReliabilityRating = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    StabilityRating = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    StartingLocation = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    StartingProfile = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Team = table.Column<bool>(type: "boolean", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyDataModels", x => x.CompanyDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "ContractModels",
                columns: table => new
                {
                    ContractModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CanExtend = table.Column<bool>(type: "boolean", nullable: false),
                    ContractId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ContractLocalId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    DateEpochMs = table.Column<long>(type: "bigint", nullable: true),
                    DueDateEpochMs = table.Column<long>(type: "bigint", nullable: true),
                    ExtensionDeadlineEpochMs = table.Column<long>(type: "bigint", nullable: true),
                    PartnerCompanyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    PartnerId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PartnerName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    Party = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Status = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractModels", x => x.ContractModelId);
                });

            migrationBuilder.CreateTable(
                name: "CountryRegistryCountries",
                columns: table => new
                {
                    CountryRegistryCountryId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CountryCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CountryId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CountryName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    CurrencyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CurrencyDecimals = table.Column<int>(type: "integer", nullable: false),
                    CurrencyName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CurrencyNumericCode = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountryRegistryCountries", x => x.CountryRegistryCountryId);
                });

            migrationBuilder.CreateTable(
                name: "CXDataModels",
                columns: table => new
                {
                    CXDataModelId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    AllTimeHigh = table.Column<double>(type: "double precision", nullable: true),
                    AllTimeLow = table.Column<double>(type: "double precision", nullable: true),
                    Ask = table.Column<double>(type: "double precision", nullable: true),
                    AskCount = table.Column<int>(type: "integer", nullable: true),
                    Bid = table.Column<double>(type: "double precision", nullable: true),
                    BidCount = table.Column<int>(type: "integer", nullable: true),
                    Currency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Demand = table.Column<int>(type: "integer", nullable: true),
                    ExchangeCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ExchangeName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    High = table.Column<double>(type: "double precision", nullable: true),
                    Low = table.Column<double>(type: "double precision", nullable: true),
                    MMBuy = table.Column<double>(type: "double precision", nullable: true),
                    MMSell = table.Column<double>(type: "double precision", nullable: true),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    NarrowPriceBandHigh = table.Column<double>(type: "double precision", nullable: true),
                    NarrowPriceBandLow = table.Column<double>(type: "double precision", nullable: true),
                    Previous = table.Column<double>(type: "double precision", nullable: true),
                    Price = table.Column<double>(type: "double precision", nullable: true),
                    PriceAverage = table.Column<double>(type: "double precision", nullable: true),
                    PriceTimeEpochMs = table.Column<long>(type: "bigint", nullable: true),
                    Supply = table.Column<int>(type: "integer", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Traded = table.Column<int>(type: "integer", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    VolumeAmount = table.Column<double>(type: "double precision", nullable: true),
                    WidePriceBandHigh = table.Column<double>(type: "double precision", nullable: true),
                    WidePriceBandLow = table.Column<double>(type: "double precision", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXDataModels", x => x.CXDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "CXOSTradeOrderModels",
                columns: table => new
                {
                    CXOSTradeOrdersModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXOSTradeOrderModels", x => x.CXOSTradeOrdersModelId);
                });

            migrationBuilder.CreateTable(
                name: "CXPCData",
                columns: table => new
                {
                    CXPCDataId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EndDataEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    ExchangeCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    StartDataEpochMs = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXPCData", x => x.CXPCDataId);
                });

            migrationBuilder.CreateTable(
                name: "ExpertModels",
                columns: table => new
                {
                    ExpertsModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AgricultureActive = table.Column<int>(type: "integer", nullable: false),
                    AgricultureAvailable = table.Column<int>(type: "integer", nullable: false),
                    AgricultureEfficiencyGain = table.Column<double>(type: "double precision", nullable: false),
                    ChemistryActive = table.Column<int>(type: "integer", nullable: false),
                    ChemistryAvailable = table.Column<int>(type: "integer", nullable: false),
                    ChemistryEfficiencyGain = table.Column<double>(type: "double precision", nullable: false),
                    ConstructionActive = table.Column<int>(type: "integer", nullable: false),
                    ConstructionAvailable = table.Column<int>(type: "integer", nullable: false),
                    ConstructionEfficiencyGain = table.Column<double>(type: "double precision", nullable: false),
                    ElectronicsActive = table.Column<int>(type: "integer", nullable: false),
                    ElectronicsAvailable = table.Column<int>(type: "integer", nullable: false),
                    ElectronicsEfficiencyGain = table.Column<double>(type: "double precision", nullable: false),
                    FoodIndustriesActive = table.Column<int>(type: "integer", nullable: false),
                    FoodIndustriesAvailable = table.Column<int>(type: "integer", nullable: false),
                    FoodIndustriesEfficiencyGain = table.Column<double>(type: "double precision", nullable: false),
                    FuelRefiningActive = table.Column<int>(type: "integer", nullable: false),
                    FuelRefiningAvailable = table.Column<int>(type: "integer", nullable: false),
                    FuelRefiningEfficiencyGain = table.Column<double>(type: "double precision", nullable: false),
                    ManufacturingActive = table.Column<int>(type: "integer", nullable: false),
                    ManufacturingAvailable = table.Column<int>(type: "integer", nullable: false),
                    ManufacturingEfficiencyGain = table.Column<double>(type: "double precision", nullable: false),
                    MetallurgyActive = table.Column<int>(type: "integer", nullable: false),
                    MetallurgyAvailable = table.Column<int>(type: "integer", nullable: false),
                    MetallurgyEfficiencyGain = table.Column<double>(type: "double precision", nullable: false),
                    PlanetId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ResourceExtractionAvailable = table.Column<int>(type: "integer", nullable: false),
                    ResourceExtractionEfficiencyGain = table.Column<double>(type: "double precision", nullable: false),
                    ResourceExtractioneActive = table.Column<int>(type: "integer", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExpertModels", x => x.ExpertsModelId);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSModels",
                columns: table => new
                {
                    FLIGHTSModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSModels", x => x.FLIGHTSModelId);
                });

            migrationBuilder.CreateTable(
                name: "FXDataModels",
                columns: table => new
                {
                    FXDataModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BaseCurrencyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    BaseCurrencyName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    BaseCurrencyNumericCode = table.Column<int>(type: "integer", nullable: false),
                    BrokerId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    High = table.Column<decimal>(type: "numeric", nullable: false),
                    Low = table.Column<decimal>(type: "numeric", nullable: false),
                    Open = table.Column<decimal>(type: "numeric", nullable: false),
                    Previous = table.Column<decimal>(type: "numeric", nullable: false),
                    PriceUpdateEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    QuoteCurrencyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    QuoteCurrencyName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    QuoteCurrencyNumericCode = table.Column<int>(type: "integer", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Traded = table.Column<decimal>(type: "numeric", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Volume = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FXDataModels", x => x.FXDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureModels",
                columns: table => new
                {
                    InfrastructureModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PopulationId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureModels", x => x.InfrastructureModelId);
                });

            migrationBuilder.CreateTable(
                name: "JumpCacheModels",
                columns: table => new
                {
                    JumpCacheModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DestinationNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    DestinationSystemId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    DestinationSystemName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    JumpCount = table.Column<int>(type: "integer", nullable: false),
                    OverallDistance = table.Column<double>(type: "double precision", nullable: false),
                    SourceSystemId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SourceSystemName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SourceSystemNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JumpCacheModels", x => x.JumpCacheModelId);
                });

            migrationBuilder.CreateTable(
                name: "LocalMarketModels",
                columns: table => new
                {
                    LocalMarketModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MarketId = table.Column<string>(type: "text", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalMarketModels", x => x.LocalMarketModelId);
                });

            migrationBuilder.CreateTable(
                name: "MATModels",
                columns: table => new
                {
                    MATModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CategoryId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CategoryName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MatId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    Ticker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Volume = table.Column<double>(type: "double precision", nullable: false),
                    Weight = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MATModels", x => x.MATModelId);
                });

            migrationBuilder.CreateTable(
                name: "PRODLinesModels",
                columns: table => new
                {
                    PRODLinesModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SiteId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PRODLinesModels", x => x.PRODLinesModelId);
                });

            migrationBuilder.CreateTable(
                name: "SHIPSModels",
                columns: table => new
                {
                    SHIPSModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SHIPSModels", x => x.SHIPSModelId);
                });

            migrationBuilder.CreateTable(
                name: "SimulationData",
                columns: table => new
                {
                    SimulationDataId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FlightFTLFactor = table.Column<int>(type: "integer", nullable: false),
                    FlightSTLFactor = table.Column<int>(type: "integer", nullable: false),
                    ParsecLength = table.Column<int>(type: "integer", nullable: false),
                    PlanetaryMotionFactor = table.Column<int>(type: "integer", nullable: false),
                    SimulationInterval = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SimulationData", x => x.SimulationDataId);
                });

            migrationBuilder.CreateTable(
                name: "SITESModels",
                columns: table => new
                {
                    SITESModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESModels", x => x.SITESModelId);
                });

            migrationBuilder.CreateTable(
                name: "Stations",
                columns: table => new
                {
                    StationId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ComexCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ComexId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ComexName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    CommisionTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    CountryCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CountryId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CountryName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    CurrencyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CurrencyDecimals = table.Column<int>(type: "integer", nullable: false),
                    CurrencyName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CurrencyNumericCode = table.Column<int>(type: "integer", nullable: false),
                    GovernorCorporationCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    GovernorCorporationId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    GovernorCorporationName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    GovernorId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    GovernorUserName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    NaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    SystemId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SystemName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SystemNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    WarehouseId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stations", x => x.StationId);
                });

            migrationBuilder.CreateTable(
                name: "StorageModels",
                columns: table => new
                {
                    StorageModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AddressableId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    FixedStore = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    StorageId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Type = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    VolumeCapacity = table.Column<double>(type: "double precision", nullable: false),
                    VolumeLoad = table.Column<double>(type: "double precision", nullable: false),
                    WeightCapacity = table.Column<double>(type: "double precision", nullable: false),
                    WeightLoad = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StorageModels", x => x.StorageModelId);
                });

            migrationBuilder.CreateTable(
                name: "SystemStars",
                columns: table => new
                {
                    SystemStarId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Luminosity = table.Column<double>(type: "double precision", nullable: false),
                    Mass = table.Column<double>(type: "double precision", nullable: false),
                    MassSol = table.Column<double>(type: "double precision", nullable: false),
                    PositionX = table.Column<double>(type: "double precision", nullable: false),
                    PositionY = table.Column<double>(type: "double precision", nullable: false),
                    PositionZ = table.Column<double>(type: "double precision", nullable: false),
                    SectorId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SubSectorId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SystemId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SystemName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SystemNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Type = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemStars", x => x.SystemStarId);
                });

            migrationBuilder.CreateTable(
                name: "SystemStarsModels",
                columns: table => new
                {
                    SystemStarsModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    NaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    PositionX = table.Column<double>(type: "double precision", nullable: false),
                    PositionY = table.Column<double>(type: "double precision", nullable: false),
                    PositionZ = table.Column<double>(type: "double precision", nullable: false),
                    SectorId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SubSectorId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SystemId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Type = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemStarsModels", x => x.SystemStarsModelId);
                });

            migrationBuilder.CreateTable(
                name: "UserDataModels",
                columns: table => new
                {
                    UserDataModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompanyId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CreatedEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    IsModeratorChat = table.Column<bool>(type: "boolean", nullable: false),
                    IsPayingUser = table.Column<bool>(type: "boolean", nullable: false),
                    Pioneer = table.Column<bool>(type: "boolean", nullable: false),
                    PlanetNamingRights = table.Column<int>(type: "integer", nullable: false),
                    SystemNamingRights = table.Column<int>(type: "integer", nullable: false),
                    Team = table.Column<bool>(type: "boolean", nullable: false),
                    Tier = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDataModels", x => x.UserDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "UserSettingsModels",
                columns: table => new
                {
                    UserSettingsModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettingsModels", x => x.UserSettingsModelId);
                });

            migrationBuilder.CreateTable(
                name: "WarehouseModels",
                columns: table => new
                {
                    WarehouseModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FeeAmount = table.Column<double>(type: "double precision", nullable: false),
                    FeeCollectorCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    FeeCollectorId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    FeeCollectorName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    FeeCurrency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    LocationName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    LocationNaturalId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    NextPaymentTimestampEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    StoreId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Units = table.Column<int>(type: "integer", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    VolumeCapacity = table.Column<double>(type: "double precision", nullable: false),
                    WarehouseId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    WeightCapacity = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WarehouseModels", x => x.WarehouseModelId);
                });

            migrationBuilder.CreateTable(
                name: "WorkforceModels",
                columns: table => new
                {
                    WorkforceModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LastWorkforceUpdateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    PlanetId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    SiteId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforceModels", x => x.WorkforceModelId);
                });

            migrationBuilder.CreateTable(
                name: "WorkforcePerOneHundreds",
                columns: table => new
                {
                    WorkforcePerOneHundredId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    WorkforceType = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforcePerOneHundreds", x => x.WorkforcePerOneHundredId);
                });

            migrationBuilder.CreateTable(
                name: "WorldSectorsModels",
                columns: table => new
                {
                    WorldSectorsModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    HexQ = table.Column<int>(type: "integer", nullable: false),
                    HexR = table.Column<int>(type: "integer", nullable: false),
                    HexS = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SectorId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Size = table.Column<int>(type: "integer", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorldSectorsModels", x => x.WorldSectorsModelId);
                });

            migrationBuilder.CreateTable(
                name: "BuildingDegradationReclaimableMaterialModels",
                columns: table => new
                {
                    BuildingDegradationReclaimableMaterialModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BuildingDegradationModelId = table.Column<int>(type: "integer", nullable: false),
                    MaterialCount = table.Column<int>(type: "integer", nullable: false),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationReclaimableMaterialModels", x => x.BuildingDegradationReclaimableMaterialModelId);
                    table.ForeignKey(
                        name: "FK_BuildingDegradationReclaimableMaterialModels_BuildingDegrad~",
                        column: x => x.BuildingDegradationModelId,
                        principalTable: "BuildingDegradationModels",
                        principalColumn: "BuildingDegradationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingDegradationRepairMaterialModels",
                columns: table => new
                {
                    BuildingDegradationRepairMaterialModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BuildingDegradationModelId = table.Column<int>(type: "integer", nullable: false),
                    MaterialCount = table.Column<int>(type: "integer", nullable: false),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationRepairMaterialModels", x => x.BuildingDegradationRepairMaterialModelId);
                    table.ForeignKey(
                        name: "FK_BuildingDegradationRepairMaterialModels_BuildingDegradation~",
                        column: x => x.BuildingDegradationModelId,
                        principalTable: "BuildingDegradationModels",
                        principalColumn: "BuildingDegradationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIBuildingCosts",
                columns: table => new
                {
                    BUIBuildingCostId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    BUIModelId = table.Column<int>(type: "integer", nullable: false),
                    CommodityName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Volume = table.Column<double>(type: "double precision", nullable: false),
                    Weight = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIBuildingCosts", x => x.BUIBuildingCostId);
                    table.ForeignKey(
                        name: "FK_BUIBuildingCosts_BUIModels_BUIModelId",
                        column: x => x.BUIModelId,
                        principalTable: "BUIModels",
                        principalColumn: "BUIModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIRecipes",
                columns: table => new
                {
                    BUIRecipeId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BUIModelId = table.Column<int>(type: "integer", nullable: false),
                    DurationMs = table.Column<int>(type: "integer", nullable: false),
                    RecipeName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIRecipes", x => x.BUIRecipeId);
                    table.ForeignKey(
                        name: "FK_BUIRecipes_BUIModels_BUIModelId",
                        column: x => x.BUIModelId,
                        principalTable: "BUIModels",
                        principalColumn: "BUIModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanyDataCurrencyBalances",
                columns: table => new
                {
                    CompanyDataCurrencyBalanceId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Balance = table.Column<double>(type: "double precision", nullable: false),
                    CompanyDataModelId = table.Column<int>(type: "integer", nullable: false),
                    Currency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyDataCurrencyBalances", x => x.CompanyDataCurrencyBalanceId);
                    table.ForeignKey(
                        name: "FK_CompanyDataCurrencyBalances_CompanyDataModels_CompanyDataMo~",
                        column: x => x.CompanyDataModelId,
                        principalTable: "CompanyDataModels",
                        principalColumn: "CompanyDataModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContractConditions",
                columns: table => new
                {
                    ContractConditionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Address = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Amount = table.Column<double>(type: "double precision", nullable: true),
                    BlockId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ConditionId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ConditionIndex = table.Column<int>(type: "integer", nullable: false),
                    ContractModelId = table.Column<int>(type: "integer", nullable: false),
                    Currency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    DeadlineEpochMs = table.Column<long>(type: "bigint", nullable: true),
                    Destination = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialAmount = table.Column<int>(type: "integer", nullable: true),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Party = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PickedUpAmount = table.Column<int>(type: "integer", nullable: true),
                    PickedUpMaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PickedUpMaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ShipmentItemId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Status = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    Type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Volume = table.Column<double>(type: "double precision", nullable: true),
                    Weight = table.Column<double>(type: "double precision", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractConditions", x => x.ContractConditionId);
                    table.ForeignKey(
                        name: "FK_ContractConditions_ContractModels_ContractModelId",
                        column: x => x.ContractModelId,
                        principalTable: "ContractModels",
                        principalColumn: "ContractModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CXBuyOrders",
                columns: table => new
                {
                    OrderId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    CXDataModelId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CompanyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CompanyId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CompanyName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    ItemCost = table.Column<double>(type: "double precision", nullable: false),
                    ItemCount = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXBuyOrders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_CXBuyOrders_CXDataModels_CXDataModelId",
                        column: x => x.CXDataModelId,
                        principalTable: "CXDataModels",
                        principalColumn: "CXDataModelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CXSellOrders",
                columns: table => new
                {
                    OrderId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    CXDataModelId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CompanyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CompanyId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CompanyName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    ItemCost = table.Column<double>(type: "double precision", nullable: false),
                    ItemCount = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXSellOrders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_CXSellOrders_CXDataModels_CXDataModelId",
                        column: x => x.CXDataModelId,
                        principalTable: "CXDataModels",
                        principalColumn: "CXDataModelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CXOSTradeOrders",
                columns: table => new
                {
                    CXOSTradeOrderId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    BrokerId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CXOSTradeOrdersModelId = table.Column<int>(type: "integer", nullable: false),
                    CreatedEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    ExchangeCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ExchangeName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    InitialAmount = table.Column<int>(type: "integer", nullable: false),
                    Limit = table.Column<double>(type: "double precision", nullable: false),
                    LimitCurrency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    OrderType = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    Status = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    TradeOrderId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXOSTradeOrders", x => x.CXOSTradeOrderId);
                    table.ForeignKey(
                        name: "FK_CXOSTradeOrders_CXOSTradeOrderModels_CXOSTradeOrdersModelId",
                        column: x => x.CXOSTradeOrdersModelId,
                        principalTable: "CXOSTradeOrderModels",
                        principalColumn: "CXOSTradeOrdersModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CXPCDataEntries",
                columns: table => new
                {
                    CXPCDataEntryId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CXPCDataId = table.Column<int>(type: "integer", nullable: false),
                    Close = table.Column<double>(type: "double precision", nullable: false),
                    Open = table.Column<double>(type: "double precision", nullable: false),
                    TimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    Traded = table.Column<int>(type: "integer", nullable: false),
                    Volume = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXPCDataEntries", x => x.CXPCDataEntryId);
                    table.ForeignKey(
                        name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                        column: x => x.CXPCDataId,
                        principalTable: "CXPCData",
                        principalColumn: "CXPCDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSFlights",
                columns: table => new
                {
                    FLIGHTSFlightId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ArrivalTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    CurrentSegmentIndex = table.Column<int>(type: "integer", nullable: false),
                    DepartureTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    Destination = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    FLIGHTSModelId = table.Column<int>(type: "integer", nullable: false),
                    FlightId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    FtlDistance = table.Column<double>(type: "double precision", nullable: false),
                    IsAborted = table.Column<bool>(type: "boolean", nullable: false),
                    Origin = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    ShipId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    StlDistance = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSFlights", x => x.FLIGHTSFlightId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSFlights_FLIGHTSModels_FLIGHTSModelId",
                        column: x => x.FLIGHTSModelId,
                        principalTable: "FLIGHTSModels",
                        principalColumn: "FLIGHTSModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureInfos",
                columns: table => new
                {
                    InfrastructureInfoId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ActiveLevel = table.Column<int>(type: "integer", nullable: false),
                    CurrentLevel = table.Column<int>(type: "integer", nullable: false),
                    InfrastructureModelId = table.Column<int>(type: "integer", nullable: false),
                    Level = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    ProjectId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Ticker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    UpgradeStatus = table.Column<double>(type: "double precision", nullable: false),
                    UpkeepStatus = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureInfos", x => x.InfrastructureInfoId);
                    table.ForeignKey(
                        name: "FK_InfrastructureInfos_InfrastructureModels_InfrastructureMode~",
                        column: x => x.InfrastructureModelId,
                        principalTable: "InfrastructureModels",
                        principalColumn: "InfrastructureModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureModelReports",
                columns: table => new
                {
                    InfrastructureReportId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AverageHappinessEngineer = table.Column<float>(type: "real", nullable: false),
                    AverageHappinessPioneer = table.Column<float>(type: "real", nullable: false),
                    AverageHappinessScientist = table.Column<float>(type: "real", nullable: false),
                    AverageHappinessSettler = table.Column<float>(type: "real", nullable: false),
                    AverageHappinessTechnician = table.Column<float>(type: "real", nullable: false),
                    ExplorersGraceEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    InfrastructureModelId = table.Column<int>(type: "integer", nullable: false),
                    NeedFulfillmentComfort = table.Column<float>(type: "real", nullable: false),
                    NeedFulfillmentCulture = table.Column<float>(type: "real", nullable: false),
                    NeedFulfillmentEducation = table.Column<float>(type: "real", nullable: false),
                    NeedFulfillmentHealth = table.Column<float>(type: "real", nullable: false),
                    NeedFulfillmentLifeSupport = table.Column<float>(type: "real", nullable: false),
                    NeedFulfillmentSafety = table.Column<float>(type: "real", nullable: false),
                    NextPopulationEngineer = table.Column<int>(type: "integer", nullable: false),
                    NextPopulationPioneer = table.Column<int>(type: "integer", nullable: false),
                    NextPopulationScientist = table.Column<int>(type: "integer", nullable: false),
                    NextPopulationSettler = table.Column<int>(type: "integer", nullable: false),
                    NextPopulationTechnician = table.Column<int>(type: "integer", nullable: false),
                    OpenJobsEngineer = table.Column<float>(type: "real", nullable: false),
                    OpenJobsPioneer = table.Column<float>(type: "real", nullable: false),
                    OpenJobsScientist = table.Column<float>(type: "real", nullable: false),
                    OpenJobsSettler = table.Column<float>(type: "real", nullable: false),
                    OpenJobsTechnician = table.Column<float>(type: "real", nullable: false),
                    PopulationDifferenceEngineer = table.Column<int>(type: "integer", nullable: false),
                    PopulationDifferencePioneer = table.Column<int>(type: "integer", nullable: false),
                    PopulationDifferenceScientist = table.Column<int>(type: "integer", nullable: false),
                    PopulationDifferenceSettler = table.Column<int>(type: "integer", nullable: false),
                    PopulationDifferenceTechnician = table.Column<int>(type: "integer", nullable: false),
                    SimulationPeriod = table.Column<int>(type: "integer", nullable: false),
                    TimestampMs = table.Column<long>(type: "bigint", nullable: false),
                    UnemploymentRateEngineer = table.Column<float>(type: "real", nullable: false),
                    UnemploymentRatePioneer = table.Column<float>(type: "real", nullable: false),
                    UnemploymentRateScientist = table.Column<float>(type: "real", nullable: false),
                    UnemploymentRateSettler = table.Column<float>(type: "real", nullable: false),
                    UnemploymentRateTechnician = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureModelReports", x => x.InfrastructureReportId);
                    table.ForeignKey(
                        name: "FK_InfrastructureModelReports_InfrastructureModels_Infrastruct~",
                        column: x => x.InfrastructureModelId,
                        principalTable: "InfrastructureModels",
                        principalColumn: "InfrastructureModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JumpCacheRoutes",
                columns: table => new
                {
                    JumpCacheRouteJumpId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DestinationNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    DestinationSystemId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    DestinationSystemName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Distance = table.Column<double>(type: "double precision", nullable: false),
                    JumpCacheModelId = table.Column<int>(type: "integer", nullable: false),
                    SourceSystemId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SourceSystemName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SourceSystemNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JumpCacheRoutes", x => x.JumpCacheRouteJumpId);
                    table.ForeignKey(
                        name: "FK_JumpCacheRoutes_JumpCacheModels_JumpCacheModelId",
                        column: x => x.JumpCacheModelId,
                        principalTable: "JumpCacheModels",
                        principalColumn: "JumpCacheModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuyingAds",
                columns: table => new
                {
                    BuyingAdId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ContractNaturalId = table.Column<int>(type: "integer", nullable: false),
                    CreationTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    CreatorCompanyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CreatorCompanyId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CreatorCompanyName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    DeliveryTime = table.Column<int>(type: "integer", nullable: false),
                    ExpiryTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    LocalMarketModelId = table.Column<int>(type: "integer", nullable: false),
                    MaterialAmount = table.Column<int>(type: "integer", nullable: false),
                    MaterialCategory = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    MaterialVolume = table.Column<double>(type: "double precision", nullable: false),
                    MaterialWeight = table.Column<double>(type: "double precision", nullable: false),
                    MinimumRating = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    PlanetId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Price = table.Column<double>(type: "double precision", nullable: false),
                    PriceCurrency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuyingAds", x => x.BuyingAdId);
                    table.ForeignKey(
                        name: "FK_BuyingAds_LocalMarketModels_LocalMarketModelId",
                        column: x => x.LocalMarketModelId,
                        principalTable: "LocalMarketModels",
                        principalColumn: "LocalMarketModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SellingAds",
                columns: table => new
                {
                    SellingAdId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ContractNaturalId = table.Column<int>(type: "integer", nullable: false),
                    CreationTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    CreatorCompanyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CreatorCompanyId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CreatorCompanyName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    DeliveryTime = table.Column<int>(type: "integer", nullable: false),
                    ExpiryTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    LocalMarketModelId = table.Column<int>(type: "integer", nullable: false),
                    MaterialAmount = table.Column<int>(type: "integer", nullable: false),
                    MaterialCategory = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    MaterialVolume = table.Column<double>(type: "double precision", nullable: false),
                    MaterialWeight = table.Column<double>(type: "double precision", nullable: false),
                    MinimumRating = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    PlanetId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Price = table.Column<double>(type: "double precision", nullable: false),
                    PriceCurrency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SellingAds", x => x.SellingAdId);
                    table.ForeignKey(
                        name: "FK_SellingAds_LocalMarketModels_LocalMarketModelId",
                        column: x => x.LocalMarketModelId,
                        principalTable: "LocalMarketModels",
                        principalColumn: "LocalMarketModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ShippingAds",
                columns: table => new
                {
                    ShippingAdId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CargoVolume = table.Column<double>(type: "double precision", nullable: false),
                    CargoWeight = table.Column<double>(type: "double precision", nullable: false),
                    ContractNaturalId = table.Column<int>(type: "integer", nullable: false),
                    CreationTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    CreatorCompanyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CreatorCompanyId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CreatorCompanyName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    DeliveryTime = table.Column<int>(type: "integer", nullable: false),
                    DestinationPlanetId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    DestinationPlanetName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    DestinationPlanetNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ExpiryTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    LocalMarketModelId = table.Column<int>(type: "integer", nullable: false),
                    MinimumRating = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    OriginPlanetId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    OriginPlanetName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    OriginPlanetNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    PayoutCurrency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    PayoutPrice = table.Column<double>(type: "double precision", nullable: false),
                    PlanetId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShippingAds", x => x.ShippingAdId);
                    table.ForeignKey(
                        name: "FK_ShippingAds_LocalMarketModels_LocalMarketModelId",
                        column: x => x.LocalMarketModelId,
                        principalTable: "LocalMarketModels",
                        principalColumn: "LocalMarketModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLines",
                columns: table => new
                {
                    ProductionLineId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Capacity = table.Column<int>(type: "integer", nullable: false),
                    Condition = table.Column<double>(type: "double precision", nullable: false),
                    Efficiency = table.Column<double>(type: "double precision", nullable: false),
                    LineId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PRODLinesModelId = table.Column<int>(type: "integer", nullable: false),
                    PlanetId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    SiteId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLines", x => x.ProductionLineId);
                    table.ForeignKey(
                        name: "FK_ProductionLines_PRODLinesModels_PRODLinesModelId",
                        column: x => x.PRODLinesModelId,
                        principalTable: "PRODLinesModels",
                        principalColumn: "PRODLinesModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SHIPSShips",
                columns: table => new
                {
                    SHIPSShipId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Acceleration = table.Column<double>(type: "double precision", nullable: false),
                    BlueprintNaturalId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CommissioningTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    Condition = table.Column<double>(type: "double precision", nullable: false),
                    EmitterPower = table.Column<double>(type: "double precision", nullable: false),
                    FlightId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    FtlFuelStoreId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    LastRepairEpochMs = table.Column<long>(type: "bigint", nullable: true),
                    Location = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    Mass = table.Column<double>(type: "double precision", nullable: false),
                    Name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    OperatingEmptyMass = table.Column<double>(type: "double precision", nullable: false),
                    ReactorPower = table.Column<double>(type: "double precision", nullable: false),
                    Registration = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SHIPSModelId = table.Column<int>(type: "integer", nullable: false),
                    ShipId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    StlFuelFlowRate = table.Column<double>(type: "double precision", nullable: false),
                    StlFuelStoreId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    StoreId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Thrust = table.Column<double>(type: "double precision", nullable: false),
                    Volume = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SHIPSShips", x => x.SHIPSShipId);
                    table.ForeignKey(
                        name: "FK_SHIPSShips_SHIPSModels_SHIPSModelId",
                        column: x => x.SHIPSModelId,
                        principalTable: "SHIPSModels",
                        principalColumn: "SHIPSModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESSites",
                columns: table => new
                {
                    SITESSiteId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    InvestedPermits = table.Column<int>(type: "integer", nullable: false),
                    MaximumPermits = table.Column<int>(type: "integer", nullable: false),
                    PlanetFoundedEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    PlanetId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PlanetIdentifier = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SITESModelId = table.Column<int>(type: "integer", nullable: false),
                    SiteId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESSites", x => x.SITESSiteId);
                    table.ForeignKey(
                        name: "FK_SITESSites_SITESModels_SITESModelId",
                        column: x => x.SITESModelId,
                        principalTable: "SITESModels",
                        principalColumn: "SITESModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StorageItems",
                columns: table => new
                {
                    StorageItemId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MaterialAmount = table.Column<int>(type: "integer", nullable: false),
                    MaterialCategory = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    MaterialValue = table.Column<float>(type: "real", nullable: false),
                    MaterialValueCurrency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    MaterialVolume = table.Column<double>(type: "double precision", nullable: false),
                    MaterialWeight = table.Column<double>(type: "double precision", nullable: false),
                    StorageModelId = table.Column<int>(type: "integer", nullable: false),
                    TotalVolume = table.Column<double>(type: "double precision", nullable: false),
                    TotalWeight = table.Column<double>(type: "double precision", nullable: false),
                    Type = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StorageItems", x => x.StorageItemId);
                    table.ForeignKey(
                        name: "FK_StorageItems_StorageModels_StorageModelId",
                        column: x => x.StorageModelId,
                        principalTable: "StorageModels",
                        principalColumn: "StorageModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SystemConnections",
                columns: table => new
                {
                    SystemConnectionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Connection = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    SystemStarsModelId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemConnections", x => x.SystemConnectionId);
                    table.ForeignKey(
                        name: "FK_SystemConnections_SystemStarsModels_SystemStarsModelId",
                        column: x => x.SystemStarsModelId,
                        principalTable: "SystemStarsModels",
                        principalColumn: "SystemStarsModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserSettingsBurnRates",
                columns: table => new
                {
                    UserSettingsBurnRateId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PlanetNaturalId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    UserSettingsModelId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettingsBurnRates", x => x.UserSettingsBurnRateId);
                    table.ForeignKey(
                        name: "FK_UserSettingsBurnRates_UserSettingsModels_UserSettingsModelId",
                        column: x => x.UserSettingsModelId,
                        principalTable: "UserSettingsModels",
                        principalColumn: "UserSettingsModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkforceDescriptions",
                columns: table => new
                {
                    WorkforceDescriptionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Capacity = table.Column<int>(type: "integer", nullable: false),
                    Population = table.Column<int>(type: "integer", nullable: false),
                    Required = table.Column<int>(type: "integer", nullable: false),
                    Reserve = table.Column<int>(type: "integer", nullable: false),
                    Satisfaction = table.Column<double>(type: "double precision", nullable: false),
                    WorkforceModelId = table.Column<int>(type: "integer", nullable: false),
                    WorkforceTypeName = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforceDescriptions", x => x.WorkforceDescriptionId);
                    table.ForeignKey(
                        name: "FK_WorkforceDescriptions_WorkforceModels_WorkforceModelId",
                        column: x => x.WorkforceModelId,
                        principalTable: "WorkforceModels",
                        principalColumn: "WorkforceModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkforcePerOneHundredNeeds",
                columns: table => new
                {
                    WorkforcePerOneHundreedNeedId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Amount = table.Column<double>(type: "double precision", nullable: false),
                    MaterialCategory = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    WorkforcePerOneHundredId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforcePerOneHundredNeeds", x => x.WorkforcePerOneHundreedNeedId);
                    table.ForeignKey(
                        name: "FK_WorkforcePerOneHundredNeeds_WorkforcePerOneHundreds_Workfor~",
                        column: x => x.WorkforcePerOneHundredId,
                        principalTable: "WorkforcePerOneHundreds",
                        principalColumn: "WorkforcePerOneHundredId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubSectors",
                columns: table => new
                {
                    SubSectorId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SSId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    WorldSectorsModelId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubSectors", x => x.SubSectorId);
                    table.ForeignKey(
                        name: "FK_SubSectors_WorldSectorsModels_WorldSectorsModelId",
                        column: x => x.WorldSectorsModelId,
                        principalTable: "WorldSectorsModels",
                        principalColumn: "WorldSectorsModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIRecipeInputs",
                columns: table => new
                {
                    BUIRecipeInputId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    BUIRecipeId = table.Column<int>(type: "integer", nullable: false),
                    CommodityName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Volume = table.Column<double>(type: "double precision", nullable: false),
                    Weight = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIRecipeInputs", x => x.BUIRecipeInputId);
                    table.ForeignKey(
                        name: "FK_BUIRecipeInputs_BUIRecipes_BUIRecipeId",
                        column: x => x.BUIRecipeId,
                        principalTable: "BUIRecipes",
                        principalColumn: "BUIRecipeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIRecipeOutputs",
                columns: table => new
                {
                    BUIRecipeOutputId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    BUIRecipeId = table.Column<int>(type: "integer", nullable: false),
                    CommodityName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Volume = table.Column<double>(type: "double precision", nullable: false),
                    Weight = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIRecipeOutputs", x => x.BUIRecipeOutputId);
                    table.ForeignKey(
                        name: "FK_BUIRecipeOutputs_BUIRecipes_BUIRecipeId",
                        column: x => x.BUIRecipeId,
                        principalTable: "BUIRecipes",
                        principalColumn: "BUIRecipeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContractDependencies",
                columns: table => new
                {
                    ContractDependencyId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ContractConditionId = table.Column<int>(type: "integer", nullable: false),
                    Dependency = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractDependencies", x => x.ContractDependencyId);
                    table.ForeignKey(
                        name: "FK_ContractDependencies_ContractConditions_ContractConditionId",
                        column: x => x.ContractConditionId,
                        principalTable: "ContractConditions",
                        principalColumn: "ContractConditionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CXOSTrades",
                columns: table => new
                {
                    CXOSTradeId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    CXOSTradeOrderId = table.Column<int>(type: "integer", nullable: false),
                    PartnerCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    PartnerId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    PartnerName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    Price = table.Column<double>(type: "double precision", nullable: false),
                    PriceCurrency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    TradeId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    TradeTimeEpochMs = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXOSTrades", x => x.CXOSTradeId);
                    table.ForeignKey(
                        name: "FK_CXOSTrades_CXOSTradeOrders_CXOSTradeOrderId",
                        column: x => x.CXOSTradeOrderId,
                        principalTable: "CXOSTradeOrders",
                        principalColumn: "CXOSTradeOrderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSFlightSegments",
                columns: table => new
                {
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ArrivalTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    DepartureTimeEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    Destination = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    FLIGHTSFlightId = table.Column<int>(type: "integer", nullable: false),
                    FtlDistance = table.Column<double>(type: "double precision", nullable: true),
                    FtlFuelConsumption = table.Column<double>(type: "double precision", nullable: true),
                    Origin = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    StlDistance = table.Column<double>(type: "double precision", nullable: true),
                    StlFuelConsumption = table.Column<double>(type: "double precision", nullable: true),
                    Type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSFlightSegments", x => x.FLIGHTSFlightSegmentId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSFlightSegments_FLIGHTSFlights_FLIGHTSFlightId",
                        column: x => x.FLIGHTSFlightId,
                        principalTable: "FLIGHTSFlights",
                        principalColumn: "FLIGHTSFlightId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureProjectContributions",
                columns: table => new
                {
                    InfrastructureProjectContributionsId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    CompanyCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    CompanyId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CompanyName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    InfrastructureInfoId = table.Column<int>(type: "integer", nullable: false),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    TimestampEpochMs = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureProjectContributions", x => x.InfrastructureProjectContributionsId);
                    table.ForeignKey(
                        name: "FK_InfrastructureProjectContributions_InfrastructureInfos_Infr~",
                        column: x => x.InfrastructureInfoId,
                        principalTable: "InfrastructureInfos",
                        principalColumn: "InfrastructureInfoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureProjectUpgradeCosts",
                columns: table => new
                {
                    InfrastructureProjectUpgradeCostsId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    CurrentAmount = table.Column<int>(type: "integer", nullable: false),
                    InfrastructureInfoId = table.Column<int>(type: "integer", nullable: false),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureProjectUpgradeCosts", x => x.InfrastructureProjectUpgradeCostsId);
                    table.ForeignKey(
                        name: "FK_InfrastructureProjectUpgradeCosts_InfrastructureInfos_Infra~",
                        column: x => x.InfrastructureInfoId,
                        principalTable: "InfrastructureInfos",
                        principalColumn: "InfrastructureInfoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureProjectUpkeeps",
                columns: table => new
                {
                    InfrastructureProjectUpkeepsId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    CurrentAmount = table.Column<int>(type: "integer", nullable: false),
                    Duration = table.Column<int>(type: "integer", nullable: false),
                    InfrastructureInfoId = table.Column<int>(type: "integer", nullable: false),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    NextTickTimestampEpochMs = table.Column<long>(type: "bigint", nullable: false),
                    StoreCapacity = table.Column<int>(type: "integer", nullable: false),
                    Stored = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureProjectUpkeeps", x => x.InfrastructureProjectUpkeepsId);
                    table.ForeignKey(
                        name: "FK_InfrastructureProjectUpkeeps_InfrastructureInfos_Infrastruc~",
                        column: x => x.InfrastructureInfoId,
                        principalTable: "InfrastructureInfos",
                        principalColumn: "InfrastructureInfoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLineOrders",
                columns: table => new
                {
                    ProductionLineOrderId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompletedPercentage = table.Column<double>(type: "double precision", nullable: true),
                    CompletionEpochMs = table.Column<long>(type: "bigint", nullable: true),
                    CreatedEpochMs = table.Column<long>(type: "bigint", nullable: true),
                    DurationMs = table.Column<long>(type: "bigint", nullable: true),
                    IsHalted = table.Column<bool>(type: "boolean", nullable: false),
                    LastUpdatedEpochMs = table.Column<long>(type: "bigint", nullable: true),
                    ProductionFee = table.Column<double>(type: "double precision", nullable: false),
                    ProductionFeeCollectorCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ProductionFeeCollectorId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ProductionFeeCollectorName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    ProductionFeeCurrency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ProductionId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ProductionLineId = table.Column<int>(type: "integer", nullable: false),
                    Recurring = table.Column<bool>(type: "boolean", nullable: false),
                    StartedEpochMs = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLineOrders", x => x.ProductionLineOrderId);
                    table.ForeignKey(
                        name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                        column: x => x.ProductionLineId,
                        principalTable: "ProductionLines",
                        principalColumn: "ProductionLineId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SHIPSRepairMaterial",
                columns: table => new
                {
                    SHIPSRepairMaterialId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    SHIPSShipId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SHIPSRepairMaterial", x => x.SHIPSRepairMaterialId);
                    table.ForeignKey(
                        name: "FK_SHIPSRepairMaterial_SHIPSShips_SHIPSShipId",
                        column: x => x.SHIPSShipId,
                        principalTable: "SHIPSShips",
                        principalColumn: "SHIPSShipId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESBuildings",
                columns: table => new
                {
                    SITESBuildingId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BuildingCreated = table.Column<long>(type: "bigint", nullable: false),
                    BuildingId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    BuildingLastRepair = table.Column<long>(type: "bigint", nullable: true),
                    BuildingName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    BuildingTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Condition = table.Column<double>(type: "double precision", nullable: false),
                    SITESSiteId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESBuildings", x => x.SITESBuildingId);
                    table.ForeignKey(
                        name: "FK_SITESBuildings_SITESSites_SITESSiteId",
                        column: x => x.SITESSiteId,
                        principalTable: "SITESSites",
                        principalColumn: "SITESSiteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserSettingsBurnRateExclusions",
                columns: table => new
                {
                    UserSettingsBurnRateExclusionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    UserSettingsBurnRateId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettingsBurnRateExclusions", x => x.UserSettingsBurnRateExclusionId);
                    table.ForeignKey(
                        name: "FK_UserSettingsBurnRateExclusions_UserSettingsBurnRates_UserSe~",
                        column: x => x.UserSettingsBurnRateId,
                        principalTable: "UserSettingsBurnRates",
                        principalColumn: "UserSettingsBurnRateId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkforceNeeds",
                columns: table => new
                {
                    WorkforceNeedId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Category = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Essential = table.Column<bool>(type: "boolean", nullable: false),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Satisfaction = table.Column<double>(type: "double precision", nullable: false),
                    UnitsPerInterval = table.Column<double>(type: "double precision", nullable: false),
                    UnitsPerOneHundred = table.Column<double>(type: "double precision", nullable: false),
                    WorkforceDescriptionId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforceNeeds", x => x.WorkforceNeedId);
                    table.ForeignKey(
                        name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                        column: x => x.WorkforceDescriptionId,
                        principalTable: "WorkforceDescriptions",
                        principalColumn: "WorkforceDescriptionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubSectorVertices",
                columns: table => new
                {
                    SubSectorVertexId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SubSectorId = table.Column<int>(type: "integer", nullable: false),
                    X = table.Column<double>(type: "double precision", nullable: false),
                    Y = table.Column<double>(type: "double precision", nullable: false),
                    Z = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubSectorVertices", x => x.SubSectorVertexId);
                    table.ForeignKey(
                        name: "FK_SubSectorVertices_SubSectors_SubSectorId",
                        column: x => x.SubSectorId,
                        principalTable: "SubSectors",
                        principalColumn: "SubSectorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSDestinationLines",
                columns: table => new
                {
                    DestinationLineId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "integer", nullable: false),
                    LineId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    LineName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    LineNaturalId = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    Type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSDestinationLines", x => x.DestinationLineId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSDestinationLines_FLIGHTSFlightSegments_FLIGHTSFlight~",
                        column: x => x.FLIGHTSFlightSegmentId,
                        principalTable: "FLIGHTSFlightSegments",
                        principalColumn: "FLIGHTSFlightSegmentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSOriginLines",
                columns: table => new
                {
                    OriginLineId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "integer", nullable: false),
                    LineId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    LineName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    LineNaturalId = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    Type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSOriginLines", x => x.OriginLineId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSOriginLines_FLIGHTSFlightSegments_FLIGHTSFlightSegme~",
                        column: x => x.FLIGHTSFlightSegmentId,
                        principalTable: "FLIGHTSFlightSegments",
                        principalColumn: "FLIGHTSFlightSegmentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLineInputs",
                columns: table => new
                {
                    ProductionLineInputId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MaterialAmount = table.Column<int>(type: "integer", nullable: false),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ProductionLineOrderId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLineInputs", x => x.ProductionLineInputId);
                    table.ForeignKey(
                        name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrd~",
                        column: x => x.ProductionLineOrderId,
                        principalTable: "ProductionLineOrders",
                        principalColumn: "ProductionLineOrderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLineOutputs",
                columns: table => new
                {
                    ProductionLineOutputId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MaterialAmount = table.Column<int>(type: "integer", nullable: false),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    ProductionLineOrderId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLineOutputs", x => x.ProductionLineOutputId);
                    table.ForeignKey(
                        name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOr~",
                        column: x => x.ProductionLineOrderId,
                        principalTable: "ProductionLineOrders",
                        principalColumn: "ProductionLineOrderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESReclaimableMaterials",
                columns: table => new
                {
                    SITESReclaimableMaterialId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MaterialAmount = table.Column<int>(type: "integer", nullable: false),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    SITESBuildingId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESReclaimableMaterials", x => x.SITESReclaimableMaterialId);
                    table.ForeignKey(
                        name: "FK_SITESReclaimableMaterials_SITESBuildings_SITESBuildingId",
                        column: x => x.SITESBuildingId,
                        principalTable: "SITESBuildings",
                        principalColumn: "SITESBuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESRepairMaterials",
                columns: table => new
                {
                    SITESRepairMaterialId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MaterialAmount = table.Column<int>(type: "integer", nullable: false),
                    MaterialId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    SITESBuildingId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESRepairMaterials", x => x.SITESRepairMaterialId);
                    table.ForeignKey(
                        name: "FK_SITESRepairMaterials_SITESBuildings_SITESBuildingId",
                        column: x => x.SITESBuildingId,
                        principalTable: "SITESBuildings",
                        principalColumn: "SITESBuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BUIBuildingCosts_BUIModelId",
                table: "BUIBuildingCosts",
                column: "BUIModelId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationModels_BuildingTicker_DaysSinceLastRepai~",
                table: "BuildingDegradationModels",
                columns: new[] { "BuildingTicker", "DaysSinceLastRepair", "HasBeenRepaired" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationReclaimableMaterialModels_BuildingDegrad~",
                table: "BuildingDegradationReclaimableMaterialModels",
                columns: new[] { "BuildingDegradationModelId", "MaterialTicker" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationRepairMaterialModels_BuildingDegradation~",
                table: "BuildingDegradationRepairMaterialModels",
                columns: new[] { "BuildingDegradationModelId", "MaterialTicker" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BUIRecipeInputs_BUIRecipeId",
                table: "BUIRecipeInputs",
                column: "BUIRecipeId");

            migrationBuilder.CreateIndex(
                name: "IX_BUIRecipeOutputs_BUIRecipeId",
                table: "BUIRecipeOutputs",
                column: "BUIRecipeId");

            migrationBuilder.CreateIndex(
                name: "IX_BUIRecipes_BUIModelId",
                table: "BUIRecipes",
                column: "BUIModelId");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_LocalMarketModelId",
                table: "BuyingAds",
                column: "LocalMarketModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDataCurrencyBalances_CompanyDataModelId",
                table: "CompanyDataCurrencyBalances",
                column: "CompanyDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDataModels_CompanyId",
                table: "CompanyDataModels",
                column: "CompanyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDataModels_UserName",
                table: "CompanyDataModels",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_ContractConditions_ContractModelId",
                table: "ContractConditions",
                column: "ContractModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ContractDependencies_ContractConditionId",
                table: "ContractDependencies",
                column: "ContractConditionId");

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_CXDataModelId",
                table: "CXBuyOrders",
                column: "CXDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_OrderId",
                table: "CXBuyOrders",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_OrderId_CXDataModelId",
                table: "CXBuyOrders",
                columns: new[] { "OrderId", "CXDataModelId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_ExchangeCode",
                table: "CXDataModels",
                column: "ExchangeCode");

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_ExchangeCode_MaterialId",
                table: "CXDataModels",
                columns: new[] { "ExchangeCode", "MaterialId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_MaterialId",
                table: "CXDataModels",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_MaterialName",
                table: "CXDataModels",
                column: "MaterialName");

            migrationBuilder.CreateIndex(
                name: "IX_CXDataModels_MaterialTicker",
                table: "CXDataModels",
                column: "MaterialTicker");

            migrationBuilder.CreateIndex(
                name: "IX_CXOSTradeOrders_CXOSTradeOrdersModelId",
                table: "CXOSTradeOrders",
                column: "CXOSTradeOrdersModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXOSTrades_CXOSTradeOrderId",
                table: "CXOSTrades",
                column: "CXOSTradeOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_CXPCDataEntries_CXPCDataId",
                table: "CXPCDataEntries",
                column: "CXPCDataId");

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_CXDataModelId",
                table: "CXSellOrders",
                column: "CXDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_OrderId",
                table: "CXSellOrders",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_OrderId_CXDataModelId",
                table: "CXSellOrders",
                columns: new[] { "OrderId", "CXDataModelId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSDestinationLines_FLIGHTSFlightSegmentId",
                table: "FLIGHTSDestinationLines",
                column: "FLIGHTSFlightSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSFlights_FLIGHTSModelId",
                table: "FLIGHTSFlights",
                column: "FLIGHTSModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSFlightSegments_FLIGHTSFlightId",
                table: "FLIGHTSFlightSegments",
                column: "FLIGHTSFlightId");

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSOriginLines_FLIGHTSFlightSegmentId",
                table: "FLIGHTSOriginLines",
                column: "FLIGHTSFlightSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureInfos_InfrastructureModelId",
                table: "InfrastructureInfos",
                column: "InfrastructureModelId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureModelReports_InfrastructureModelId",
                table: "InfrastructureModelReports",
                column: "InfrastructureModelId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectContributions_InfrastructureInfoId",
                table: "InfrastructureProjectContributions",
                column: "InfrastructureInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectUpgradeCosts_InfrastructureInfoId",
                table: "InfrastructureProjectUpgradeCosts",
                column: "InfrastructureInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectUpkeeps_InfrastructureInfoId",
                table: "InfrastructureProjectUpkeeps",
                column: "InfrastructureInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCacheRoutes_JumpCacheModelId",
                table: "JumpCacheRoutes",
                column: "JumpCacheModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLineInputs_ProductionLineOrderId",
                table: "ProductionLineInputs",
                column: "ProductionLineOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLineOrders_ProductionLineId",
                table: "ProductionLineOrders",
                column: "ProductionLineId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLineOutputs_ProductionLineOrderId",
                table: "ProductionLineOutputs",
                column: "ProductionLineOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_PRODLinesModelId",
                table: "ProductionLines",
                column: "PRODLinesModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_LocalMarketModelId",
                table: "SellingAds",
                column: "LocalMarketModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_LocalMarketModelId",
                table: "ShippingAds",
                column: "LocalMarketModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SHIPSRepairMaterial_SHIPSShipId",
                table: "SHIPSRepairMaterial",
                column: "SHIPSShipId");

            migrationBuilder.CreateIndex(
                name: "IX_SHIPSShips_SHIPSModelId",
                table: "SHIPSShips",
                column: "SHIPSModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESBuildings_SITESSiteId",
                table: "SITESBuildings",
                column: "SITESSiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESReclaimableMaterials_SITESBuildingId",
                table: "SITESReclaimableMaterials",
                column: "SITESBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESRepairMaterials_SITESBuildingId",
                table: "SITESRepairMaterials",
                column: "SITESBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESSites_SITESModelId",
                table: "SITESSites",
                column: "SITESModelId");

            migrationBuilder.CreateIndex(
                name: "IX_StorageItems_StorageModelId",
                table: "StorageItems",
                column: "StorageModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SubSectors_WorldSectorsModelId",
                table: "SubSectors",
                column: "WorldSectorsModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SubSectorVertices_SubSectorId",
                table: "SubSectorVertices",
                column: "SubSectorId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemConnections_SystemStarsModelId",
                table: "SystemConnections",
                column: "SystemStarsModelId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettingsBurnRateExclusions_UserSettingsBurnRateId",
                table: "UserSettingsBurnRateExclusions",
                column: "UserSettingsBurnRateId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettingsBurnRates_UserSettingsModelId",
                table: "UserSettingsBurnRates",
                column: "UserSettingsModelId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkforceDescriptions_WorkforceModelId",
                table: "WorkforceDescriptions",
                column: "WorkforceModelId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkforceNeeds_WorkforceDescriptionId",
                table: "WorkforceNeeds",
                column: "WorkforceDescriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkforcePerOneHundredNeeds_WorkforcePerOneHundredId",
                table: "WorkforcePerOneHundredNeeds",
                column: "WorkforcePerOneHundredId");
        }
    }
}
