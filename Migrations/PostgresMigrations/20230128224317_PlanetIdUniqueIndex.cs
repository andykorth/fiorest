﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class PlanetIdUniqueIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_PlanetDataModels_PlanetId",
                table: "PlanetDataModels",
                column: "PlanetId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PlanetDataModels_PlanetId",
                table: "PlanetDataModels");
        }
    }
}
