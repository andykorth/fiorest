﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class Infrastructure
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string InfrastructureId { get; set; }

        public virtual List<InfrastructureProject> InfrastructureProjects { get; set; } = new List<InfrastructureProject>();
        public virtual List<InfrastructureReport> InfrastructureReports { get; set; } = new List<InfrastructureReport>();
        public virtual List<InfrastructureProgram> InfrastructurePrograms { get; set; } = new List<InfrastructureProgram>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(InfrastructureId) && InfrastructureId.Length == 32 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            InfrastructureProjects.ForEach(ip => ip.Validate());
            InfrastructureReports.ForEach(ir => ir.Validate());
            InfrastructurePrograms.ForEach(ip => ip.Validate());
        }
    }

    public class InfrastructureProject
    {
        [Key]
        [JsonIgnore]
        [StringLength(64)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string InfrastructureProjectId { get; set; }

        [StringLength(32)]
        public string InfraProjectId { get; set; }

        public int SimulationPeriod { get; set; }

        [StringLength(32)]
        public string Type { get; set; }

        [StringLength(8)]
        public string Ticker { get; set; }

        [StringLength(64)]
        public string Name { get; set; }

        public int Level { get; set; }
        public int ActiveLevel { get; set; }
        public int CurrentLevel { get; set; }
        public double UpkeepStatus { get; set; }
        public double UpgradeStatus { get; set; }

        public virtual List<InfrastructureProjectUpgradeCosts> UpgradeCosts { get; set; } = new List<InfrastructureProjectUpgradeCosts>();
        public virtual List<InfrastructureProjectUpkeeps> Upkeeps { get; set; } = new List<InfrastructureProjectUpkeeps>();
        public virtual List<InfrastructureProjectContributions> Contributions { get; set; } = new List<InfrastructureProjectContributions>();

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string InfrastructureId { get; set; }

        [JsonIgnore]
        public virtual Infrastructure Infrastructure { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(InfrastructureProjectId) && InfrastructureProjectId.Length <= 64 &&
                !String.IsNullOrEmpty(InfraProjectId) && InfraProjectId.Length == 32 &&
                SimulationPeriod >= 0 &&
                !String.IsNullOrEmpty(Type) && Type.Length <= 32 &&
                !String.IsNullOrEmpty(Ticker) && Ticker.Length <= 8 &&
                !String.IsNullOrEmpty(Name) && Name.Length <= 64 &&
                !String.IsNullOrEmpty(InfrastructureId) && InfrastructureId.Length == 32
                );

            UpgradeCosts.ForEach(uc => uc.Validate());
            Upkeeps.ForEach(u => u.Validate());
            Contributions.ForEach(c => c.Validate());
        }
    }

    public class InfrastructureProjectUpgradeCosts
    {
        [Key]
        [StringLength(128)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string InfrastructureProjectUpgradeCostsId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int Amount { get; set; }
        public int CurrentAmount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(64)]
        public string InfrastructureProjectId { get; set; }

        [JsonIgnore]
        public virtual InfrastructureProject InfrastructureProject { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(InfrastructureProjectUpgradeCostsId) && InfrastructureProjectUpgradeCostsId.Length <= 128 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                Amount >= 0 &&
                CurrentAmount >= 0 &&
                !String.IsNullOrEmpty(InfrastructureProjectId) && InfrastructureProjectId.Length <= 64
    );
        }
    }

    public class InfrastructureProjectUpkeeps
    {
        [Key]
        [StringLength(128)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string InfrastructureProjectUpkeepsId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int Stored { get; set; }
        public int StoreCapacity { get; set; }
        public int Duration { get; set; }
        public long NextTickTimestampEpochMs { get; set; }

        public int Amount { get; set; }
        public int CurrentAmount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(64)]
        public string InfrastructureProjectId { get; set; }

        [JsonIgnore]
        public virtual InfrastructureProject InfrastructureProject { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(InfrastructureProjectUpkeepsId) && InfrastructureProjectUpkeepsId.Length <= 128 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(InfrastructureProjectId) && InfrastructureProjectId.Length <= 64
                );
        }
    }

    public class InfrastructureProjectContributions
    {
        [Key]
        [StringLength(128)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string InfrastructureProjectContributionsId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int Amount { get; set; }

        public long TimestampEpochMs { get; set; }
        [StringLength(32)]
        public string CompanyId { get; set; }
        [StringLength(64)]
        public string CompanyName { get; set; }
        [StringLength(8)]
        public string CompanyCode { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(64)]
        public string InfrastructureProjectId { get; set; }

        [JsonIgnore]
        public virtual InfrastructureProject InfrastructureProject { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(InfrastructureProjectContributionsId) && InfrastructureProjectContributionsId.Length <= 128 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                Amount > 0 &&
                TimestampEpochMs > 0 &&
                !String.IsNullOrEmpty(CompanyId) && CompanyId.Length == 32 &&
                (CompanyName == null || CompanyName.Length <= 64) &&
                (CompanyCode == null || CompanyCode.Length <= 8) &&
                !String.IsNullOrEmpty(InfrastructureProjectId) && InfrastructureProjectId.Length < 64
                );
        }
    }

    public class InfrastructureReport
    {
        [Key]
        [StringLength(40)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string InfrastructureReportId { get; set; }

        public bool ExplorersGraceEnabled { get; set; }
        public int SimulationPeriod { get; set; }
        public long TimestampMs { get; set; }

        public int NextPopulationPioneer { get; set; }
        public int NextPopulationSettler { get; set; }
        public int NextPopulationTechnician { get; set; }
        public int NextPopulationEngineer { get; set; }
        public int NextPopulationScientist { get; set; }

        public int PopulationDifferencePioneer { get; set; }
        public int PopulationDifferenceSettler { get; set; }
        public int PopulationDifferenceTechnician { get; set; }
        public int PopulationDifferenceEngineer { get; set; }
        public int PopulationDifferenceScientist { get; set; }

        public float AverageHappinessPioneer { get; set; }
        public float AverageHappinessSettler { get; set; }
        public float AverageHappinessTechnician { get; set; }
        public float AverageHappinessEngineer { get; set; }
        public float AverageHappinessScientist { get; set; }

        public float UnemploymentRatePioneer { get; set; }
        public float UnemploymentRateSettler { get; set; }
        public float UnemploymentRateTechnician { get; set; }
        public float UnemploymentRateEngineer { get; set; }
        public float UnemploymentRateScientist { get; set; }

        public float OpenJobsPioneer { get; set; }
        public float OpenJobsSettler { get; set; }
        public float OpenJobsTechnician { get; set; }
        public float OpenJobsEngineer { get; set; }
        public float OpenJobsScientist { get; set; }

        public float NeedFulfillmentLifeSupport { get; set; }
        public float NeedFulfillmentSafety { get; set; }
        public float NeedFulfillmentHealth { get; set; }
        public float NeedFulfillmentComfort { get; set; }
        public float NeedFulfillmentCulture { get; set; }
        public float NeedFulfillmentEducation { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string InfrastructureId { get; set; }

        [JsonIgnore]
        public virtual Infrastructure Infrastructure { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(InfrastructureReportId) && InfrastructureReportId.Length <= 40 &&
                SimulationPeriod >= 0 &&
                TimestampMs > 0 &&
                NextPopulationPioneer >= 0 &&
                NextPopulationSettler >= 0 &&
                NextPopulationTechnician >= 0 &&
                NextPopulationEngineer >= 0 &&
                NextPopulationScientist >= 0 &&
                AverageHappinessPioneer >= 0.0f &&
                AverageHappinessSettler >= 0.0f &&
                AverageHappinessTechnician >= 0.0f &&
                AverageHappinessEngineer >= 0.0f &&
                AverageHappinessScientist >= 0.0f &&
                !String.IsNullOrEmpty(InfrastructureId) && InfrastructureId.Length == 32);
        }
    }

    public class InfrastructureProgram
    {
        [Key]
        [JsonIgnore]
        [StringLength(40)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string InfrastructureProgramId { get; set; } // {PopulationId}-{Number}

        public int Number { get; set; }

        public long StartTimestampEpochMs { get; set; }
        public long EndTimestampEpochMs { get; set; }

        [StringLength(32)]
        public string Category { get; set; }

        [StringLength(32)]
        public string Program { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string InfrastructureId { get; set; }

        [JsonIgnore]
        public virtual Infrastructure Infrastructure { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(InfrastructureProgramId) && InfrastructureProgramId.Length <= 40 &&
                Number >= 0 &&
                !String.IsNullOrEmpty(Category) && Category.Length <= 32 &&
                !String.IsNullOrEmpty(Program) && Program.Length <= 32 &&
                !String.IsNullOrEmpty(InfrastructureId) && InfrastructureId.Length == 32
                );
        }
    }
}
