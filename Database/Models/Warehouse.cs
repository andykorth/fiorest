﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(StoreId))]
    public class Warehouse
    {
        [Key]
        [StringLength(65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string WarehouseId { get; set; }

        [StringLength(32)]
        public string StoreId { get; set; }

        public int Units { get; set; }
        public double WeightCapacity { get; set; }
        public double VolumeCapacity { get; set; }
        public long? NextPaymentTimestampEpochMs { get; set; }

        public double? FeeAmount { get; set; }

        [StringLength(8)]
        public string FeeCurrency { get; set; }

        [StringLength(32)]
        public string FeeCollectorId { get; set; }

        [StringLength(64)]
        public string FeeCollectorName { get; set; }

        [StringLength(8)]
        public string FeeCollectorCode { get; set; }

        [StringLength(64)]
        public string LocationName { get; set; }

        [StringLength(8)]
        public string LocationNaturalId { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(WarehouseId) && WarehouseId.Length == 65 &&
                !String.IsNullOrEmpty(StoreId) && StoreId.Length == 32 &&
                (FeeCurrency == null || FeeCurrency.Length <= 8) &&
                (FeeCollectorId == null || FeeCollectorId.Length == 32) &&
                (FeeCollectorName == null || FeeCollectorName.Length <= 64) &&
                (FeeCollectorCode == null || FeeCollectorCode.Length <= 8) &&
                !String.IsNullOrEmpty(LocationName) && LocationName.Length <= 64 &&
                !String.IsNullOrEmpty(LocationNaturalId) && LocationNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );
        }
    }
}
