﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [IndexAttribute(nameof(PlanetId), IsUnique = true)]
    public class PlanetDataModel
    {
        [Key]
        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(8)]
        public string PlanetNaturalId { get; set; }
        [StringLength(32)]
        public string PlanetName { get; set; }
        [StringLength(32)]
        public string Namer { get; set; }
        public long NamingDataEpochMs { get; set; }
        public bool Nameable { get; set; }

        [StringLength(32)]
        public string SystemId { get; set; }

        public double Gravity { get; set; }
        public double MagneticField { get; set; }
        public double Mass { get; set; }
        public double MassEarth { get; set; }

        public double OrbitSemiMajorAxis { get; set; }
        public double OrbitEccentricity { get; set; }
        public double OrbitInclination { get; set; }
        public double OrbitRightAscension { get; set; }
        public double OrbitPeriapsis { get; set; }
        public int OrbitIndex { get; set; }

        public double Pressure { get; set; }
        public double Radiation { get; set; }
        public double Radius { get; set; }

        public virtual List<PlanetDataResource> Resources { get; set; } = new List<PlanetDataResource>();

        public double Sunlight { get; set; }
        public bool Surface { get; set; }
        public double Temperature { get; set; }
        public double Fertility { get; set; }

        public virtual List<PlanetBuildRequirement> BuildRequirements { get; set; } = new List<PlanetBuildRequirement>();

        public bool HasLocalMarket { get; set; }
        public bool HasChamberOfCommerce { get; set; }
        public bool HasWarehouse { get; set; }
        public bool HasAdministrationCenter { get; set; }
        public bool HasShipyard { get; set; }

        [StringLength(8)]
        public string FactionCode { get; set; }
        [StringLength(32)]
        public string FactionName { get; set; }

        [StringLength(32)]
        public string GovernorId { get; set; }
        [StringLength(32)]
        public string GovernorUserName { get; set; }

        [StringLength(32)]
        public string GovernorCorporationId { get; set; }
        [StringLength(64)]
        public string GovernorCorporationName { get; set; }
        [StringLength(8)]
        public string GovernorCorporationCode { get; set; }

        [StringLength(32)]
        public string CurrencyName { get; set; }
        [StringLength(8)]
        public string CurrencyCode { get; set; }

        [StringLength(32)]
        public string CollectorId { get; set; }
        [StringLength(64)]
        public string CollectorName { get; set; }
        [StringLength(8)]
        public string CollectorCode { get; set; }

        public virtual List<PlanetProductionFee> ProductionFees { get; set; } = new List<PlanetProductionFee>();
        public double? BaseLocalMarketFee { get; set; }
        public double? LocalMarketFeeFactor { get; set; }

        public double? WarehouseFee { get; set; }

        [StringLength(32)]
        public string PopulationId { get; set; }

        [StringLength(32)]
        public string COGCProgramStatus { get; set; }
        public virtual List<PlanetCOGCProgram> COGCPrograms { get; set; } = new List<PlanetCOGCProgram>();
        public virtual List<PlanetCOGCVote> COGCVotes { get; set; } = new List<PlanetCOGCVote>();
        public virtual List<PlanetCOGCUpkeep> COGCUpkeep { get; set; } = new List<PlanetCOGCUpkeep>();

        public int PlanetTier { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as PlanetDataModel;
            if (other != null)
            {
                return PlanetId == other.PlanetId &&
                       PlanetNaturalId == other.PlanetNaturalId &&
                       PlanetName == other.PlanetName &&
                       Namer == other.Namer &&
                       NamingDataEpochMs == other.NamingDataEpochMs &&
                       Nameable == other.Nameable &&
                       Gravity == other.Gravity &&
                       MagneticField == other.MagneticField &&
                       Mass == other.Mass &&
                       MassEarth == other.MassEarth &&
                       OrbitSemiMajorAxis == other.OrbitSemiMajorAxis &&
                       OrbitEccentricity == other.OrbitEccentricity &&
                       OrbitInclination == other.OrbitInclination &&
                       OrbitRightAscension == other.OrbitRightAscension &&
                       OrbitPeriapsis == other.OrbitPeriapsis &&
                       OrbitIndex == other.OrbitIndex &&
                       Pressure == other.Pressure &&
                       Radiation == other.Radiation &&
                       Radius == other.Radius &&
                       Resources.OrderBy(r => r.MaterialId).SequenceEqual(other.Resources.OrderBy(r => r.MaterialId)) &&
                       Sunlight == other.Sunlight &&
                       Surface == other.Surface &&
                       Temperature == other.Temperature &&
                       Fertility == other.Fertility &&
                       BuildRequirements.OrderBy(br => br.MaterialId).SequenceEqual(other.BuildRequirements.OrderBy(br => br.MaterialId)) &&
                       HasLocalMarket == other.HasLocalMarket &&
                       HasChamberOfCommerce == other.HasChamberOfCommerce &&
                       HasWarehouse == other.HasWarehouse &&
                       HasAdministrationCenter == other.HasAdministrationCenter &&
                       HasShipyard == other.HasShipyard &&
                       FactionCode == other.FactionCode &&
                       FactionName == other.FactionName &&
                       GovernorId == other.GovernorId &&
                       GovernorUserName == other.GovernorUserName &&
                       GovernorCorporationId == other.GovernorCorporationId &&
                       GovernorCorporationName == other.GovernorCorporationName &&
                       GovernorCorporationCode == other.GovernorCorporationCode &&
                       CurrencyName == other.CurrencyName &&
                       CurrencyCode == other.CurrencyCode &&
                       CollectorId == other.CollectorId &&
                       CollectorName == other.CollectorName &&
                       CollectorCode == other.CollectorCode &&
                       ProductionFees.OrderBy(pf => pf.Category).ThenBy(pf => pf.WorkforceLevel).SequenceEqual(other.ProductionFees.OrderBy(pf => pf.Category).ThenBy(pf => pf.WorkforceLevel)) &&
                       BaseLocalMarketFee == other.BaseLocalMarketFee &&
                       LocalMarketFeeFactor == other.LocalMarketFeeFactor &&
                       WarehouseFee == other.WarehouseFee &&
                       PopulationId == other.PopulationId &&
                       PlanetTier == other.PlanetTier;
            }

            return false;
        }

        public override int GetHashCode()
        {
            HashCode hashCode = new HashCode();

            hashCode.Add(PlanetId);
            hashCode.Add(PlanetNaturalId);
            hashCode.Add(PlanetName);
            hashCode.Add(Namer);
            hashCode.Add(NamingDataEpochMs);
            hashCode.Add(Nameable);
            hashCode.Add(Gravity);
            hashCode.Add(MagneticField);
            hashCode.Add(Mass);
            hashCode.Add(MassEarth);
            hashCode.Add(OrbitSemiMajorAxis);
            hashCode.Add(OrbitEccentricity);
            hashCode.Add(OrbitInclination);
            hashCode.Add(OrbitRightAscension);
            hashCode.Add(OrbitPeriapsis);
            hashCode.Add(OrbitIndex);
            hashCode.Add(Pressure);
            hashCode.Add(Radiation);
            hashCode.Add(Radius);
            foreach (var resource in Resources)
            {
                hashCode.Add(resource);
            }
            hashCode.Add(Sunlight);
            hashCode.Add(Surface);
            hashCode.Add(Temperature);
            hashCode.Add(Fertility);
            foreach (var buildReq in BuildRequirements)
            {
                hashCode.Add(buildReq);
            }

            hashCode.Add(HasLocalMarket);
            hashCode.Add(HasChamberOfCommerce);
            hashCode.Add(HasWarehouse);
            hashCode.Add(HasAdministrationCenter);
            hashCode.Add(HasShipyard);
            hashCode.Add(FactionCode);
            hashCode.Add(FactionName);
            hashCode.Add(GovernorId);
            hashCode.Add(GovernorUserName);
            hashCode.Add(GovernorCorporationId);
            hashCode.Add(GovernorCorporationName);
            hashCode.Add(GovernorCorporationCode);
            hashCode.Add(CurrencyName);
            hashCode.Add(CurrencyCode);
            hashCode.Add(CollectorId);
            hashCode.Add(CollectorName);
            hashCode.Add(CollectorCode);
            foreach (var prodFee in ProductionFees)
            {
                hashCode.Add(prodFee);
            }
            hashCode.Add(BaseLocalMarketFee);
            hashCode.Add(LocalMarketFeeFactor);
            hashCode.Add(WarehouseFee);
            hashCode.Add(PopulationId);
            hashCode.Add(PlanetTier);

            return hashCode.ToHashCode();
        }

        public void UpdatePlanetTier()
        {
            int tier = 1;
            if (Gravity < 0.25)
            {
                tier += 1;
            }
            else if (Gravity > 2.5)
            {
                tier += 1;
            }

            if (Pressure < 0.25)
            {
                tier += 1;
            }
            else if (Pressure > 2.0)
            {
                tier += 2;
            }

            if (Temperature < -25.0)
            {
                tier += 3;
            }
            else if (Temperature > 75.0)
            {
                tier += 4;
            }

            PlanetTier = tier;
        }
    }

    public class PlanetDataResource
    {
        [JsonIgnore]
        public int PlanetDataResourceId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(32)]
        public string ResourceType { get; set; }
        public double Factor { get; set; }

        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        [JsonIgnore]
        public virtual PlanetDataModel PlanetDataModel { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as PlanetDataResource;
            if (other != null)
            {
                return MaterialId == other.MaterialId &&
                       ResourceType == other.ResourceType &&
                       Factor == other.Factor;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(MaterialId, ResourceType, Factor);
        }
    }

    public class PlanetBuildRequirement
    {
        [JsonIgnore]
        public int PlanetBuildRequirementId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialCategory { get; set; }

        public int MaterialAmount { get; set; }

        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        [JsonIgnore]
        public virtual PlanetDataModel PlanetDataModel { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as PlanetBuildRequirement;
            if (other != null)
            {
                return MaterialName == other.MaterialName &&
                       MaterialId == other.MaterialId &&
                       MaterialTicker == other.MaterialTicker &&
                       MaterialCategory == other.MaterialCategory &&
                       MaterialAmount == other.MaterialAmount &&
                       MaterialWeight == other.MaterialWeight &&
                       MaterialVolume == other.MaterialVolume;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(MaterialName, MaterialId, MaterialTicker, MaterialCategory, MaterialAmount, MaterialWeight, MaterialVolume);
        }
    }

    public class PlanetProductionFee
    {
        [JsonIgnore]
        public int PlanetProductionFeeId { get; set; }

        [StringLength(32)]
        public string Category { get; set; }
        [StringLength(32)]
        public string WorkforceLevel { get; set; }
        public double FeeAmount { get; set; }
        public string FeeCurrency { get; set; }

        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        [JsonIgnore]
        public virtual PlanetDataModel PlanetDataModel { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as PlanetProductionFee;
            if (other != null)
            {
                return Category == other.Category &&
                       WorkforceLevel == other.WorkforceLevel &&
                       FeeAmount == other.FeeAmount &&
                       FeeCurrency == other.FeeCurrency;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Category, WorkforceLevel, FeeAmount, FeeCurrency);
        }
    }

    public class PlanetCOGCProgram
    {
        [JsonIgnore]
        public int PlanetCOGCProgramId { get; set; }

        [StringLength(32)]
        public string ProgramType { get; set; }
        public long? StartEpochMs { get; set; }
        public long? EndEpochMs { get; set; }

        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        [JsonIgnore]
        public virtual PlanetDataModel PlanetDataModel { get; set; }
    }

    public class PlanetCOGCVote
    {
        [JsonIgnore]
        public int PlanetCOGCVoteId { get; set; }

        [StringLength(64)]
        public string CompanyName { get; set; }
        [StringLength(8)]
        public string CompanyCode { get; set; }
        public float? Influence { get; set; }
        public string VoteType { get; set; }
        public long VoteTimeEpochMs { get; set; }

        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        [JsonIgnore]
        public virtual PlanetDataModel PlanetDataModel { get; set; }
    }

    public class PlanetCOGCUpkeep
    {
        [JsonIgnore]
        public int PlanetCOGCUpkeepId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }
        public int MaterialAmount { get; set; }
        public int CurrentAmount { get; set; }
        public long DueDateEpochMs { get; set; }

        [JsonIgnore]
        public int PlanetDataModelId { get; set; }

        [JsonIgnore]
        public virtual PlanetDataModel PlanetDataModel { get; set; }
    }

    [Microsoft.EntityFrameworkCore.Index(nameof(PlanetId), nameof(PlotId), IsUnique = true)]
    public class PlanetSite
    {
        [JsonIgnore]
        public int PlanetSiteId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }
        [StringLength(32)]
        public string OwnerId { get; set; }

        [StringLength(32)]
        public string OwnerName { get; set; }
        [StringLength(8)]
        public string OwnerCode { get; set; }

        public int PlotNumber { get; set; }
        [StringLength(32)]
        public string PlotId { get; set; }
        [StringLength(8)]
        public string SiteId { get; set; } // First 8 characters of PlotId
    }
}
