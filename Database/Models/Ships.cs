﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(StoreId))]
    [Index(nameof(StlFuelStoreId))]
    [Index(nameof(FtlFuelStoreId))]
    [Index(nameof(Registration))]
    [Index(nameof(Name))]
    public class Ship
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ShipId { get; set; }

        [StringLength(32)]
        public string StoreId { get; set; }

        [StringLength(32)]
        public string StlFuelStoreId { get; set; }

        [StringLength(32)]
        public string FtlFuelStoreId { get; set; }

        [StringLength(32)]
        public string Registration { get; set; }

        [StringLength(32)]
        public string Name { get; set; }

        public long CommissioningTimeEpochMs { get; set; }

        [StringLength(32)]
        public string BlueprintNaturalId { get; set; }

        [StringLength(32)]
        public string FlightId { get; set; }

        public double Acceleration { get; set; }
        public double Thrust { get; set; }
        public double Mass { get; set; }
        public double OperatingEmptyMass { get; set; }
        public double ReactorPower { get; set; }
        public double EmitterPower { get; set; }
        public double Volume { get; set; }

        public double Condition { get; set; }
        public long? LastRepairEpochMs { get; set; }

        public virtual List<ShipRepairMaterial> RepairMaterials { get; set; } = new List<ShipRepairMaterial>();

        [StringLength(64)]
        public string Location { get; set; }

        public virtual List<ShipLocationAddressLine> AddressLines { get; set; } = new List<ShipLocationAddressLine>();

        public double StlFuelFlowRate { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrWhiteSpace(ShipId) && ShipId.Length == 32 &&
                !String.IsNullOrWhiteSpace(StoreId) && StoreId.Length == 32 &&
                !String.IsNullOrWhiteSpace(StlFuelStoreId) && StlFuelStoreId.Length == 32 &&
                !String.IsNullOrWhiteSpace(FtlFuelStoreId) && FtlFuelStoreId.Length == 32 &&
                !String.IsNullOrWhiteSpace(Registration) && Registration.Length <= 32 &&
                (Name == null || Name.Length <= 32) &&
                CommissioningTimeEpochMs > 0 &&
                (BlueprintNaturalId == null || BlueprintNaturalId.Length <= 32) &&
                (FlightId == null || FlightId.Length == 32) &&
                Acceleration > 0.0 &&
                Thrust > 0.0 &&
                Mass > 0.0 &&
                OperatingEmptyMass >= 0.0 &&
                ReactorPower >= 0.0 &&
                EmitterPower >= 0.0 &&
                Volume > 0.0 &&
                Condition > 0.0 &&
                (LastRepairEpochMs == null || LastRepairEpochMs >= 0) &&
                (Location == null || Location.Length <= 64) &&
                StlFuelFlowRate >= 0.0 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            RepairMaterials.ForEach(rm => rm.Validate());
            AddressLines.ForEach(al => al.Validate());
        }
    }

    public class ShipLocationAddressLine
    {
        [Key]
        [JsonIgnore]
        [StringLength(65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ShipLocationAddressLineId { get; set; } // ShipId-LineEntityId

        [StringLength(32)]
        public string LineId { get; set; }

        [StringLength(16)]
        public string LineType { get; set; }

        [StringLength(8)]
        public string NaturalId { get; set; }

        [StringLength(32)]
        public string Name { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string ShipId { get; set; }

        [JsonIgnore]
        public virtual Ship Ship { get; set; }

        public void Validate()
        {
            Debug.Assert(
                (!String.IsNullOrEmpty(ShipLocationAddressLineId) && ShipLocationAddressLineId.Length == 65) &&
                (!String.IsNullOrEmpty(LineId) && LineId.Length == 32) &&
                (!String.IsNullOrEmpty(LineType) && LineType.Length <= 16) &&
                (NaturalId == null || NaturalId.Length <= 8) &&
                (Name == null || Name.Length <= 32) &&
                (!String.IsNullOrEmpty(ShipId) && ShipId.Length == 32));
        }
    }

    public class ShipRepairMaterial
    {
        [Key]
        [StringLength(41)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ShipRepairMaterialId { get; set; } // ShipId-MaterialTicker

        [StringLength(64)]
        public string MaterialName { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }

        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public int Amount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string ShipId { get; set; }

        [JsonIgnore]
        public virtual Ship Ship { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ShipRepairMaterialId) && ShipRepairMaterialId.Length <= 41 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                Amount > 0 &&
                !String.IsNullOrEmpty(ShipId) && ShipId.Length == 32
                );
        }
    }
}
