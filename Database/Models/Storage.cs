﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(AddressableId))]
    public class Storage
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string StorageId { get; set; }

        [StringLength(32)]
        public string AddressableId { get; set; }

        [StringLength(32)]
        public string Name { get; set; }

        public double WeightLoad { get; set; }
        public double WeightCapacity { get; set; }
        public double VolumeLoad { get; set; }
        public double VolumeCapacity { get; set; }

        public virtual List<StorageItem> StorageItems { get; set; } = new List<StorageItem>();

        public bool FixedStore { get; set; }

        [StringLength(16)]
        public string Type { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(StorageId) && StorageId.Length == 32 &&
                !String.IsNullOrEmpty(AddressableId) && AddressableId.Length == 32 &&
                (Name == null || Name.Length <= 32) &&
                WeightLoad >= 0.0 &&
                WeightCapacity >= 0.0 &&
                VolumeLoad >= 0.0 &&
                VolumeCapacity >= 0.0 &&
                !String.IsNullOrEmpty(Type) && Type.Length <= 16 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            StorageItems.ForEach(si => si.Validate());
        }
    }

    public class StorageItem
    {
        [Key]
        [JsonIgnore]
        [StringLength(65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string StorageItemId { get; set; } // StorageId-MaterialId

        [StringLength(32)]
        public string MaterialId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }

        [StringLength(8)]
        public string MaterialTicker { get; set; }

        [StringLength(32)]
        public string MaterialCategory { get; set; }

        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        public int MaterialAmount { get; set; }

        public float MaterialValue { get; set; }

        [StringLength(8)]
        public string MaterialValueCurrency { get; set; }

        [StringLength(16)]
        public string Type { get; set; }

        public double TotalWeight { get; set; }
        public double TotalVolume { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string StorageId { get; set; }

        [JsonIgnore]
        public virtual Storage Storage { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(StorageItemId) && StorageItemId.Length <= 65 &&
                (MaterialId == null || MaterialId.Length == 32) &&
                (MaterialName == null || MaterialName.Length <= 64) &&
                (MaterialTicker == null || MaterialTicker.Length <= 8) &&
                (MaterialCategory == null || MaterialCategory.Length <= 32) &&
                MaterialWeight >= 0.0 &&
                MaterialVolume >= 0.0 &&
                MaterialAmount >= 0 &&
                (MaterialValueCurrency == null || MaterialValueCurrency.Length <= 8) &&
                !String.IsNullOrEmpty(Type) && Type.Length <= 16 &&
                TotalWeight >= 0.0 &&
                TotalVolume >= 0.0 &&
                !String.IsNullOrEmpty(StorageId) && StorageId.Length == 32
                );
        }
    }
}
