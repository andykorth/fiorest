﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(Ticker))]
    public class Material
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string MaterialId { get; set; }

        [StringLength(32)]
        public string CategoryName { get; set; }

        [StringLength(32)]
        public string CategoryId { get; set; }

        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(8)]
        public string Ticker { get; set; }

        public double Weight { get; set; }
        public double Volume { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(CategoryName) && CategoryName.Length <= 32 &&
                !String.IsNullOrEmpty(CategoryId) && CategoryId.Length == 32 &&
                !String.IsNullOrEmpty(Name) && Name.Length <= 64 &&
                !String.IsNullOrEmpty(Ticker) && Ticker.Length <= 8 &&
                Weight > 0.0 && Volume > 0.0 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );
        }
    }
}
