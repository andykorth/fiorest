﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(UserName))]
    [Index(nameof(CompanyId))]
    public class UserData
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserDataId { get; set; } // CompanyId

        [StringLength(32)]
        public string UserId { get; set; }

        [StringLength(32)]
        public string UserName { get; set; }

        [StringLength(16)]
        public string SubscriptionLevel { get; set; }

        [StringLength(16)]
        public string Tier { get; set; }

        public bool Team { get; set; }
        public bool Pioneer { get; set; }
        public bool Moderator { get; set; }

        public long CreatedEpochMs { get; set; }

        [StringLength(32)]
        public string CompanyId { get; set; }

        [StringLength(64)]
        public string CompanyName { get; set; }

        [StringLength(4)]
        public string CompanyCode { get; set; }

        [StringLength(32)]
        public string CountryId { get; set; }

        [StringLength(4)]
        public string CountryCode { get; set; }

        [StringLength(64)]
        public string CountryName { get; set; }

        [StringLength(32)]
        public string CorporationId { get; set; }

        [StringLength(64)]
        public string CorporationName { get; set; }

        [StringLength(8)]
        public string CorporationCode { get; set; }

        [StringLength(8)]
        public string OverallRating { get; set; }

        [StringLength(8)]
        public string ActivityRating { get; set; }

        [StringLength(8)]
        public string ReliabilityRating { get; set; }

        [StringLength(8)]
        public string StabilityRating { get; set; }

        // Start of private section
        [StringLength(8)]
        public string HeadquartersNaturalId { get; set; }

        public int HeadquartersLevel { get; set; }
        public int HeadquartersBasePermits { get; set; }
        public int HeadquartersUsedBasePermits { get; set; }
        public int AdditionalBasePermits { get; set; }
        public int AdditionalProductionQueueSlots { get; set; }

        public bool RelocationLocked { get; set; }
        public long NextRelocationTimeEpochMs { get; set; }

        public virtual List<UserDataBalance> Balances { get; set; } = new List<UserDataBalance>();
        // End of private section

        public virtual List<UserDataPlanet> Planets { get; set; } = new List<UserDataPlanet>();

        public virtual List<UserDataOffice> Offices { get; set; } = new List<UserDataOffice>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        [Flags]
        public enum UserDataValidationType
        {
            Default = 0,
            CheckUserIdAndName = 1,
            CheckCompanyNameAndCode = 2,
            CheckSubAndTier = 4,
            CheckCountrySettings = 8,
            CheckCorporationSettings = 16,
            CheckRatings = 32,
            CheckPlanets = 64,
            CheckPrivateData = 128,

            Full = CheckUserIdAndName | CheckCompanyNameAndCode | CheckSubAndTier | CheckCountrySettings | CheckCorporationSettings | CheckRatings | CheckPlanets | CheckPrivateData
        }

        public void Validate(UserDataValidationType validationType)
        {
            bool bIsValid = true;
            bIsValid &= !String.IsNullOrEmpty(UserDataId) && UserDataId.Length == 32;
            bIsValid &= !String.IsNullOrEmpty(UserName) && UserName.Length <= 32;

            if (validationType.HasFlag(UserDataValidationType.CheckSubAndTier))
            {
                bIsValid &= !String.IsNullOrEmpty(SubscriptionLevel) && SubscriptionLevel.Length <= 16;
                bIsValid &= !String.IsNullOrEmpty(Tier) && Tier.Length <= 16;
            }

            bIsValid &= !String.IsNullOrEmpty(CompanyId) && CompanyId.Length == 32;

            if (validationType.HasFlag(UserDataValidationType.CheckCompanyNameAndCode))
            {
                bIsValid &= !String.IsNullOrEmpty(CompanyName) && CompanyName.Length <= 64;
                bIsValid &= !String.IsNullOrEmpty(CompanyCode) && CompanyCode.Length <= 4;
            }
            
            if (validationType.HasFlag(UserDataValidationType.CheckCountrySettings))
            {
                bIsValid &= !String.IsNullOrEmpty(CountryId) && CountryId.Length == 32;
                bIsValid &= !String.IsNullOrEmpty(CountryCode) && CountryCode.Length <= 4;
                bIsValid &= !String.IsNullOrEmpty(CountryName) && CountryName.Length <= 64;
            }

            if (validationType.HasFlag(UserDataValidationType.CheckRatings))
            {
                bIsValid &= !String.IsNullOrEmpty(OverallRating) && OverallRating.Length <= 8;
                bIsValid &= !String.IsNullOrEmpty(ActivityRating) && ActivityRating.Length <= 8;
                bIsValid &= !String.IsNullOrEmpty(ReliabilityRating) && ReliabilityRating.Length <= 8;
                bIsValid &= !String.IsNullOrEmpty(StabilityRating) && StabilityRating.Length <= 8;
            }

            if (validationType.HasFlag(UserDataValidationType.CheckPlanets))
            {
                Planets.ForEach(p => p.Validate());
            }

            if (validationType.HasFlag(UserDataValidationType.CheckPrivateData))
            {
                bIsValid &= String.IsNullOrEmpty(HeadquartersNaturalId) || HeadquartersNaturalId.Length <= 8;
                bIsValid &= HeadquartersLevel >= 0;
                bIsValid &= HeadquartersBasePermits >= 0;
                bIsValid &= HeadquartersUsedBasePermits >= 0;
                bIsValid |= AdditionalBasePermits >= 0;
                bIsValid |= AdditionalProductionQueueSlots >= 0;
                Balances.ForEach(b => b.Validate());
            }

            Offices.ForEach(o => o.Validate());

            bIsValid &= !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32;
            bIsValid &= Timestamp != default;
        }

        public void StripPrivateData()
        {
            HeadquartersNaturalId = null;
            HeadquartersLevel = -1;
            HeadquartersBasePermits = -1;
            HeadquartersUsedBasePermits = -1;
            AdditionalBasePermits = -1;
            AdditionalProductionQueueSlots = -1;
            RelocationLocked = false;
            NextRelocationTimeEpochMs = 0;
            Balances = null;
        }
    }

    public class UserDataPlanet
    {
        [Key]
        [JsonIgnore]
        [StringLength(65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserDataPlanetId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }

        [StringLength(8)]
        public string PlanetNaturalId { get; set; }

        [StringLength(32)]
        public string PlanetName { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string UserDataId { get; set; }

        [JsonIgnore]
        public virtual UserData UserData { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(UserDataPlanetId) && UserDataPlanetId.Length == 65 &&
                !String.IsNullOrEmpty(PlanetId) && PlanetId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetNaturalId) && PlanetNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(PlanetName) && PlanetName.Length <= 32 &&
                !String.IsNullOrEmpty(UserDataId) && UserDataId.Length == 32
            );
        }
    }

    public class UserDataOffice
    {
        [Key]
        [JsonIgnore]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserDataOfficeId { get; set; }

        [StringLength(8)]
        public string PlanetNaturalId { get; set; }

        [StringLength(32)]
        public string PlanetName { get; set; }

        public long StartEpochMs { get; set; }
        public long EndEpochMs { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string UserDataId { get; set; }

        [JsonIgnore]
        public virtual UserData UserData { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(UserDataOfficeId) && UserDataOfficeId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetNaturalId) && PlanetNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(PlanetName) && PlanetName.Length <= 32 &&
                StartEpochMs > 0 &&
                EndEpochMs > 0);
        }
    }

    public class UserDataBalance
    {
        [Key]
        [StringLength(40)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserDataBalanceId { get; set; }

        [StringLength(8)]
        public string Currency { get; set; }

        public double Amount { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string UserDataId { get; set; }

        [JsonIgnore]
        public virtual UserData UserData { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(UserDataBalanceId) && UserDataBalanceId.Length <= 40 &&
                !String.IsNullOrEmpty(Currency) && Currency.Length <= 8 &&
                Amount >= 0.0 &&
                !String.IsNullOrEmpty(UserDataId) && UserDataId.Length == 32
            );
        }
    }
}
