﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(ShipId))]
    public class Flight
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string FlightId { get; set; }

        [StringLength(32)]
        public string ShipId { get; set; }

        [StringLength(64)]
        public string Origin { get; set; }

        [StringLength(64)]
        public string Destination { get; set; }

        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }

        public virtual List<FlightSegment> Segments { get; set; } = new List<FlightSegment>();

        public int CurrentSegmentIndex { get; set; }

        public double StlDistance { get; set; }
        public double FtlDistance { get; set; }

        public bool IsAborted { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(FlightId) && FlightId.Length == 32 &&
                !String.IsNullOrEmpty(ShipId) && ShipId.Length == 32 &&
                !String.IsNullOrEmpty(Origin) && Origin.Length <= 64 &&
                !String.IsNullOrEmpty(Destination) && Destination.Length <= 64 &&
                CurrentSegmentIndex >= 0 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            Segments.ForEach(s => s.Validate());
        }
    }

    public class FlightSegment
    {
        [Key]
        [JsonIgnore]
        [StringLength(64)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string FlightSegmentId { get; set; } // FlightId-Index

        [StringLength(32)]
        public string Type { get; set; }

        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }

        public double? StlDistance { get; set; }
        public double? StlFuelConsumption { get; set; }
        public double? FtlDistance { get; set; }
        public double? FtlFuelConsumption { get; set; }

        [StringLength(64)]
        public string Origin { get; set; }

        [StringLength(64)]
        public string Destination { get; set; }

        public virtual List<OriginLine> OriginLines { get; set; } = new List<OriginLine>();
        public virtual List<DestinationLine> DestinationLines { get; set; } = new List<DestinationLine>();

        [JsonIgnore]
        [StringLength(32)]
        public string FlightId { get; set; }

        [JsonIgnore]
        public virtual Flight Flight { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(FlightSegmentId) && FlightSegmentId.Length <= 64 &&
                !String.IsNullOrEmpty(Type) && Type.Length <= 32 &&
                !String.IsNullOrEmpty(Origin) && Origin.Length <= 64 &&
                !String.IsNullOrEmpty(Destination) && Destination.Length <= 64 &&
                !String.IsNullOrEmpty(FlightId) && FlightId.Length == 32
                );

            OriginLines.ForEach(ol => ol.Validate());
            DestinationLines.ForEach(dl => dl.Validate());
        }
    }

    public class OriginLine
    {
        [Key]
        [JsonIgnore]
        [StringLength(128)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string OriginLineId { get; set; } // FlightSegmentId-LineId

        [StringLength(32)]
        public string Type { get; set; }

        [StringLength(32)]
        public string LineId { get; set; }

        [StringLength(16)]
        public string LineNaturalId { get; set; }

        [StringLength(64)]
        public string LineName { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(64)]
        public string FlightSegmentId { get; set; }

        [JsonIgnore]
        public virtual FlightSegment FlightSegment { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(OriginLineId) && OriginLineId.Length <= 128 &&
                !String.IsNullOrEmpty(Type) && Type.Length <= 32 &&
                !String.IsNullOrEmpty(LineId) && LineId.Length == 32 &&
                !String.IsNullOrEmpty(LineNaturalId) && LineNaturalId.Length <= 16 &&
                !String.IsNullOrEmpty(LineName) && LineName.Length <= 64 &&
                !String.IsNullOrEmpty(FlightSegmentId) && FlightSegmentId.Length <= 64
            );
        }
    }

    public class DestinationLine
    {
        [Key]
        [JsonIgnore]
        [StringLength(128)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string DestinationLineId { get; set; } // FlightSegmentId-LineId

        [StringLength(32)]
        public string Type { get; set; }

        [StringLength(32)]
        public string LineId { get; set; }

        [StringLength(16)]
        public string LineNaturalId { get; set; }

        [StringLength(64)]
        public string LineName { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(64)]
        public string FlightSegmentId { get; set; }

        [JsonIgnore]
        public virtual FlightSegment FlightSegment { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(DestinationLineId) && DestinationLineId.Length <= 128 &&
                !String.IsNullOrEmpty(Type) && Type.Length <= 32 &&
                !String.IsNullOrEmpty(LineId) && LineId.Length == 32 &&
                !String.IsNullOrEmpty(LineNaturalId) && LineNaturalId.Length <= 16 &&
                !String.IsNullOrEmpty(LineName) && LineName.Length <= 64 &&
                !String.IsNullOrEmpty(FlightSegmentId) && FlightSegmentId.Length <= 64
                );
        }
    }
}
