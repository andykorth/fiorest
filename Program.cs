﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Mono.Options;

//using Microsoft.EntityFrameworkCore.Infrastructure;
//using Microsoft.EntityFrameworkCore.Migrations;


namespace FIORest
{
    class Program
    {
        private const string FIORestGuid = "4b9ab7cc-ac18-404e-a9a0-81bfdd9dbad5";
        private static bool ShouldBeInteractive = false;
        private static bool ShouldShowHelp = false;

        private static AutoResetEvent ShouldExitEvent = new AutoResetEvent(false);

        static void Main(string[] args)
        {
            OptionSet options = new OptionSet
            {
                {
                    "v",
                    "Increase debug message verbosity",
                    v =>
                    {
                        if (v != null)
                        {
                            ++Globals.Opts.VerbosityLevel;
                        }
                    }
                },
                {
                    "port=",
                    "What port number to use",
                    (int port) =>
                    {
                        Globals.Opts.Port = port;
                    }
                },
                {
                    "database=",
                    "Type of database to use (sqlite, postgres)",
                    databasetype =>
                    {
                        if(databasetype != null)
                        {
                            if(databasetype == "postgres")
                            {
                                Globals.Opts.DatabaseType = Options.DBTypes.Postgres;
                            }
                            else
                            {
                                // Default to Sqlite.
                                Globals.Opts.DatabaseType = Options.DBTypes.Sqlite;
                            }
                        }
                    }
                },
                {
                    "databasefilepath=",
                    "The path to store the sqlite database",
                    databasefilepath =>
                    {
                        if (databasefilepath != null)
                        {
                            Globals.Opts.DatabaseFilePath = databasefilepath;
                        }
                    }
                },
                {
                    "databasehost=",
                    "(Postgresql) database server to connect to",
                    databasehost =>
                    {
                        if(databasehost != null)
                        {
                            Globals.Opts.DatabaseHost = databasehost;
                        }
                    }
                },
                {
                    "databaseuser=",
                    "(Postgresql) database user name",
                    databaseuser =>
                    {
                        if(databaseuser != null)
                        {
                            Globals.Opts.DatabaseUser = databaseuser;
                        }
                    }
                },
                {
                    "databasepass=",
                    "(Postgresql) database password",
                    databasepass =>
                    {
                        if(databasepass != null)
                        {
                            Globals.Opts.DatabasePass = databasepass;
                        }
                    }
                },
                {
                    "databasename=",
                    "(Postgresql) database name",
                    databasename =>
                    {
                        if(databasename != null)
                        {
                            Globals.Opts.DatabaseName = databasename;
                        }
                    }
                },
                {
                    "debug-database",
                    "Enable database debugging (displays queries)",
                    debugdatabase =>
                    {
                        if(debugdatabase != null)
                        {
                            Globals.Opts.DebugDatabase = true;
                        }
                    }
                },
                {
                    "badrequestpath=",
                    "The path to store bad requests",
                    badrequestpath =>
                    {
                        if (badrequestpath != null)
                        {
                            Globals.Opts.BadRequestPath = badrequestpath;
                        }
                    }
                },
                {
                    "updatedirectory=",
                    "Path to the update directory",
                    updatedirectory =>
                    {
                        if (updatedirectory != null)
                        {
                            Globals.Opts.UpdateDirectory = updatedirectory;
                        }
                    }
                },
                {
                    "migrate",
                    "Applies any pending migrations to the database",
                    migrate =>
                    {
                        if (migrate != null)
                        {
                            Globals.Opts.ShouldApplyMigration = true;
                        }
                    }
                },
                {
                    "h|help",
                    "Show this message and exit",
                    h =>
                    {
                        ShouldShowHelp = (h != null);
                    }
                },
                {
                    "i|interactive",
                    "Show interactive options",
                    i =>
                    {
                        ShouldBeInteractive = (i != null);
                    }
                }
            };

            options.Parse(args);

            if (ShouldShowHelp)
            {
                options.WriteOptionDescriptions(Console.Out);
                Environment.Exit(0);
            }

            using (var gm = new GlobalMutex($"{FIORestGuid}-{Globals.Opts.Port}"))
            {
                if (!gm.Acquired())
                {
                    Console.Error.WriteLine("Failed to acquire global mutex.");
                    Environment.Exit(1);
                }

                using (var db = PRUNDataContext.GetNewContext())
                {
                    var pendingMigrations = db.Database.GetPendingMigrations().ToList();

                    if (Globals.Opts.ShouldApplyMigration)
                    {
                        if (pendingMigrations.Count == 0)
                        {
                            //var migrator = db.GetService<IMigrator>();
                            //migrator.Migrate("AddInfrastructurePrograms");
                            Console.Out.WriteLine("No migrations needed to be applied.");
                            Environment.Exit(0);
                        }

                        // Back up database
                        if (Globals.Opts.DatabaseType == Options.DBTypes.Sqlite && !String.IsNullOrWhiteSpace(Globals.Opts.DatabaseFilePath))
                        {
                            var fullyQualifiedDBPath = Path.GetFullPath(Globals.Opts.DatabaseFilePath);
                            var dbFileName = Path.GetFileName(Globals.Opts.DatabaseFilePath);
                            var dbDirectory = Path.GetDirectoryName(fullyQualifiedDBPath);
                            var backupDirectory = Path.GetFullPath(Globals.Opts.DatabaseBackupPath);
                            Directory.CreateDirectory(backupDirectory);
                            var backupDBFilePath = Path.Combine(backupDirectory, $"{pendingMigrations[0]}-{dbFileName}");
                            Console.Out.WriteLine($"Backing up Database: \n  '{fullyQualifiedDBPath}'\n ->\n  '{backupDBFilePath}'");
                            try
                            {
                                File.Copy(fullyQualifiedDBPath, backupDBFilePath);
                            }
                            catch
                            {
#if DEBUG
                                // Copy failed
                                Debugger.Break();
#endif
                            }
                        }


                        Console.Out.WriteLine("Appling the following migrations:");
                        pendingMigrations.ForEach(migration =>
                        {
                            Console.Out.WriteLine($"    {migration}");
                        });
                        Console.Out.WriteLine("=================================");
                        db.Database.Migrate();
                        Console.Out.WriteLine("Migration(s) Applied Successfully.");
                        Environment.Exit(0);
                    }
                    else if (pendingMigrations.Count > 0)
                    {
                        Console.Error.WriteLine("The database requires migrations be applied.");
                        Environment.Exit(1);
                    }
                }

                AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit; 

                var host = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseKestrel(options =>
                {
                    options.AllowSynchronousIO = true;
                    options.ListenAnyIP(Globals.Opts.Port);
                })
                .UseStartup<Startup>()
                .Build();
#if WITH_JUMPCALCULATOR
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var systems = DB.Systems
                        .AsNoTracking()
                        .Include(m => m.Connections)
                        .ToList();
                    Console.Out.WriteLine("Initializing JumpCalculator");
                    JumpCalculator.Initialize(systems);

                    Console.Out.WriteLine("Initializing JumpCount Cache");
                    JumpCalculator.InitExchangeMemoryCache(systems, DB);
                }
#endif // WITH_JUMPCALCULATOR

                try
                {
                    host.RunAsync();
                    TimedEvents.Start();
                }
                catch (Exception)
                {
                    Console.Error.WriteLine("Failed to start server");
                }

                Console.Out.WriteLine("Server started.");

                if (ShouldBeInteractive)
                {
                    PrintHelp();

                    ConsoleKeyInfo k;
                    do
                    {
                        Console.Out.Write("\b \b");
                        k = Console.ReadKey();
                        Console.Out.Write("\b \b");
                        switch (k.KeyChar)
                        {
                            case 'c':
                                CreateUser();
                                break;
                            case 'h':
                                PrintHelp();
                                break;
                        }
                    } while (k.KeyChar != 'q');
                }
                else
                {
                    Console.CancelKeyPress += (s, e) =>
                    {
                        Console.WriteLine("SIGTERM received.  Shutting down gracefully.");
                        ShouldExitEvent.Set();
                    };

                    ShouldExitEvent.WaitOne();
                }

                host.StopAsync();
            }
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            TimedEvents.Stop();
        }

        // Not cryptographically secure, but who cares.
        private static Random rnd = new Random();
        private static string GenerateRandomPassword(int length)
        {
            const string validCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_-=+/\\'\"<>,.`~";
            StringBuilder res = new StringBuilder();
            while (0 < length--)
            {
                res.Append(validCharacters[rnd.Next(validCharacters.Length)]);
            }

            return res.ToString();
        }

        private static void CreateUser()
        {
            Console.Out.WriteLine("Enter UserName: ");
            string username = Console.ReadLine().Trim();
            if (username.Length == 0)
            {
                Console.Error.WriteLine("Empty username specified. Canceling user create.");
                return;
            }


            string preMadeRandomPassword = GenerateRandomPassword(16);
            Console.Out.WriteLine($"Enter Password: [{preMadeRandomPassword}] ");
            string password = Console.ReadLine();
            if (password.Length == 0)
            {
                // Use the pre-made
                password = preMadeRandomPassword;
            }
            else if (password.Length < 8)
            {
                Console.Error.WriteLine("Password with length less than 8 specified.  Canceling user create.");
                return;
            }

            bool isAdmin = false;
            Console.Out.WriteLine("User Administrator (y/n): [n] ");
            string isAdminInput = Console.ReadLine().Trim().ToLower();
            if (isAdminInput.Length > 0 && isAdminInput[0] == 'y')
            {
                isAdmin = true;
            }

            bool exitingUser = false;
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var authModel = DB.AuthenticationModels.Where(e => e.UserName == username).FirstOrDefault();
                if (authModel != null)
                {
                    exitingUser = true;
                    Console.Out.WriteLine("User already exists.  Overwrite? (y/n): [n] ");
                    string overwriteInput = Console.ReadLine().Trim().ToLower();
                    if (overwriteInput.Length == 0 || overwriteInput[0] == 'n')
                    {
                        Console.Error.WriteLine("Canceling user create.");
                        return;
                    }
                }

                if (authModel == null)
                {
                    authModel = new AuthenticationModel();
                }

                authModel.AccountEnabled = true;
                authModel.UserName = username;
                authModel.PasswordHash = SecurePasswordHasher.Hash(password);
                authModel.IsAdministrator = isAdmin;

                if (!exitingUser)
                {
                    DB.AuthenticationModels.Add(authModel);
                }
                DB.SaveChanges();
                Console.Out.WriteLine("User created.");
            }
        }

        private static void PrintHelp()
        {
            Console.Out.WriteLine("Press 'c' to create a user.");
            Console.Out.WriteLine("Press 'h' to display this help.");
            Console.Out.WriteLine("Press 'q' to quit.\n");
        }
    }
}
