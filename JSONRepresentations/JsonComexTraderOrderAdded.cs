﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.JsonComexTraderOrderAdded
{

    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string id { get; set; }
        public StdExchange exchange { get; set; }
        public string brokerId { get; set; }
        public string type { get; set; }
        public StdMaterial material { get; set; }
        public int amount { get; set; }
        public int initialAmount { get; set; }
        public StdPrice limit { get; set; }
        public string status { get; set; }
        public StdTimestamp created { get; set; }
        public object[] trades { get; set; }
    }

}
