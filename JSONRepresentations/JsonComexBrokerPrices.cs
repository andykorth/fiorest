﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.ComexBrokerPrices
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public string brokerId { get; set; }
        public StdTimestamp from { get; set; }
        public Price[] prices { get; set; }
    }

    public class Price
    {
        public long date { get; set; }
        public float open { get; set; }
        public float close { get; set; }
        public float volume { get; set; }
        public int traded { get; set; }
    }

}
