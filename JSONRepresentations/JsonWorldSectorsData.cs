﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.WorldSectors
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Sector[] sectors { get; set; }
    }

    public class Sector
    {
        public string id { get; set; }
        public string name { get; set; }
        public Hex hex { get; set; }
        public int size { get; set; }
        public Subsector[] subsectors { get; set; }
    }

    public class Hex
    {
        public int q { get; set; }
        public int r { get; set; }
        public int s { get; set; }
    }

    public class Subsector
    {
        public string id { get; set; }
        public Vertex[] vertices { get; set; }
    }

    public class Vertex
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

}
