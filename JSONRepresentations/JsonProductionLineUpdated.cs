﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.ProductionLineUpdated
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string id { get; set; }
        public string siteId { get; set; }
        public StdAddress address { get; set; }
        public string type { get; set; }
        public int capacity { get; set; }
        public int slots { get; set; }
        public float efficiency { get; set; }
        public float condition { get; set; }
        public Workforce[] workforces { get; set; }
        public Order[] orders { get; set; }
        public Productiontemplate[] productionTemplates { get; set; }
        public Efficiencyfactor[] efficiencyFactors { get; set; }
    }


    public class Workforce
    {
        public string level { get; set; }
        public float efficiency { get; set; }
    }

    public class Order
    {
        public string id { get; set; }
        public string productionLineId { get; set; }
        public IO_Order[] inputs { get; set; }
        public IO_Order[] outputs { get; set; }
        public StdTimestamp created { get; set; }
        public StdTimestamp started { get; set; }
        public StdTimestamp completion { get; set; }
        public Duration duration { get; set; }
        public StdTimestamp lastUpdated { get; set; }
        public float completed { get; set; }
        public bool halted { get; set; }
        public StdPrice productionFee { get; set; }
        public StdCompany productionFeeCollector { get; set; }
        public bool recurring { get; set; }
    }

    public class Duration
    {
        public long millis { get; set; }
    }

    public class IO_Order
    {
        public StdPrice value { get; set; }
        public StdMaterial material { get; set; }
        public int amount { get; set; }
    }

    public class Productiontemplate
    {
        public string id { get; set; }
        public string name { get; set; }
        public IOFactor[] inputFactors { get; set; }
        public IOFactor[] outputFactors { get; set; }
        public float effortFactor { get; set; }
        public float efficiency { get; set; }
        public Duration duration { get; set; }
        public StdPrice productionFeeFactor { get; set; }
        public StdCompany productionFeeCollector { get; set; }
    }

    public class IOFactor
    {
        public StdMaterial material { get; set; }
        public int factor { get; set; }
    }

    public class Efficiencyfactor
    {
        public string type { get; set; }
        public float effectivity { get; set; }
        public float value { get; set; }
        public string expertiseCategory { get; set; }
    }
}
