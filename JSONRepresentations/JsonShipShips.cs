﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.ShipShips
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Ship[] ships { get; set; }
    }

    public class Ship
    {
        public string id { get; set; }
        public string idShipStore { get; set; }
        public string idStlFuelStore { get; set; }
        public string idFtlFuelStore { get; set; }
        public string registration { get; set; }
        public string name { get; set; }
        public StdTimestamp commissioningTime { get; set; }
        public string blueprintNaturalId { get; set; }
        public StdAddress address { get; set; }
#nullable enable
        public string? flightId { get; set; }
#nullable disable
        public float acceleration { get; set; }
        public float thrust { get; set; }
        public float mass { get; set; }
        public float operatingEmptyMass { get; set; }
        public float volume { get; set; }
        public float reactorPower { get; set; }
        public float emitterPower { get; set; }
        public string stlFuelStoreId { get; set; }
        public float stlFuelFlowRate { get; set; }
        public string ftlFuelStoreId { get; set; }
        public StdDuration operatingTimeStl { get; set; }
        public StdDuration operatingTimeFtl { get; set; }
        public float condition { get; set; }
        public StdDuration lastRepair { get; set; }
        public StdMaterialAmount[] repairMaterials { get; set; }
        public string status { get; set; }
    }

}
