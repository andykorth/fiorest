﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.JsonOtherUser
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string username { get; set; }
        public string subscriptionLevel { get; set; }
        public string highestTier { get; set; }
        public bool pioneer { get; set; }
        public bool moderator { get; set; }
        public bool team { get; set; }
        public StdTimestamp created { get; set; }
        public StdCompany company { get; set; }
        public string id { get; set; }
    }
}
