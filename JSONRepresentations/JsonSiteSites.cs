﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.SiteSites
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Site[] sites { get; set; }
    }

    public class Site
    {
        public string siteId { get; set; }
        public StdAddress address { get; set; }
        public StdTimestamp founded { get; set; }
        public int investedPermits { get; set; }
        public int maximumPermits { get; set; }
        public Platform[] platforms { get; set; }
        public Buildoptions buildOptions { get; set; }
        public int area { get; set; }
    }

    public class Buildoptions
    {
        public Option[] options { get; set; }
    }

    public class Option
    {
        public string id { get; set; }
        public string name { get; set; }
        public int area { get; set; }
        public string ticker { get; set; }
        public string expertiseCategory { get; set; }
        public bool needsFertileSoil { get; set; }
        public string type { get; set; }
        public Workforcecapacity[] workforceCapacities { get; set; }
        public Materials materials { get; set; }
    }

    public class Materials
    {
        public StdMaterialAmount[] quantities { get; set; }
    }

    public class Workforcecapacity
    {
        public string level { get; set; }
        public int capacity { get; set; }
    }

    public class Platform
    {
        public string siteId { get; set; }
        public string id { get; set; }
        public Module module { get; set; }
        public int area { get; set; }
        public StdTimestamp creationTime { get; set; }
        public StdMaterialAmount[] reclaimableMaterials { get; set; }
        public StdMaterialAmount[] repairMaterials { get; set; }
        public StdPrice bookValue { get; set; }
        public float condition { get; set; }
        public StdTimestamp lastRepair { get; set; }
    }

    public class Module
    {
        public string id { get; set; }
        public string platformId { get; set; }
        public string reactorId { get; set; }
        public string reactorName { get; set; }
        public string reactorTicker { get; set; }
        public string type { get; set; }
    }

}
