﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.Channel.MessageDeleted
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string messageId { get; set; }
        public string type { get; set; }
        public StdUser sender { get; set; }
        public string message { get; set; }
        public StdTimestamp time { get; set; }
        public string channelId { get; set; }
        public StdUser deletingUser { get; set; }
    }

}
