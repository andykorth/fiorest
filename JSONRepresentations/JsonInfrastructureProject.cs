﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.InfrastructureProject
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string type { get; set; }
        public string projectIdentifier { get; set; }
        public int level { get; set; }
        public int activeLevel { get; set; }
        public int currentLevel { get; set; }
        public Upgradecost[] upgradeCosts { get; set; }
        public float upgradeStatus { get; set; }
        public Upkeep[] upkeeps { get; set; }
        public Contribution[] contributions { get; set; }
        public string id { get; set; }
    }

    public class Upgradecost
    {
        public StdMaterial material { get; set; }
        public int amount { get; set; }
        public int currentAmount { get; set; }
    }

    public class Upkeep
    {
        public int stored { get; set; }
        public int storeCapacity { get; set; }
        public int duration { get; set; }
        public StdTimestamp nextTick { get; set; }
        public StdMaterial material { get; set; }
        public int amount { get; set; }
        public int currentAmount { get; set; }
    }

    public class Contribution
    {
        public StdMaterialAmount[] materials { get; set; }
        public StdTimestamp time { get; set; }
        public StdCompany contributor { get; set; }
    }

}
