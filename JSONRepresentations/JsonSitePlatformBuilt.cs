﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.SitePlatformBuilt
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string siteId { get; set; }
        public string id { get; set; }
        public Module module { get; set; }
        public int area { get; set; }
        public StdTimestamp creationTime { get; set; }
        public StdMaterialAmount[] reclaimableMaterials { get; set; }
        public StdMaterialAmount[] repairMaterials { get; set; }
        public StdPrice bookValue { get; set; }
        public float condition { get; set; }
        public StdTimestamp lastRepair { get; set; }
    }

    public class Module
    {
        public string id { get; set; }
        public string platformId { get; set; }
        public string reactorId { get; set; }
        public string reactorName { get; set; }
        public string reactorTicker { get; set; }
        public string type { get; set; }
    }

}
