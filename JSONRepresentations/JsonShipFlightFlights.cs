﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.ShipFlightFlights
{

    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Flight[] flights { get; set; }
    }

    public class Flight
    {
        public string id { get; set; }
        public string shipId { get; set; }
        public StdAddress origin { get; set; }
        public StdAddress destination { get; set; }
        public StdTimestamp departure { get; set; }
        public StdTimestamp arrival { get; set; }
        public Segment[] segments { get; set; }
        public int currentSegmentIndex { get; set; }
        public float stlDistance { get; set; }
        public float ftlDistance { get; set; }
        public bool aborted { get; set; }
    }

    public class Segment
    {
        public string type { get; set; }
        public StdAddress origin { get; set; }
        public StdTimestamp departure { get; set; }
        public StdAddress destination { get; set; }
        public StdTimestamp arrival { get; set; }
        public float? stlDistance { get; set; }
        public float? stlFuelConsumption { get; set; }
        public Transferellipse transferEllipse { get; set; }
        public float? ftlDistance { get; set; }
        public float? ftlFuelConsumption { get; set; }
    }

    public class Transferellipse
    {
        public Position startPosition { get; set; }
        public Position targetPosition { get; set; }
        public Position center { get; set; }
        public float alpha { get; set; }
        public float semiMajorAxis { get; set; }
        public float semiMinorAxis { get; set; }
    }

    public class Position
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }

}
