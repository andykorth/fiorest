﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.ProductionOrderUpdated
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string id { get; set; }
        public string productionLineId { get; set; }
        public IO_Order[] inputs { get; set; }
        public IO_Order[] outputs { get; set; }
        public StdTimestamp created { get; set; }
        public StdTimestamp started { get; set; }
        public StdTimestamp completion { get; set; }
        public Duration duration { get; set; }
        public StdTimestamp lastUpdated { get; set; }
        public float completed { get; set; }
        public bool halted { get; set; }
        public StdPrice productionFee { get; set; }
        public StdCompany productionFeeCollector { get; set; }
        public bool recurring { get; set; }
    }

    public class Duration
    {
        public long millis { get; set; }
    }

    public class IO_Order
    {
        public StdPrice value { get; set; }
        public StdMaterial material { get; set; }
        public int amount { get; set; }
    }
}
