﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.Station
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string naturalId { get; set; }
        public string name { get; set; }
        public StdAddress address { get; set; }
        public StdTimestamp commissioningTime { get; set; }
        public StdEntity comex { get; set; }
        public string warehouseId { get; set; }
        public StdEntity country { get; set; }
        public StdCurrency currency { get; set; }
        public StdUser governor { get; set; }
        public StdEntity governingEntity { get; set; }
        public string id { get; set; }
    }

}
