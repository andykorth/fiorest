using System.Collections.Generic;
using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.ForexCurrencyPairs
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Base    {
        public int numericCode { get; set; } 
        public string code { get; set; } 
        public string name { get; set; } 
        public int decimals { get; set; } 
    }

    public class Quote    {
        public int numericCode { get; set; } 
        public string code { get; set; } 
        public string name { get; set; } 
        public int decimals { get; set; } 
    }

    public class Pair2    {
        public Base @base { get; set; } 
        public Quote quote { get; set; } 
        public int decimals { get; set; } 
    }

    public class Open    {
        public string @base { get; set; } 
        public string quote { get; set; } 
        public double rate { get; set; } 
        public int decimals { get; set; } 
    }

    public class Low    {
        public string @base { get; set; } 
        public string quote { get; set; } 
        public double rate { get; set; } 
        public int decimals { get; set; } 
    }

    public class High    {
        public string @base { get; set; } 
        public string quote { get; set; } 
        public double rate { get; set; } 
        public int decimals { get; set; } 
    }

    public class Previous    {
        public string @base { get; set; } 
        public string quote { get; set; } 
        public double rate { get; set; } 
        public int decimals { get; set; } 
    }

    public class Close    {
        public string @base { get; set; } 
        public string quote { get; set; } 
        public double rate { get; set; } 
        public int decimals { get; set; } 
    }

    public class Traded    {
        public int amount { get; set; } 
        public string currency { get; set; } 
    }

    public class Volume    {
        public double amount { get; set; } 
        public string currency { get; set; } 
    }

    public class Time    {
        public long timestamp { get; set; } 
    }

    public class Price    {
        public Open open { get; set; } 
        public Low low { get; set; } 
        public High high { get; set; } 
        public Previous previous { get; set; } 
        public Close close { get; set; } 
        public Traded traded { get; set; } 
        public Volume volume { get; set; } 
        public Time time { get; set; } 
    }

    public class Pair    {
        public string brokerId { get; set; } 
        public Pair2 pair { get; set; } 
        public Price price { get; set; } 
    }

    public class Payload    {
        public List<Pair> pairs { get; set; } 
        public string actionId { get; set; } 
        public int status { get; set; } 
        public Message message { get; set; } 
    }

    public class Message    {
        public string messageType { get; set; } 
        public Payload payload { get; set; } 
    }

    public class Rootobject    {
        public string messageType { get; set; } 
        public Payload payload { get; set; } 
    }

}