﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.JsonContractDataChange
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Contract payload { get; set; }
    }

    public class Contract
    {
        public string id { get; set; }
        public string localId { get; set; } // `CONT {localId}` buffer
        public StdTimestamp date { get; set; }
        public string party { get; set; } // `PROVIDER` or `CUSTOMER`
        public Partner partner { get; set; }
        public string status { get; set; }
        public Condition[] conditions { get; set; }
#nullable enable
        public StdTimestamp? extensionDeadline { get; set; }
#nullable disable
        public bool canExtend { get; set; }
        public bool canRequestTermination { get; set; }
        public bool terminationSent { get; set; }
        public bool terminationReceived { get; set; }
        public StdTimestamp dueDate { get; set; }
#nullable enable
        public string? name { get; set; }
#nullable disable
        public string preamble { get; set; }
        public bool agentContract { get; set; }
        public string[] realtedContracts { get; set; }
        public string contractType { get; set; }
    }

    public class Partner
    {
        // unconfirmed format for partner data.
#nullable enable
        public string? agentId { get; set; }
        public string? id { get; set; }
        public string? countryId { get; set; }
        public string? countryCode { get; set; }
        public string? code { get; set; }
        public string? type { get; set; }
#nullable disable
        public string name { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Condition
    {
#nullable enable
        public StdAddress? address { get; set; }
        public StdMaterialAmount? quantity { get; set; } // null if shipment
        public string? blockId { get; set; }
#nullable disable
        public double weight { get; set; }
        public double volume { get; set; }
        public string type { get; set; }
        public string id { get; set; }
        public string party { get; set; }
        public int index { get; set; }
        public string status { get; set; }
        public string[] dependencies { get; set; }
#nullable enable
        public StdDuration? deadlineDuration { get; set; }
        public StdTimestamp? deadline { get; set; }
        public StdPrice? amount { get; set; }
        public StdAddress? destination { get; set; }
        public string? shipmentItemId { get; set; }
        public StdMaterialAmount? pickedUp { get; set; }
#nullable disable
    }
}
