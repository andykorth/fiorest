﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.WarehouseStorages
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Storage[] storages { get; set; }
    }

    public class Storage
    {
        public string warehouseId { get; set; }
        public string storeId { get; set; }
        public int units { get; set; }
        public double weightCapacity { get; set; }
        public double volumeCapacity { get; set; }
        public StdTimestamp nextPayment { get; set; }
        public StdPrice fee { get; set; }
        public StdEntity feeCollector { get; set; }
        public string status { get; set; }
        public StdAddress address { get; set; }
    }

}