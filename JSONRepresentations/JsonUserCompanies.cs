﻿using FIORest.JSONRepresentations.Common;

namespace FIORest.JSONRepresentations.UserCompanies
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string name { get; set; }
        public string code { get; set; }
        public StdTimestamp founded { get; set; }
        public StdUser user { get; set; }
        public StdEntity country { get; set; }
        public StdCorporation corporation { get; set; }
        public Ratingreport ratingReport { get; set; }
        public StdAddress[] siteAddresses { get; set; }
        public Reputation[] reputation { get; set; }
        public string id { get; set; }
    }

    public class Ratingreport
    {
        public string overallRating { get; set; }
        public Subrating[] subRatings { get; set; }
    }

    public class Subrating
    {
        public string score { get; set; }
        public string rating { get; set; }
    }

    public class Reputation
    {
        public StdEntity entityId { get; set; }
        public int reputation { get; set; }
    }

}
