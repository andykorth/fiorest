﻿namespace FIORest.JSONRepresentations.SiteDemolish
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string siteId { get; set; }
        public string platformId { get; set; }
        public string actionId { get; set; }
    }
}
