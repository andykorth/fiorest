﻿#define WRITE_REQUESTS_TO_DISK

using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

using Newtonsoft.Json;

namespace FIORest
{
	public enum LogLevel
	{
		Info,
		Warning,
		Error
	}

	public static class Logger
	{
		public static void Log(string message)
		{
			Log(LogLevel.Info, message);
		}

		public static void Log(LogLevel logLevel, string message)
		{
			switch (logLevel)
			{
				case LogLevel.Info:
					Console.Out.WriteLine($"\t{message}");
					break;
				case LogLevel.Warning:
					Console.Error.WriteLine($"\tWARNING: {message}");
					break;
				case LogLevel.Error:
					Console.Error.WriteLine($"\tERROR: {message}");
					break;
			}
		}

		public static List<string> TempBadRequestsToSuppress = new List<string>
		{
			// "23505: duplicate key value violates unique constraint \"PK_ChatMessages\"",
			// Maybefixed by UpsertChanges in the future?  Maybe this is on Kovus
			"-2063465026", 

			// "Database operation expected to affect 1 row(s) but actually affected 0 row(s). Data may have been modified or deleted since entities were loaded."
			// Should be fixed by UpsertChanges in the future
			"801398246",
		};

		// Hack: For now, only allow certain users to log bad requests
		private static List<string> AllowedUsers = new List<string>()
		{
			"LOWSTRIFE", "SAGANAKI"
		};

		private const string StackFramePattern = @"at FIORest\..*\sin\s.+[/\\](?<File>(.+))\.cs:line\s(?<LineNumber>\d+)$";
		private static Regex StackFrameRegex = new Regex(StackFramePattern, RegexOptions.Compiled);

		public static void LogBadRequest(string body, string UserName, Exception ex)
		{
			if (!AllowedUsers.Contains(UserName.ToUpper()))
			{
				return;
			}

#if WRITE_REQUESTS_TO_DISK
			if (ex == null || ex.Message == null || ex.StackTrace == null)
			{
				return;
			}

			if (String.IsNullOrWhiteSpace(UserName))
			{
				UserName = "UnknownUserName";
			}

			string Message = ex.Message;
			string StackTrace = ex.StackTrace;

			string RootPathFolder = null;
			if (StackTrace != null)
			{
				// Try to find a FIORest stackframe.
				var StackFrames = StackTrace.Split(new String[] { "\r\n", "\\r\\n", "\n", }, StringSplitOptions.RemoveEmptyEntries);
				foreach(var StackFrame in StackFrames)
				{
					var StackFrameTrimmed = StackFrame.Trim();
					MatchCollection mc = StackFrameRegex.Matches(StackFrameTrimmed);
					if (mc.Count == 1)
					{
						RootPathFolder = $"{mc[0].Groups["File"]}_cs_{mc[0].Groups["LineNumber"]}";
						break;
					}
				}
			}

			if (RootPathFolder == null)
			{
				// Fallback to Message HashCode
				RootPathFolder = Message.GetHashCode().ToString();
			}

			if (TempBadRequestsToSuppress.Contains(RootPathFolder))
			{
				// Temporary suppression
				return;
			}

			string FullBadRequestPath = Path.GetFullPath(Globals.Opts.BadRequestPath);
			Directory.CreateDirectory(FullBadRequestPath);

			// Create the directory for the directory for this BadRequest grouping if it doesn't exist.  Also add a Message.txt file to the root of it
			string RequestRootPath = Path.Combine(FullBadRequestPath, RootPathFolder);
			if (!Directory.Exists(RequestRootPath))
			{
				Directory.CreateDirectory(RequestRootPath);
				File.WriteAllText(Path.Combine(RequestRootPath, "Message.txt"), Message);
			}

			// Create the username directory under this path
			string UserNameDirectory = Path.Combine(RequestRootPath, UserName);
			Directory.CreateDirectory(UserNameDirectory);

			// Now create the individual BadRequest directory
			DateTime now = DateTime.UtcNow;
			string ThisBadRequestPath = Path.Combine(UserNameDirectory, $"{now.Year}-{now.Month}-{now.Day}-{now.Hour}-{now.Minute}-{now.Second}-{now.Millisecond}");
			Directory.CreateDirectory(ThisBadRequestPath);
			File.WriteAllText(Path.Combine(ThisBadRequestPath, "ExData.json"), JsonConvert.SerializeObject(ex, Formatting.Indented));
			File.WriteAllText(Path.Combine(ThisBadRequestPath, "Request.json"), body);
#endif // WRITE_REQUESTS_TO_DISK
		}
	}
}
