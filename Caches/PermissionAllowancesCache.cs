﻿using System;
using System.Collections.Generic;
using System.Linq;

using FIORest.Database;
using FIORest.Database.Models;

using Microsoft.EntityFrameworkCore;

namespace FIORest.Caches
{
    public static class PermissionAllowancesCache
    {
        private static MRUCache<string, Dictionary<string, PermissionAllowance>> MRUCache = new MRUCache<string, Dictionary<string, PermissionAllowance>>(512);

        public static Dictionary<string, PermissionAllowance> Get(string UserName)
        {
            UserName = UserName.ToUpper();
            return MRUCache.Get(UserName);
        }

        public static void Set(string UserName, Dictionary<string, PermissionAllowance> Value)
        {
            UserName = UserName.ToUpper();
            MRUCache.Set(UserName, Value);
        }

        public static bool Remove(string UserName)
        {
            UserName = UserName.ToUpper();
            return MRUCache.Remove(UserName);
        }

        public static void Clear() => MRUCache.Clear();

        public static void RemoveGroupFromCache(GroupModel gm)
        {
            var groupUserNames = gm.GroupUsers.Select(gm => gm.GroupUserName.ToUpper());
            MRUCache.RemoveRange(groupUserNames);
        }

        public static Dictionary<string, PermissionAllowance> GetOrCache(string UserName, PRUNDataContext context = null)
        {
            var res = Get(UserName);
            if (res == null)
            {
                res = Cache(UserName, context);
            }

            return res;
        }

        public static Dictionary<string, PermissionAllowance> Cache(string UserName, PRUNDataContext context = null)
        {
            UserName = UserName.ToUpper();
            PRUNDataContext DB = context ?? PRUNDataContext.GetNewContext();

            var GroupsImIn = DB.GroupModels
                .AsNoTracking()
                .Include(gm => gm.GroupUsers)
                .Include(gm => gm.GroupAdmins)
                .Where(gm => gm.GroupUsers.Any(gu => gu.GroupUserName.ToUpper() == UserName))
                .AsSplitQuery()
                .Select(gm => $"{gm.GroupName}-{gm.GroupModelId}")
                .ToList();

            var PermissionsGrantedByGroups = DB.AuthenticationModels
                .AsNoTracking()
                .Include(am => am.Allowances)
                .Select(am => new
                {
                    UserName = am.UserName,
                    Allowances = am.Allowances.Where(a => !String.IsNullOrEmpty(a.GroupNameAndId) && GroupsImIn.Contains(a.GroupNameAndId))
                })
                .Where(a => a.Allowances.Any())
                .ToList();

            var PermissionsGrantedByUser = DB.AuthenticationModels
                .AsNoTracking()
                .Include(am => am.Allowances)
                .Select(am => new
                {
                    UserName = am.UserName,
                    Allowances = am.Allowances.Where(a => !String.IsNullOrEmpty(a.UserName) && (a.UserName == "*" || a.UserName.ToUpper() == UserName))
                })
                .Where(a => a.Allowances.Any())
                .ToList();

            var AllPermissionsGranted = PermissionsGrantedByGroups.Union(PermissionsGrantedByUser);

            var PermissionAllowances = new Dictionary<string, PermissionAllowance>(StringComparer.InvariantCultureIgnoreCase);

            // Always add ourselves with full permissions
            PermissionAllowances.Add(UserName, new PermissionAllowance
            {
                FlightData = true,
                BuildingData = true,
                StorageData = true,
                ProductionData = true,
                WorkforceData = true,
                ExpertsData = true,
                ContractData = true,
                ShipmentTrackingData = true
            });

            foreach (var PermissionsGranted in AllPermissionsGranted)
            {
                var PermissionsGrantedUserName = PermissionsGranted.UserName;
                foreach (var Allowance in PermissionsGranted.Allowances)
                {
                    if (!PermissionAllowances.ContainsKey(PermissionsGrantedUserName))
                    {
                        PermissionAllowances.Add(PermissionsGrantedUserName, Allowance);
                    }
                    else
                    {
                        PermissionAllowances[PermissionsGrantedUserName] = PermissionAllowances[PermissionsGrantedUserName].Merge(Allowance);
                    }
                }
            }

            Set(UserName, PermissionAllowances);

            if (context == null)
            {
                DB.Dispose();
            }

            return PermissionAllowances;
        }
    }
}
