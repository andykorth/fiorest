﻿#if WITH_MODULES
using System.Collections.Generic;
using System.Linq;

using Nancy;

using Newtonsoft.Json;
using FIORest.Database;
using FIORest.Database.Models;

using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class RecipesModule : NancyModule
    {
        public RecipesModule() : base("/recipes")
        {
            this.Cacheable();

            Get("/allrecipes", _ =>
            {
                return GetRecipes();
            });

            Get("/{ticker}", parameters =>
            {
                return GetRecipesForTicker(parameters.ticker);
            });
        }

        class MinimalInput
        {
            public string Ticker { get; set; }
            public int Amount { get; set; }
        }

        class MinimalOutput
        {
            public string Ticker { get; set; }
            public int Amount { get; set; }
        }

        class MinimalRecipe
        {
            public string BuildingTicker { get; set; }
            public string RecipeName { get; set; }
            public string StandardRecipeName { get; set; }
            public List<MinimalInput> Inputs { get; set; } = new List<MinimalInput>();
            public List<MinimalOutput> Outputs { get; set; } = new List<MinimalOutput>();
            public int TimeMs { get; set; }
        }

        private Response GetRecipes()
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var buildings = DB.Buildings
                    .AsNoTracking()
                    .Include(b => b.Recipes)
                        .ThenInclude(r => r.Inputs)
                    .Include(b => b.Recipes)
                        .ThenInclude(r => r.Outputs)
                    .AsSplitQuery()
                    .ToList();

                List<MinimalRecipe> minimalRecipes = new List<MinimalRecipe>();

                foreach (var building in buildings)
                {
                    foreach (var recipe in building.Recipes)
                    {
                        MinimalRecipe mr = new MinimalRecipe();
                        mr.BuildingTicker = building.Ticker;
                        mr.RecipeName = recipe.RecipeName;
                        mr.StandardRecipeName = recipe.StandardRecipeName;

                        foreach (var input in recipe.Inputs)
                        {
                            MinimalInput mi = new MinimalInput();
                            mi.Ticker = input.CommodityTicker;
                            mi.Amount = input.Amount;
                            mr.Inputs.Add(mi);
                        }

                        foreach (var output in recipe.Outputs)
                        {
                            MinimalOutput mo = new MinimalOutput();
                            mo.Ticker = output.CommodityTicker;
                            mo.Amount = output.Amount;
                            mr.Outputs.Add(mo);
                        }

                        mr.TimeMs = recipe.DurationMs;
                        minimalRecipes.Add(mr);
                    }
                }

                return JsonConvert.SerializeObject(minimalRecipes);
            }
        }

        class RecipeForTickerPayload : BuildingRecipe
        {
            public string BuildingTicker { get; set; }
        }

        private Response GetRecipesForTicker(string ticker)
        {
            ticker = ticker.ToUpper();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var results = new List<RecipeForTickerPayload>();

                var buildings = DB.Buildings
                    .AsNoTracking()
                    .Include(b => b.Recipes)
                        .ThenInclude(r => r.Inputs)
                    .Include(b => b.Recipes)
                        .ThenInclude(r => r.Outputs)
                    .AsSplitQuery()
                    .ToList();
                foreach (var building in buildings)
                {
                    var recipeInputs = building.Recipes
                        .SelectMany(r => r.Inputs)
                        .Where(i => i.CommodityTicker == ticker);
                    foreach (var recipeInput in recipeInputs)
                    {
                        var payload = new RecipeForTickerPayload();
                        payload.BuildingTicker = recipeInput.BuildingRecipe.Building.Ticker;
                        payload.RecipeName = recipeInput.BuildingRecipe.RecipeName;
                        payload.Inputs = recipeInput.BuildingRecipe.Inputs;
                        payload.Outputs = recipeInput.BuildingRecipe.Outputs;
                        payload.DurationMs = recipeInput.BuildingRecipe.DurationMs;
                        results.Add(payload);
                    }

                    var recipeOutputs = building.Recipes
                        .SelectMany(r => r.Outputs)
                        .Where(o => o.CommodityTicker == ticker);
                    foreach (var recipeOutput in recipeOutputs)
                    {
                        var payload = new RecipeForTickerPayload();
                        payload.BuildingTicker = recipeOutput.BuildingRecipe.Building.Ticker;
                        payload.RecipeName = recipeOutput.BuildingRecipe.RecipeName;
                        payload.StandardRecipeName = recipeOutput.BuildingRecipe.StandardRecipeName;
                        payload.Inputs = recipeOutput.BuildingRecipe.Inputs;
                        payload.Outputs = recipeOutput.BuildingRecipe.Outputs;
                        payload.DurationMs = recipeOutput.BuildingRecipe.DurationMs;
                        results.Add(payload);
                    }
                }

                return JsonConvert.SerializeObject(results);
            }
        }
    }
}
#endif // WITH_MODULES