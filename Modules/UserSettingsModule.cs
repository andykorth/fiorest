﻿#if WITH_MODULES
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class UserSettingsModule : NancyModule
    {
        public UserSettingsModule() : base("/usersettings")
        {
            Get("/general", _ =>
            {
                this.EnforceReadAuth();
                return GetGeneral();
            });

            Get("/general/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetGeneral(parameters.user_name);
            });

            Post("/general", parameters =>
            {
                this.EnforceWriteAuth();
                return PostGeneral();
            });

            Get("/burnrate", parameters =>
            {
                this.EnforceReadAuth();
                return GetBurnRate_UserSettings();
            });

            Get("/burnrate/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetBurnRate_UserSettings(parameters.user_name);
            });

            Get("/burnrate/{user_name}/{planet_natural_id}", parameters =>
            {
                this.EnforceReadAuth();
                return GetBurnRate_UserSettings(parameters.user_name, parameters.planet_natural_id);
            });

            Post("/burnrate/addexclusion", _ =>
            {
                this.EnforceWriteAuth();
                return PostBurnRate_AddExclusion();
            });

            Post("/burnrate/deleteexclusion", _ =>
            {
                this.EnforceWriteAuth();
                return PostBurnRate_DeleteExclusion();
            });

            Post("/burnrate/yellowthreshold", _ =>
            {
                this.EnforceWriteAuth();
                return null;
            });

            Post("/burnrate/redthreshold", _ =>
            {
                this.EnforceWriteAuth();
                return null;
            });
        }

        private Response GetGeneral(string UserName = null)
        {
            return null;
        }

        private Response PostGeneral()
        {
            return null;
        }

        private Response GetBurnRate_UserSettings(string UserName = null, string PlanetNaturalId = null)
        {
            string RequesterUserName = Request.GetUserName();
            if (UserName == null)
            {
                UserName = RequesterUserName;
            }
            else
            {
                UserName = UserName.ToUpper();
            }

            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Workforce) && Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var userSettings = DB.UserSettings
                        .AsNoTracking()
                        .Include(us => us.BurnRateSettings)
                            .ThenInclude(brs => brs.MaterialExclusions)
                        .Where(usm => usm.UserName.ToUpper() == UserName)
                        .FirstOrDefault();
                    if (userSettings == null)
                    {
                        // Create an empty user settings model
                        userSettings = new UserSettings();
                    }

                    if (PlanetNaturalId != null)
                    {
                        PlanetNaturalId = PlanetNaturalId.ToUpper();
                        var burnRateSetting = userSettings.BurnRateSettings.Where(brs => brs.PlanetNaturalId.ToUpper() == PlanetNaturalId).FirstOrDefault();
                        if (burnRateSetting == null)
                        {
                            // Create an empy burnRate settings for this planet
                            burnRateSetting = new UserSettingsBurnRate();
                            burnRateSetting.PlanetNaturalId = PlanetNaturalId;
                        }

                        return JsonConvert.SerializeObject(burnRateSetting);
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(userSettings.BurnRateSettings);
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private class BurnRateExclusion
        {
            public string PlanetNaturalId { get; set; }
            public string MaterialTicker { get; set; }
        }

        private Response PostBurnRate_AddExclusion()
        {
            using (var req = new FIORequest<BurnRateExclusion>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                string PlanetNaturalId = req.JsonPayload.PlanetNaturalId.ToUpper();
                string MaterialTicker = req.JsonPayload.MaterialTicker.ToUpper();

                if (null == req.DB.Materials.Where(m => m.Ticker.ToUpper() == MaterialTicker).FirstOrDefault())
                {
                    // Material doesn't exist
                    return req.ReturnBadRequest();
                }

                var model = req.DB.UserSettings
                    .AsNoTracking() // Don't track, since we'll be upserting anyhow
                    .Include(us => us.BurnRateSettings)
                        .ThenInclude(brs => brs.MaterialExclusions)
                    .Where(usm => usm.UserName.ToUpper() == req.UserName)
                    .AsSplitQuery()
                    .FirstOrDefault();

                bool bShouldAddUserSettingsModel = (model == null);
                if (bShouldAddUserSettingsModel)
                {
                    model = new UserSettings();
                    model.UserSettingsId = req.UserName;
                    model.UserName = req.UserName;
                }

                var userSettingsBurnRate = model.BurnRateSettings.Where(brs => brs.PlanetNaturalId.ToUpper() == PlanetNaturalId).FirstOrDefault();
                bool bAddUserSettingsBurnRate = (userSettingsBurnRate == null);
                if (bAddUserSettingsBurnRate)
                {
                    userSettingsBurnRate = new UserSettingsBurnRate();
                    userSettingsBurnRate.UserSettingsBurnRateId = $"{model.UserSettingsId}-{req.JsonPayload.PlanetNaturalId}";
                    userSettingsBurnRate.PlanetNaturalId = req.JsonPayload.PlanetNaturalId;
                    userSettingsBurnRate.UserSettingsId = model.UserSettingsId;
                }

                var materialExclusion = userSettingsBurnRate.MaterialExclusions.Where(me => me.MaterialTicker.ToUpper() == MaterialTicker).FirstOrDefault();
                if (materialExclusion == null)
                {
                    materialExclusion = new UserSettingsBurnRateExclusion();
                    materialExclusion.UserSettingsBurnRateExclusionId = $"{userSettingsBurnRate.UserSettingsBurnRateId}-{MaterialTicker}";
                    materialExclusion.MaterialTicker = MaterialTicker;
                    materialExclusion.UserSettingsBurnRateId = userSettingsBurnRate.UserSettingsBurnRateId;
                    userSettingsBurnRate.MaterialExclusions.Add(materialExclusion);
                }

                if (bAddUserSettingsBurnRate)
                {
                    model.BurnRateSettings.Add(userSettingsBurnRate);
                }

                model.Validate();

                req.DB.UserSettings.Upsert(model)
                    .On(us => new { us.UserSettingsId })
                    .Run();

                req.DB.UserSettingsBurnRates.UpsertRange(model.BurnRateSettings)
                    .On(brs => new { brs.UserSettingsBurnRateId })
                    .Run();

                var exclusions = model.BurnRateSettings.SelectMany(brs => brs.MaterialExclusions);
                req.DB.UserSettingsBurnRateExclusions.UpsertRange(exclusions)
                    .On(e => new { e.UserSettingsBurnRateExclusionId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostBurnRate_DeleteExclusion()
        {
            using (var req = new FIORequest<BurnRateExclusion>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                string PlanetNaturalId = req.JsonPayload.PlanetNaturalId.ToUpper();
                string MaterialTicker = req.JsonPayload.MaterialTicker.ToUpper();

                var model = req.DB.UserSettings.Where(usm => usm.UserName.ToUpper() == req.UserName).FirstOrDefault();
                if (model != null)
                {
                    var planetBurnRateSettings = model.BurnRateSettings.Where(brs => brs.PlanetNaturalId.ToUpper() == PlanetNaturalId).FirstOrDefault();
                    if (planetBurnRateSettings != null)
                    {
                        planetBurnRateSettings.MaterialExclusions.RemoveAll(me => me.MaterialTicker.ToUpper() == MaterialTicker);
                        req.DB.SaveChanges();
                    }
                }

                return HttpStatusCode.OK;
            }
        }
    }
}
#endif // WITH_MODULES