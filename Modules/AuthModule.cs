#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;
using Nancy;

using Newtonsoft.Json;

namespace FIORest.Modules
{
    public class LoginPayload
    {
        public string UserName;
        public string Password;
    }

    public class LoginResponsePayload
    {
        public string AuthToken;
        public DateTime Expiry;
        public bool IsAdministrator;
    }

    public class ChangePasswordPayload
    {
        public string OldPassword;
        public string NewPassword;
    }

    public class AuthModule : NancyModule
    {
        public AuthModule() : base("/auth")
        {
            Get("/", parameters =>
            {
                if (Request.IsReadAuthenticated())
                {
                    Response resp = Request.GetUserName();
                    resp.ContentType = "text/plain";
                    resp.StatusCode = HttpStatusCode.OK;
                    return resp;
                }
                else return HttpStatusCode.Unauthorized;
            });

            Post("/login", _ =>
            {
                return PostLogin();
            });

            Post("/refreshauthtoken", _ =>
            {
                return PostRefreshAuthToken();
            });

            Post("/changepassword", _ =>
            {
                return PostChangePassword();
            });

            Post("/addpermission", _ =>
            {
                return PostAddPermission();
            });

            Post("/deletepermission/{username}", parameters =>
            {
                return PostDeletePermission(parameters.username);
            });

            Post("/deletepermission/groupid/{groupid:int}", parameters =>
            {
                return PostDeletePermissionGroupId(parameters.groupid);
            });

            Post("/autorequestregister", _ =>
            {
                return PostAutoRequestRegister();
            });

            Post("/register", _ =>
            {
                return PostRegister();
            });

            Post("/createapikey", _ =>
            {
                return PostCreateAPIKey();
            });

            Post("/revokeapikey", _ =>
            {
                return PostRevokeAPIKey();
            });

            Get("/listapikeys", _ =>
            {
                this.EnforceWriteAuth();
                return GetListAPIKeys();
            });

            Post("/listapikeys", _ =>
            {
                return PostListAPIKeys();
            });

            Get("/permissions", _ =>
            {
                return GetPermissions();
            });

            Get("/visibility", _ =>
            {
                return GetVisibility();
            });

            Get("/visibility/{permissiontype}", parameters =>
            {
                return GetVisibility(parameters.permissiontype);
            });

            Post("/creategroup", _ =>
            {
                this.EnforceWriteAuth();
                return PostCreateGroup();
            });

            Post("/deletegroup/{groupid:int}", parameters =>
            {
                this.EnforceWriteAuth();
                return PostDeleteGroup(parameters.groupid);
            });

            Get("/groups", _ =>
            {
                this.EnforceReadAuth();
                return GetMyGroups();
            });

            Get("/groupmemberships", _ =>
            {
                this.EnforceReadAuth();
                return GetGroupMemberships();
            });

            Get("/group/{groupid:int}", parameters =>
            {
                return GetGroup(parameters.groupid);
            });

            Post("/discordid", _ =>
            {
                this.EnforceWriteAuth();
                return SetDiscordId();
            });

            Get("/discordid/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetDiscordId(parameters.username);
            });
        }

        private Response PostLogin()
        {
            // Automatically sleep for a random amount of time between 0.5 & 1.5 sec
            //Random random = new Random();
            //var mseconds = random.Next(500,1500);
            //System.Threading.Thread.Sleep(mseconds);

            using (var req = new FIORequest<LoginPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                if (req.JsonPayload == null || String.IsNullOrWhiteSpace(req.JsonPayload.UserName) || String.IsNullOrWhiteSpace(req.JsonPayload.Password))
                {
                    return req.ReturnBadRequest();
                }

                var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == req.JsonPayload.UserName.ToUpper()).FirstOrDefault();
                if (result != null)
                {
                    // First check for failed attempts brute force
                    DateTime fiveMinutesAgo = req.Now.AddMinutes(-5);
                    var fwfor = req.Request.Headers["X-Forwarded-For"];
                    var remoteAddress = fwfor.Any() ? fwfor.FirstOrDefault() : req.Request.UserHostAddress;
                    var failedAttempts = result.FailedAttempts.Where(f => f.FailedAttemptDateTime > fiveMinutesAgo && f.Address == remoteAddress).ToList();
                    if (failedAttempts.Count > 5)
                    {
                        Response resp = "Too many failed attempts. Locked out.";
                        resp.ContentType = "text/plain";
                        resp.StatusCode = HttpStatusCode.Unauthorized;
                        return resp;
                    }

                    if (!result.AccountEnabled)
                    {
                        Response resp = "Account disabled.";
                        resp.ContentType = "text/plain";
                        resp.StatusCode = HttpStatusCode.Unauthorized;
                        return resp;
                    }

                    bool bPasswordValid = SecurePasswordHasher.Verify(req.JsonPayload.Password, result.PasswordHash);
                    if (bPasswordValid)
                    {
                        if (req.Now < result.AuthorizationExpiry)
                        {
                            result.AuthorizationExpiry = req.Now + new TimeSpan(1, 0, 0, 0);
                        }
                        else
                        {
                            Caches.AuthTokenCache.Remove(result.AuthorizationKey);
                            result.AuthorizationKey = Guid.NewGuid();
                            result.AuthorizationExpiry = req.Now + new TimeSpan(1, 0, 0, 0);
                        }

                        Caches.AuthTokenCache.Set(result.AuthorizationKey, result);

                        LoginResponsePayload response = new LoginResponsePayload();
                        response.AuthToken = result.AuthorizationKey.ToString("N");
                        response.Expiry = result.AuthorizationExpiry;
                        response.IsAdministrator = result.IsAdministrator;

                        req.DB.SaveChanges();

                        Response resp = JsonConvert.SerializeObject(response);
                        resp.ContentType = "application/json";
                        resp.Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } };
                        return resp;
                    }
                    else
                    {
                        result.FailedAttempts.Add(new FailedLoginAttempt { FailedAttemptDateTime = req.Now, Address = remoteAddress });
                        req.DB.SaveChanges();
                        return HttpStatusCode.Unauthorized;
                    }
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        private Response PostRefreshAuthToken()
        {
            if (Request.IsReadAuthenticated())
            {
                using (var DB = PRUNDataContext.GetNewContext())
                using (var transaction = DB.Database.BeginTransaction())
                {
                    var RequestAuthToken = new Guid(Request.GetAuthToken());
                    var result = DB.AuthenticationModels.Where(e => e.AuthorizationKey == RequestAuthToken).FirstOrDefault();
                    if (result != null)
                    {
                        result.AuthorizationExpiry = DateTime.UtcNow + new TimeSpan(1, 0, 0, 0);
                        DB.SaveChanges();
                        transaction.Commit();

                        Caches.AuthTokenCache.Set(result.AuthorizationKey, result);

                        return HttpStatusCode.OK;
                    }
                }

                return HttpStatusCode.BadRequest;
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostChangePassword()
        {
            if (Request.IsWriteAuthenticated())
            {
                using (var req = new FIORequest<ChangePasswordPayload>(Request))
                {
                    if (req.BadRequest)
                    {
                        return req.ReturnBadRequest();
                    }

                    var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == req.UserName.ToUpper()).FirstOrDefault();
                    if (result != null)
                    {
                        if (SecurePasswordHasher.Verify(req.JsonPayload.OldPassword, result.PasswordHash))
                        {
                            result.PasswordHash = SecurePasswordHasher.Hash(req.JsonPayload.NewPassword);
                            req.DB.SaveChanges();
                            return HttpStatusCode.OK;
                        }
                        else
                        {
                            return HttpStatusCode.Unauthorized;
                        }
                    }
                    else
                    {
                        return HttpStatusCode.Unauthorized;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostAddPermission()
        {
            if (Request.IsWriteAuthenticated())
            {
                using (var req = new FIORequest<PermissionAllowance>(Request))
                {
                    if (req.BadRequest)
                    {
                        return req.ReturnBadRequest();
                    }

                    var setPermissionPayload = req.JsonPayload;

                    string GroupName = null;
                    int GroupId = 0;

                    int HyphenIndex = !String.IsNullOrEmpty(setPermissionPayload.GroupNameAndId) ? setPermissionPayload.GroupNameAndId.LastIndexOf('-') : -1;
                    if (HyphenIndex != -1)
                    {
                        GroupName = setPermissionPayload.GroupNameAndId.Substring(0, HyphenIndex);
                        int.TryParse(setPermissionPayload.GroupNameAndId.Substring(HyphenIndex+1), out GroupId);
                    }

                    bool UserInPayloadExists = !String.IsNullOrEmpty(setPermissionPayload.UserName) && (req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == setPermissionPayload.UserName.ToUpper()).FirstOrDefault() != null);
                    bool GroupInPayloadExists = GroupId > 0 && (req.DB.GroupModels.Where(gm => gm.GroupModelId == GroupId).FirstOrDefault() != null);
                    if (setPermissionPayload.UserName == "*" || UserInPayloadExists || GroupInPayloadExists)
                    {
                        string UserName = Request.GetUserName();

                        var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == UserName).FirstOrDefault();

                        PermissionAllowance existingPermission = null;
                        if (GroupInPayloadExists)
                        {
                            var Group = req.DB.GroupModels.Where(gm => gm.GroupModelId == GroupId).First();
                            existingPermission = result.Allowances.Where(a => !String.IsNullOrEmpty(a.GroupNameAndId) && a.GroupNameAndId.EndsWith($"-{Group.GroupModelId}")).FirstOrDefault();

                            Caches.PermissionAllowancesCache.RemoveGroupFromCache(Group);
                        }
                        else
                        {
                            existingPermission = result.Allowances.Where(a => !String.IsNullOrEmpty(a.UserName) && a.UserName.ToUpper() == setPermissionPayload.UserName.ToUpper()).FirstOrDefault();

                            if (setPermissionPayload.UserName == "*")
                            {
                                // Special-case: Wipe the entire PermissionAllowancesCache when somebody changes their '*' permission.  No real way around it, unfortunately.
                                Caches.PermissionAllowancesCache.Clear();
                            }
                            else
                            {
                                Caches.PermissionAllowancesCache.Remove(setPermissionPayload.UserName);
                            }
                        }

                        if (existingPermission != null)
                        {
                            existingPermission.FlightData = setPermissionPayload.FlightData;
                            existingPermission.BuildingData = setPermissionPayload.BuildingData;
                            existingPermission.StorageData = setPermissionPayload.StorageData;
                            existingPermission.ProductionData = setPermissionPayload.ProductionData;
                            existingPermission.WorkforceData = setPermissionPayload.WorkforceData;
                            existingPermission.ExpertsData = setPermissionPayload.ExpertsData;
                            existingPermission.ContractData = setPermissionPayload.ContractData;
                            existingPermission.ShipmentTrackingData = setPermissionPayload.ShipmentTrackingData;
                        }
                        else
                        {
                            result.Allowances.Add(setPermissionPayload);
                        }

                        req.DB.SaveChanges();
                        return HttpStatusCode.OK;
                    }
                    else
                    {
                        return HttpStatusCode.NotFound;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostDeletePermission(string PermUserName)
        {
            PermUserName = PermUserName.ToUpper();
            if (Request.IsWriteAuthenticated())
            {
                string UserName = Request.GetUserName();
                if (PermUserName == "*")
                {
                    // Special-case: Wipe the entire PermissionAllowancesCache when somebody changes their '*' permission.  No real way around it, unfortunately.
                    Caches.PermissionAllowancesCache.Clear();
                }
                else
                {
                    Caches.PermissionAllowancesCache.Remove(PermUserName);
                }

                using (var DB = PRUNDataContext.GetNewContext())
                using (var transaction = DB.Database.BeginTransaction())
                {
                    var AuthModelId = DB.AuthenticationModels.Single(e => e.UserName.ToUpper() == UserName).AuthenticationModelId;
                    var Allowance = DB.PermissionAllowances.Where(p => p.UserName.ToUpper() == PermUserName && p.AuthenticationModelId == AuthModelId).FirstOrDefault();
                    if (Allowance != null)
                    {
                        DB.PermissionAllowances.Attach(Allowance);
                        DB.PermissionAllowances.Remove(Allowance);
                        DB.SaveChanges();
                        transaction.Commit();
                        return HttpStatusCode.OK;
                    }
                    else
                    {
                        return HttpStatusCode.NotFound;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response PostDeletePermissionGroupId(int GroupId)
        {
            if (Request.IsWriteAuthenticated())
            {
                string UserName = Request.GetUserName();

                using (var DB = PRUNDataContext.GetNewContext())
                using (var transaction = DB.Database.BeginTransaction())
                {
                    var AuthModelId = DB.AuthenticationModels.Single(e => e.UserName.ToUpper() == UserName).AuthenticationModelId;
                    var Allowance = DB.PermissionAllowances.Where(p => !String.IsNullOrEmpty(p.GroupNameAndId) && p.GroupNameAndId.EndsWith($"-{GroupId}")&& p.AuthenticationModelId == AuthModelId).FirstOrDefault();
                    if (Allowance != null)
                    {
                        var Group = DB.GroupModels
                            .AsNoTracking()
                            .Include(gm => gm.GroupUsers)
                            .Include(gm => gm.GroupAdmins)
                            .Where(gm => gm.GroupModelId == GroupId)
                            .FirstOrDefault();
                        if (Group != null)
                        {
                            Caches.PermissionAllowancesCache.RemoveGroupFromCache(Group);
                        }

                        DB.PermissionAllowances.Attach(Allowance);
                        DB.PermissionAllowances.Remove(Allowance);
                        DB.SaveChanges();
                        transaction.Commit();
                        return HttpStatusCode.OK;
                    }
                    else
                    {
                        return HttpStatusCode.NotFound;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        class AuthAuthenticated
        {
            public string username { get; set; }
            public bool admin { get; set; }

            public bool IsValid()
            {
                return !String.IsNullOrWhiteSpace(username);
            }
        }

        private Response PostAutoRequestRegister()
        {
            using (var req = new FIORequest<JSONRepresentations.AuthAuthenticated.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var userName = req.JsonPayload.payload.username;
                var ExistingUser = req.DB.AuthenticationModels.Where(a => a.UserName == userName).FirstOrDefault();
                if (ExistingUser == null)
                {
                    // Look for an existing registration
                    var registration = req.DB.Registrations.Where(r => r.UserName == userName).FirstOrDefault();
                    if (registration != null)
                    {
                        // Delete it if it's older than 30 mins
                        DateTime thirtyMinsAgo = req.Now.AddMinutes(-30.0);
                        if (registration.RegistrationTime < thirtyMinsAgo)
                        {
                            req.DB.Registrations.Remove(registration);
                            registration = null;
                        }
                    }

                    if (registration == null)
                    {
                        registration = new Registration();
                        registration.UserName = userName;
                        registration.RegistrationGuid = Guid.NewGuid().ToString("N");
                        registration.RegistrationTime = req.Now;
                        req.DB.Registrations.Add(registration);
                    }

                    req.DB.SaveChanges();
                    return JsonConvert.SerializeObject(registration);
                }

                // Send 204 if the user already exists
                return HttpStatusCode.NoContent;
            }
        }

        class PostRegistration
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string RegistrationGuid { get; set; }

            public bool IsValid()
            {
                return !String.IsNullOrWhiteSpace(UserName) && !String.IsNullOrWhiteSpace(Password) && !String.IsNullOrWhiteSpace(RegistrationGuid);
            }
        }

        private Response PostRegister()
        {
            using (var req = new FIORequest<PostRegistration>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                if (!req.JsonPayload.IsValid())
                {
                    return req.ReturnBadRequest();
                }

                // Make sure the user doesn't exist already
                var existingUser = req.DB.AuthenticationModels.Where(a => a.UserName == req.JsonPayload.UserName).FirstOrDefault();
                if (existingUser != null)
                {
                    return HttpStatusCode.Unauthorized;
                }

                if (req.JsonPayload.Password.Length <= 4)
                {
                    return HttpStatusCode.BadRequest;
                }

                // Look for the registration
                var registration = req.DB.Registrations.Where(r => r.UserName == req.JsonPayload.UserName && r.RegistrationGuid.ToUpper() == req.JsonPayload.RegistrationGuid.ToUpper()).FirstOrDefault();
                if (registration != null)
                {
                    var authModel = new AuthenticationModel();
                    authModel.AccountEnabled = true;
                    authModel.UserName = req.JsonPayload.UserName;
                    authModel.PasswordHash = SecurePasswordHasher.Hash(req.JsonPayload.Password);
                    authModel.IsAdministrator = false;
                    req.DB.AuthenticationModels.Add(authModel);

                    req.DB.Registrations.Remove(registration);

                    req.DB.SaveChanges();
                    return HttpStatusCode.OK;
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        class CreateAPIKeyPayload
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string Application { get; set; }
            public bool AllowWrites { get; set; }

            public bool IsValid()
            {
                return !String.IsNullOrWhiteSpace(Application) && !String.IsNullOrWhiteSpace(UserName) && !String.IsNullOrWhiteSpace(Password);
            }
        }

        private Response PostCreateAPIKey()
        {
            using (var req = new FIORequest<CreateAPIKeyPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                if (!req.JsonPayload.IsValid())
                {
                    return req.ReturnBadRequest();
                }

                if (req.JsonPayload.Application.Length > 255)
                {
                    Response resp = "Application length cannot exceed 255 characters";
                    resp.ContentType = "text/plain";
                    resp.StatusCode = HttpStatusCode.BadRequest;
                    return resp;
                }

                var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == req.JsonPayload.UserName.ToUpper()).FirstOrDefault();
                if (result != null)
                {
                    bool bPasswordValid = SecurePasswordHasher.Verify(req.JsonPayload.Password, result.PasswordHash);
                    if (bPasswordValid)
                    {
                        if (result.APIKeys.Count > 20)
                        {
                            Response resp = "Too many APIKeys";
                            resp.ContentType = "text/plain";
                            resp.StatusCode = HttpStatusCode.NotAcceptable;
                            return resp;
                        }
                        else
                        {
                            var apiKey = new APIKey();
                            apiKey.AuthAPIKey = Guid.NewGuid();
                            apiKey.Application = req.JsonPayload.Application;
                            apiKey.AllowWrites = req.JsonPayload.AllowWrites;
                            apiKey.LastAccessTime = DateTime.UtcNow;
                            result.APIKeys.Add(apiKey);
                            req.DB.SaveChanges();

                            Caches.APIKeyCache.Set(apiKey.AuthAPIKey, apiKey);

                            Response resp = apiKey.AuthAPIKey.ToString("N");
                            resp.ContentType = "text/plain";
                            resp.StatusCode = HttpStatusCode.OK;
                            return resp;
                        }
                    }
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        class RevokeAPIKeyPayload
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string ApiKeyToRevoke { get; set; }

            public bool IsValid()
            {
                return !String.IsNullOrWhiteSpace(UserName) && !String.IsNullOrWhiteSpace(Password) && !String.IsNullOrWhiteSpace(ApiKeyToRevoke);
            }
        }

        private Response PostRevokeAPIKey()
        {
            using (var req = new FIORequest<RevokeAPIKeyPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                if (!req.JsonPayload.IsValid())
                {
                    return req.ReturnBadRequest();
                }

                var result = req.DB.AuthenticationModels.Where(e => e.UserName.ToUpper() == req.JsonPayload.UserName.ToUpper()).FirstOrDefault();
                if (result != null)
                {
                    bool bPasswordValid = SecurePasswordHasher.Verify(req.JsonPayload.Password, result.PasswordHash);
                    if (bPasswordValid)
                    {
                        Guid ApiKeyGuid;
                        if (Guid.TryParse(req.JsonPayload.ApiKeyToRevoke, out ApiKeyGuid))
                        {
                            var apiKey = result.APIKeys.Where(ap => ap.AuthAPIKey == ApiKeyGuid).FirstOrDefault();
                            if (apiKey != null)
                            {
                                req.DB.APIKeys.Remove(apiKey);
                                req.DB.SaveChanges();

                                Caches.APIKeyCache.Remove(ApiKeyGuid);

                                return HttpStatusCode.OK;
                            }
                            else
                            {
                                return HttpStatusCode.NoContent;
                            }
                        }
                    }
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        private Response GetListAPIKeys()
        {
            string UserName = Request.GetUserName();
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var AuthModel = DB.AuthenticationModels
                    .Include(am => am.APIKeys)
                    .AsNoTracking()
                    .AsSplitQuery()
                    .Single(e => e.UserName.ToUpper() == UserName);
                return JsonConvert.SerializeObject(AuthModel.APIKeys);
            }
        }

        private Response PostListAPIKeys()
        {
            using (var req = new FIORequest<LoginPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var result = req.DB.AuthenticationModels
                    .AsNoTracking()
                    .Include(am => am.APIKeys)
                    .Where(e => e.UserName.ToUpper() == req.JsonPayload.UserName.ToUpper())
                    .AsSplitQuery()
                    .FirstOrDefault();
                if (result != null)
                {
                    bool bPasswordValid = SecurePasswordHasher.Verify(req.JsonPayload.Password, result.PasswordHash);
                    if (bPasswordValid)
                    {
                        return JsonConvert.SerializeObject(result.APIKeys.ToList());
                    }
                }

                return HttpStatusCode.Unauthorized;
            }
        }

        private Response GetPermissions()
        {
            if (Request.IsReadAuthenticated())
            {
                string UserName = Request.GetUserName();
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var AuthModel = DB.AuthenticationModels
                        .AsNoTracking()
                        .Include(am => am.Allowances)
                        .AsSplitQuery()
                        .Single(e => e.UserName.ToUpper() == UserName);
                    if (AuthModel.Allowances != null)
                    {
                        return JsonConvert.SerializeObject(AuthModel.Allowances);
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new List<PermissionAllowance>());
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        class PermissionDef
        {
            public string UserName { get; set; }
            public IEnumerable<PermissionAllowance> Allowances { get; set; }
        }

        private Response GetVisibility(string permissionType = null)
        {
            if (Request.IsReadAuthenticated())
            {
                string UserName = Request.GetUserName();
                var AllPermissionsGranted = Caches.PermissionAllowancesCache.GetOrCache(UserName);
                if (AllPermissionsGranted == null)
                {
                    return HttpStatusCode.BadRequest;
                }

                if (permissionType != null)
                {
                    var UserNames = new List<string>();
                    switch (permissionType.ToUpper())
                    {
                        case "FLIGHT":
                            UserNames = AllPermissionsGranted
                                .Where(pg => pg.Value.FlightData)
                                .Select(pg => pg.Key)
                                .OrderBy(a => a)
                                .ToList();
                            break;
                        case "BUILDING":
                            UserNames = AllPermissionsGranted
                                .Where(pg => pg.Value.BuildingData)
                                .Select(pg => pg.Key)
                                .OrderBy(a => a)
                                .ToList();
                            break;
                        case "STORAGE":
                            UserNames = AllPermissionsGranted
                                .Where(pg => pg.Value.StorageData)
                                .Select(pg => pg.Key)
                                .OrderBy(a => a)
                                .ToList();
                            break;
                        case "PRODUCTION":
                            UserNames = AllPermissionsGranted
                                .Where(pg => pg.Value.ProductionData)
                                .Select(pg => pg.Key)
                                .OrderBy(a => a)
                                .ToList();
                            break;
                        case "WORKFORCE":
                            UserNames = AllPermissionsGranted
                                .Where(pg => pg.Value.WorkforceData)
                                .Select(pg => pg.Key)
                                .OrderBy(a => a)
                                .ToList();
                            break;
                        case "EXPERTS":
                            UserNames = AllPermissionsGranted
                                .Where(pg => pg.Value.ExpertsData)
                                .Select(pg => pg.Key)
                                .OrderBy(a => a)
                                .ToList();
                            break;
                        case "CONTRACT":
                        case "CONTRACTS":
                            UserNames = AllPermissionsGranted
                                .Where(pg => pg.Value.ContractData)
                                .Select(pg => pg.Key)
                                .OrderBy(a => a)
                                .ToList();
                            break;
                        case "SHIPMENTTRACKING":
                            UserNames = AllPermissionsGranted
                                .Where(pg => pg.Value.ShipmentTrackingData)
                                .Select(pg => pg.Key)
                                .OrderBy(a => a)
                                .ToList();
                            break;
                        default:
                            return HttpStatusCode.BadRequest;
                    }
                }
                else
                {
                    var AllAllowances = AllPermissionsGranted
                        .Select(pg => new PermissionAllowance
                        {
                            UserName = pg.Key,
                            FlightData = pg.Value.FlightData,
                            BuildingData = pg.Value.BuildingData,
                            StorageData = pg.Value.StorageData,
                            ProductionData = pg.Value.ProductionData,
                            WorkforceData = pg.Value.WorkforceData,
                            ExpertsData = pg.Value.ExpertsData,
                            ContractData = pg.Value.ContractData,
                            ShipmentTrackingData = pg.Value.ShipmentTrackingData
                        })
                        .OrderBy(pa => pa.UserName)
                        .ToList();

                    return JsonConvert.SerializeObject(AllAllowances);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        class CreateGroupPayload
        {
            public string GroupId { get; set; }
            public string GroupName { get; set; }
            public List<string> GroupUsers { get; set; }
            public List<string> GroupAdmins { get; set; }
        }

        class CreateGroupResponse
        {
            public int GroupId { get; set; }
            public string GroupString { get; set; }
        }

        const int GroupModelLimit = 20;
        const int GroupMembershipLimit = 50;

        private const string GroupNameRegexPattern = @"^[a-zA-Z0-9]{1}[a-zA-Z0-9_]+$";
        private static Regex GroupNameRegex = new Regex(GroupNameRegexPattern, RegexOptions.Compiled);

        private Response PostCreateGroup()
        {
            string UserName = Request.GetUserName();
            using (var req = new FIORequest<CreateGroupPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                // Replace spaces with underscores
                req.JsonPayload.GroupName = req.JsonPayload.GroupName.Replace(" ", "_");

                int GroupIdInt = -1;
                if (req.JsonPayload.GroupId != null)
                {
                    if (req.JsonPayload.GroupId.Length <= 8 && !int.TryParse(req.JsonPayload.GroupId, out GroupIdInt))
                    {
                        Response resp = "GroupId specified is invalid.  GroupName must be an integer value less than or equal to 8 digits.";
                        resp.ContentType = "text/plain";
                        resp.StatusCode = HttpStatusCode.NotAcceptable;
                        return req.ReturnBadRequest();
                    }
                }

                if (String.IsNullOrWhiteSpace(req.JsonPayload.GroupName) || !GroupNameRegex.IsMatch(req.JsonPayload.GroupName) || req.JsonPayload.GroupName.Length >= 32)
                {
                    Response resp = "GroupName is invalid. GroupName must not be whitespace, must be alphanumeric or underscores, not have an underscore as the first character, and be less than 32 characters.";
                    resp.ContentType = "text/plain";
                    resp.StatusCode = HttpStatusCode.NotAcceptable;
                    return resp;
                }

                var GroupModelCount = req.DB.GroupModels
                    .Where(g => g.GroupOwner == UserName)
                    .Count();
                if (GroupModelCount >= GroupModelLimit)
                {
                    Response resp = $"You cannot have more than {GroupModelLimit} groups.";
                    resp.ContentType = "text/plain";
                    resp.StatusCode = HttpStatusCode.NotAcceptable;
                    return resp;
                }

                bool bIsAdd = false;
                GroupModel model = null;
                if (req.JsonPayload.GroupId != null)
                {
                    // This is a edit
                    model = req.DB.GroupModels.FirstOrDefault(g => (g.GroupOwner == UserName || g.GroupAdmins.Any(ga => ga.GroupAdminUserName.ToUpper() == UserName.ToUpper())) && g.GroupModelId == GroupIdInt);
                }
                else
                {
                    if (req.JsonPayload.GroupUsers.Count > GroupMembershipLimit)
                    {
                        Response resp = $"You cannot have more than {GroupMembershipLimit} members in a group.";
                        resp.ContentType = "text/plain";
                        resp.StatusCode = HttpStatusCode.NotAcceptable;
                        return resp;
                    }

                    // This is a new group
                    model = new GroupModel();
                    bIsAdd = true;
                }

                if (model.GroupModelId == 0)
                {
                    GroupModel existingGroupWithId = null;
                    do
                    {
                        var bytes = new byte[4];
                        var rng = RandomNumberGenerator.Create();
                        rng.GetBytes(bytes);
                        uint random = BitConverter.ToUInt32(bytes, 0) % 100000000;
                        model.GroupModelId = int.Parse(String.Format("{0:D8}", random));
                        existingGroupWithId = req.DB.GroupModels.FirstOrDefault(g => g.GroupModelId == model.GroupModelId);
                    } while (existingGroupWithId != null);
                }

                model.GroupName = req.JsonPayload.GroupName;
                if (req.JsonPayload.GroupId == null)
                {
                    // Only modify GroupOwner on create...
                    model.GroupOwner = UserName;
                }

                // Get only the valid group usernames
                var groupUserEntries = (
                    from AuthModel in req.DB.AuthenticationModels.AsEnumerable()
                    join GroupUser in req.JsonPayload.GroupUsers
                        on AuthModel.UserName.ToUpper() equals GroupUser.ToUpper()
                    select GroupUser.ToUpper())
                    .Distinct()
                    .Select(GroupUser => new GroupUserEntryModel { GroupUserName = GroupUser.ToUpper() })
                    .ToList();

                model.GroupUsers.Clear();
                model.GroupUsers.AddRange(groupUserEntries);

                var groupAdminUserEntries = (
                    from AuthModel in req.DB.AuthenticationModels.AsEnumerable()
                    join AdminUser in req.JsonPayload.GroupAdmins
                        on AuthModel.UserName.ToUpper() equals AdminUser.ToUpper()
                    select AdminUser.ToUpper())
                    .Distinct()
                    .Select(AdminUser => new GroupModelAdmin { GroupAdminUserName = AdminUser.ToUpper() })
                    .ToList();

                model.GroupAdmins.Clear();
                model.GroupAdmins.AddRange(groupAdminUserEntries);

                model.Validate();

                Caches.PermissionAllowancesCache.RemoveGroupFromCache(model);

                if (bIsAdd)
                {
                    req.DB.GroupModels.Add(model);
                }

                req.DB.SaveChanges();

                var respObj = new CreateGroupResponse();
                respObj.GroupId = model.GroupModelId;
                respObj.GroupString = $"{respObj.GroupId}-{model.GroupName}";
                return JsonConvert.SerializeObject(respObj);
            }
        }

        private Response PostDeleteGroup(int groupid)
        {
            string UserName = Request.GetUserName();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var model = DB.GroupModels
                    .Include(gm => gm.GroupUsers)
                    .Include(gm => gm.GroupAdmins)
                    .FirstOrDefault(gm => gm.GroupOwner == UserName && gm.GroupModelId == groupid);
                if (model != null)
                {
                    Caches.PermissionAllowancesCache.RemoveGroupFromCache(model);

                    DB.Database.BeginTransaction();

                    // Remove all permission allowances referencing the group
                    DB.PermissionAllowances.RemoveRange(DB.PermissionAllowances.Where(pa => pa.GroupNameAndId.EndsWith($"-{groupid}")));

                    // Remove the group
                    DB.GroupModels.Remove(model);

                    DB.Database.CommitTransaction();
                    DB.SaveChanges();
                    return HttpStatusCode.OK;
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetMyGroups()
        {
            string UserName = Request.GetUserName();

            using (var DB = PRUNDataContext.GetNewContext())
            {
                var myGroups = DB.GroupModels
                    .AsNoTracking()
                    .Include(gm => gm.GroupUsers)
                    .Include(gm => gm.GroupAdmins)
                    .Where(gm => gm.GroupOwner.ToUpper() == UserName || gm.GroupAdmins.Any(ga => ga.GroupAdminUserName.ToUpper() == UserName))
                    .AsSplitQuery()
                    .ToList();
                return JsonConvert.SerializeObject(myGroups);
            }
        }

        private Response GetGroupMemberships()
        {
            string UserName = Request.GetUserName();
            
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var myGroupMemberships = DB.GroupUserEntryModels
                    .AsNoTracking()
                    .Where(guem => guem.GroupUserName.ToUpper() == UserName)
                    .Select(guem => guem.GroupModel)
                    .Select(gm => new
                    {
                        GroupName = gm.GroupName,
                        GroupId = gm.GroupModelId
                    })
                    .ToList();

                return JsonConvert.SerializeObject(myGroupMemberships);
            }
        }

        private Response GetGroup(int groupid)
        {
            using (var DB = PRUNDataContext.GetNewContext())
            {
                var group = DB.GroupModels
                    .AsNoTracking()
                    .Include(gm => gm.GroupUsers)
                    .Include(gm => gm.GroupAdmins)
                    .AsSplitQuery()
                    .FirstOrDefault(gm => gm.GroupModelId == groupid);
                if (group != null)
                {
                    return JsonConvert.SerializeObject(group);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        class DiscordIdPayload
        {
            public string DiscordId { get; set; }
        }

        private Response SetDiscordId()
        {
            string UserName = Request.GetUserName();
            using (var req = new FIORequest<DiscordIdPayload>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var discordId = req.JsonPayload.DiscordId;
                if (String.IsNullOrWhiteSpace(discordId) || discordId.Length > 40)
                {
                    return req.ReturnBadRequest();
                }

                var authModel = req.DB.AuthenticationModels.First(am => am.UserName.ToUpper() == UserName);
                authModel.DiscordId = discordId;
                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetDiscordId(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();

            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Storage))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var discordIdPayload = DB.AuthenticationModels
                        .AsNoTracking()
                        .Where(am => am.UserName.ToUpper() == UserName)
                        .Select(am => new DiscordIdPayload
                        {
                            DiscordId = am.DiscordId
                        }).First();

                    return JsonConvert.SerializeObject(discordIdPayload);
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
#endif // WITH_MODULES