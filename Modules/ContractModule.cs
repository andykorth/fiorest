﻿#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class ContractModule : NancyModule
    {
        public ContractModule() : base("/contract")
        {
            Get("/allcontracts", _ =>
            {
                this.EnforceReadAuth();
                return GetAllContracts();
            });

            Get("/allcontracts/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetAllContracts(parameters.user_name);
            });

            Get("/open", _ =>
            {
                this.EnforceReadAuth();
                return GetOpenContracts();
            });

            Get("/open/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetOpenContracts(parameters.user_name);
            });

            Get("/concerns", _ =>
            {
                this.EnforceReadAuth();
                return GetContractConcerns();
            });

            Get("/concerns/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetContractConcerns(parameters.user_name);
            });

            Get("/actionable", _ =>
            {
                this.EnforceReadAuth();
                return GetActionableContracts();
            });

            Get("/actionable/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetActionableContracts(parameters.user_name);
            });

            Get("/loans", _ =>
            {
                this.EnforceReadAuth();
                return GetContractLoans();
            });

            Get("/loans/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetContractLoans(parameters.user_name);
            });

            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostAllContracts();
            });

            Post("/change", _ =>
            {
                this.EnforceWriteAuth();
                return PostContract();
            });

            Get("/shipments", _ =>
            {
                this.EnforceReadAuth();
                return GetShipments();
            });

            Get("/shipments/{user_name}", parameters =>
            {
                this.EnforceReadAuth();
                return GetShipments(parameters.user_name);
            });

            Get("/taco", _ =>
            {
                this.EnforceReadAuth();
                return GetTaco();
            });
        }

        private Response GetAllContracts(string UserName = null)
        {
            if (String.IsNullOrWhiteSpace(UserName))
            {
                UserName = Request.GetUserName();
            }

            if (Auth.CanSeeData(Request.GetUserName(), UserName, Auth.PrivacyType.Contracts))
            {
                using(var DB = PRUNDataContext.GetNewContext())
                {
                    var conts = DB.Contracts
                        .AsNoTracking()
                        .Include(cm => cm.Conditions)
                            .ThenInclude(cond => cond.Dependencies)
                        .Where(c => c.UserNameSubmitted.ToUpper() == UserName.ToUpper())
                        .OrderByDescending(c => c.DateEpochMs)
                        .Take(100)
                        .ToList();
                    return JsonConvert.SerializeObject(conts);
                }
            }
            else
            {
                return HttpStatusCode.Unauthorized;
            }
        }

        private Response GetOpenContracts(string UserName = null)
        {
            if (String.IsNullOrWhiteSpace(UserName))
            {
                UserName = Request.GetUserName();
            }

            if (Auth.CanSeeData(Request.GetUserName(), UserName, Auth.PrivacyType.Contracts))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    long nowEpochMs = DateTime.UtcNow.ToEpochMs();
                    var openContracts = DB.Contracts
                        .AsNoTracking()
                        .Include(cm => cm.Conditions)
                            .ThenInclude(cond => cond.Dependencies)
                        .Where(c => c.UserNameSubmitted.ToUpper() == UserName.ToUpper() && (c.Status != "FULFILLED" && c.Status != "BREACHED") && ((c.DueDateEpochMs != null && c.DueDateEpochMs > nowEpochMs) || (c.ExtensionDeadlineEpochMs != null && c.ExtensionDeadlineEpochMs > nowEpochMs)))
                        .OrderByDescending(c => c.DateEpochMs)
                        .AsSplitQuery()
                        .ToList();
                    return JsonConvert.SerializeObject(openContracts);
                }
            }
            else
            {
                return HttpStatusCode.Unauthorized;
            }
        }

        private Response GetContractConcerns(string UserName = null)
        {
            if (String.IsNullOrWhiteSpace(UserName))
            {
                UserName = Request.GetUserName();
            }

            if (Auth.CanSeeData(Request.GetUserName(), UserName, Auth.PrivacyType.Contracts))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    long nowEpochMs = DateTime.UtcNow.ToEpochMs();
                    long ThresholdEpochMs = DateTime.UtcNow.AddDays(1.0).ToEpochMs();
                    var recentContracts = DB.Contracts
                        .AsNoTracking()
                        .Include(cm => cm.Conditions)
                            .ThenInclude(cond => cond.Dependencies)
                        .Where(c => c.UserNameSubmitted.ToUpper() == UserName.ToUpper() && ((c.DueDateEpochMs != null && c.DueDateEpochMs < ThresholdEpochMs) || c.ExtensionDeadlineEpochMs != null && c.ExtensionDeadlineEpochMs > nowEpochMs))
                        .OrderByDescending(c => c.DateEpochMs)
                        .AsSplitQuery()
                        .ToList();
                    return JsonConvert.SerializeObject(recentContracts);
                }
            }
            else
            {
                return HttpStatusCode.Unauthorized;
            }
        }

        private Response GetActionableContracts(string UserName = null)
        {
            if (String.IsNullOrWhiteSpace(UserName))
            {
                UserName = Request.GetUserName();
            }

            if (Auth.CanSeeData(Request.GetUserName(), UserName, Auth.PrivacyType.Contracts))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    long nowEpochMs = DateTime.UtcNow.ToEpochMs();
                    var recentContracts = DB.Contracts
                        .AsNoTracking()
                        .Include(cm => cm.Conditions)
                            .ThenInclude(cond => cond.Dependencies)
                        .Where(c => c.UserNameSubmitted.ToUpper() == UserName.ToUpper() && (c.Status != "FULFILLED" && c.Status != "BREACHED") && ((c.DueDateEpochMs != null && c.DueDateEpochMs > nowEpochMs) || (c.ExtensionDeadlineEpochMs != null && c.ExtensionDeadlineEpochMs > nowEpochMs)))
                        .Where(c => c.Conditions.Any(cond => (cond.Party == c.Party && cond.Status != "FULFILLED")))
                        .OrderByDescending(c => c.DateEpochMs)
                        .AsSplitQuery()
                        .ToList();
                    return JsonConvert.SerializeObject(recentContracts);
                }
            }
            else
            {
                return HttpStatusCode.Unauthorized;
            }
        }

        public class LoanDetails
        {
            public long LoanTimestampEpochms { get; set; }
            public double Amount { get; set; }
            public string Currency { get; set; }
        }

        public class LoanGroup
        {
            public string CompanyName { get; set; }
            public string UserName { get; set; }
            public string ContractId { get; set; }

            public List<LoanDetails> Details { get; set; } = new List<LoanDetails>();
        }

        private Response GetContractLoans(string UserName = null)
        {
            if (String.IsNullOrWhiteSpace(UserName))
            {
                UserName = Request.GetUserName();
            }

            UserName = UserName.ToUpper();

            if (Auth.CanSeeData(Request.GetUserName(), UserName, Auth.PrivacyType.Contracts))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var allLoans = new List<LoanGroup>();

                    var conts = DB.Contracts
                        .AsNoTracking()
                        .Include(cm => cm.Conditions)
                            .ThenInclude(cond => cond.Dependencies)
                        .Where(c => c.UserNameSubmitted == UserName && c.Status == "FULFILLED")
                        .ToList();

                    foreach (var cont in conts)
                    {
                        if (cont.Conditions.Any(cond => cond.MaterialAmount != null && (int)cond.MaterialAmount == 1 && cond.Type == "PROVISION" && cond.Party == "CUSTOMER"))
                        {
                            var payment = cont.Conditions.Where(cond => cond.Type == "PAYMENT" && cond.Party == "PROVIDER" && cond.Amount != null).FirstOrDefault();
                            if (payment != null)
                            {
                                var lg = allLoans.Where(l => l.CompanyName == cont.PartnerCompanyCode).FirstOrDefault();
                                bool bAdd = (lg == null);
                                if (bAdd)
                                {
                                    lg = new LoanGroup();
                                    lg.CompanyName = cont.PartnerCompanyCode;
                                    lg.UserName = DB.UserData
                                        .Where(ud => ud.CompanyId == cont.PartnerId)
                                        .Select(ud => ud.UserName)
                                        .FirstOrDefault();
                                    lg.ContractId = cont.ContractLocalId;
                                }

                                var ld = new LoanDetails();
                                if ( cont.DateEpochMs != null)
                                {
                                    ld.LoanTimestampEpochms = (long)cont.DateEpochMs;
                                }
                                ld.Amount = (double)payment.Amount;
                                ld.Currency = payment.Currency;
                                lg.Details.Add(ld);

                                if (bAdd)
                                {
                                    allLoans.Add(lg);
                                }
                            }
                        }
                    }

                    return JsonConvert.SerializeObject(allLoans);
                }
            }
            else
            {
                return HttpStatusCode.Unauthorized;
            }
        }

        private Response PostAllContracts()
        {
            using (var req = new FIORequest<JSONRepresentations.JsonContractData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var allContracts = new List<Contract>();

                var data = req.JsonPayload.payload.message.payload;
                // filter the user data down to contracts which are: 
                // - in the last X days;
                // - can be extended (still); or
                // - have had an extension in the specified period.
                var interestingContracts = data.contracts
                    .Where(c => 
                        c.date.timestamp > DateTime.Now.AddDays(-30).ToEpochMs() ||
                        c.canExtend == true ||
                        (
                            c.extensionDeadline != null && 
                            c.extensionDeadline.timestamp > DateTime.Now.AddDays(-30).ToEpochMs()
                        )
                    )
                    .ToList();
                var interestingContractIds = interestingContracts.Select(c => c.id).ToList();
                var savedContracts = req.DB.Contracts
                    .Where(cm => interestingContractIds.Contains(cm.ContractId))
                    .ToList();
                foreach(var contract in interestingContracts)
                {
                    var model = new Contract();
                    model.ContractId = contract.id;
                    model.ContractLocalId = contract.localId;
                    model.DateEpochMs = contract.date?.timestamp;
                    model.ExtensionDeadlineEpochMs = contract.extensionDeadline?.timestamp;
                    model.DueDateEpochMs = contract.dueDate?.timestamp;

                    model.CanExtend = contract.canExtend;
                    model.CanRequestTermination = contract.canRequestTermination;
                    model.TerminationSent = contract.terminationSent;
                    model.TerminationReceived = contract.terminationReceived;

                    model.Name = contract.name;
                    model.Preamble = contract.preamble;

                    model.Party = contract.party;
                    model.Status = contract.status;

                    model.PartnerId = contract.partner?.id;
                    model.PartnerName = contract.partner?.name;
                    model.PartnerCompanyCode = contract.partner?.code;
                    
                    if ( contract.conditions != null )
                    {
                        foreach (var condition in contract.conditions)
                        {
                            ContractCondition cond = new ContractCondition();
                            cond.ContractConditionId = $"{contract.id}-{condition.id}";

                            if (condition.address != null)
                            {
                                if (condition.address.lines[1].entity.naturalId != condition.address.lines[1].entity.name)
                                {
                                    cond.Address = $"{condition.address.lines[1].entity.name} ({condition.address.lines[1].entity.naturalId})";
                                }
                                else
                                {
                                    cond.Address = condition.address.lines[1].entity.naturalId;
                                }
                            }

                            cond.MaterialId = condition.quantity?.material?.id;
                            cond.MaterialTicker = condition.quantity?.material?.ticker;
                            cond.MaterialAmount = condition.quantity?.amount;

                            cond.Weight = condition.weight;
                            cond.Volume = condition.volume;

                            cond.BlockId = condition.blockId;
                            cond.Type = condition.type;
                            cond.ConditionId = condition.id;
                            cond.Party = condition.party;
                            cond.ConditionIndex = condition.index;
                            cond.Status = condition.status;

                            foreach (var dependency in condition.dependencies)
                            {
                                ContractDependency dep = new ContractDependency();
                                dep.ContractDependencyId = $"{cond.ContractConditionId}-{dependency}";
                                dep.Dependency = dependency;
                                dep.ContractConditionId = cond.ContractConditionId;
                                cond.Dependencies.Add(dep);
                            }

                            cond.DeadlineEpochMs = condition.deadline?.timestamp;
                            cond.Amount = condition.amount?.amount;
                            cond.Currency = condition.amount?.currency;

                            if (condition.destination != null)
                            {
                                if (condition.destination.lines[1].entity.naturalId != condition.destination.lines[1].entity.name)
                                {
                                    cond.Destination = $"{condition.destination.lines[1].entity.name} ({condition.destination.lines[1].entity.naturalId})";
                                }
                                else
                                {
                                    cond.Destination = condition.destination.lines[1].entity.naturalId;
                                }
                            }

                            cond.ShipmentItemId = condition.shipmentItemId;
                            cond.PickedUpMaterialId = condition.pickedUp?.material?.id;
                            cond.PickedUpMaterialTicker = condition.pickedUp?.material?.ticker;
                            cond.PickedUpAmount = condition.pickedUp?.amount;
                            cond.ContractId = model.ContractId;

                            model.Conditions.Add(cond);

                            model.UserNameSubmitted = req.UserName;
                            model.Timestamp = req.Now;
                        }
                    }

                    model.Validate();

                    allContracts.Add(model);
                }

                req.DB.Contracts.UpsertRange(allContracts)
                    .On(c => new { c.ContractId })
                    .Run();

                var allConditions = allContracts.SelectMany(c => c.Conditions).ToList();
                req.DB.ContractConditions.UpsertRange(allConditions)
                    .On(cc => new { cc.ContractConditionId })
                    .Run();

                var allDeps = allConditions.SelectMany(c => c.Dependencies).ToList();
                req.DB.ContractDependencies.UpsertRange(allDeps)
                    .On(cd => new { cd.ContractDependencyId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostContract()
        {
            using (var req = new FIORequest<JSONRepresentations.JsonContractDataChange.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var contract = req.JsonPayload.payload;
                if (contract.id == null)
				{
                    // @TODO
                    return HttpStatusCode.OK;
				}
                if (contract.agentContract == true)
                {
                    // @TODO
                    // If I understand correctly, this should be faction contracts
                    // we haven't done any work for those.
                    return HttpStatusCode.OK;
                }

                var model = new Contract();
                model.ContractId = contract.id;
                model.ContractLocalId = contract.localId;
                model.DateEpochMs = contract.date?.timestamp;
                model.ExtensionDeadlineEpochMs = contract.extensionDeadline?.timestamp;
                model.DueDateEpochMs = contract.dueDate?.timestamp;

                model.CanExtend = contract.canExtend;
                model.CanRequestTermination = contract.canRequestTermination;
                model.TerminationSent = contract.terminationSent;
                model.TerminationReceived = contract.terminationReceived;

                model.Name = contract.name;
                model.Preamble = contract.preamble;

                model.Party = contract.party;
                model.Status = contract.status;

                if(contract.partner != null && contract.partner._type == "country-agent")
                {
                    model.PartnerId = contract.partner?.agentId;
                    model.PartnerName = contract.partner?.name;
                    model.PartnerCompanyCode = contract.partner?.countryCode;
                }
                if(contract.partner != null)
                {
                    model.PartnerId = contract.partner?.id;
                    model.PartnerName = contract.partner?.name;
                    model.PartnerCompanyCode = contract.partner?.code;
                }

                if ( contract.conditions != null)
                {
                    foreach (var condition in contract.conditions)
                    {
                        ContractCondition cond = new ContractCondition();
                        cond.ContractConditionId = $"{contract.id}-{condition.id}";
                        if (condition.address != null)
                        {
                            if (condition.address.lines[1].entity.naturalId != condition.address.lines[1].entity.name)
                            {
                                cond.Address = $"{condition.address.lines[1].entity.name} ({condition.address.lines[1].entity.naturalId})";
                            }
                            else
                            {
                                cond.Address = condition.address.lines[1].entity.naturalId;
                            }
                        }

                        cond.MaterialId = condition.quantity?.material?.id;
                        cond.MaterialTicker = condition.quantity?.material?.ticker;
                        cond.MaterialAmount = condition.quantity?.amount;

                        cond.Weight = condition.weight;
                        cond.Volume = condition.volume;

                        cond.BlockId = condition.blockId;
                        cond.Type = condition.type;
                        cond.ConditionId = condition.id;
                        cond.Party = condition.party;
                        cond.ConditionIndex = condition.index;
                        cond.Status = condition.status;

                        foreach (var dependency in condition.dependencies)
                        {
                            ContractDependency dep = new ContractDependency();
                            dep.ContractDependencyId = $"{cond.ContractConditionId}-{dependency}";
                            dep.Dependency = dependency;
                            dep.ContractConditionId = cond.ContractConditionId;
                            cond.Dependencies.Add(dep);
                        }

                        cond.DeadlineEpochMs = condition.deadline?.timestamp;
                        cond.Amount = condition.amount?.amount;
                        cond.Currency = condition.amount?.currency;

                        if (condition.destination != null)
                        {
                            if (condition.destination.lines[1].entity.naturalId != condition.destination.lines[1].entity.name)
                            {
                                cond.Destination = $"{condition.destination.lines[1].entity.name} ({condition.destination.lines[1].entity.naturalId})";
                            }
                            else
                            {
                                cond.Destination = condition.destination.lines[1].entity.naturalId;
                            }
                        }

                        cond.ShipmentItemId = condition.shipmentItemId;
                        cond.PickedUpMaterialId = condition.pickedUp?.material?.id;
                        cond.PickedUpMaterialTicker = condition.pickedUp?.material?.ticker;
                        cond.PickedUpAmount = condition.pickedUp?.amount;
                        cond.ContractId = model.ContractId;

                        model.Conditions.Add(cond);
                    }
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                model.Validate();

                req.DB.Contracts.Upsert(model)
                    .On(m => new { m.ContractId })
                    .Run();

                req.DB.ContractConditions.UpsertRange(model.Conditions)
                    .On(cc => new { cc.ContractConditionId })
                    .Run();

                var deps = model.Conditions.SelectMany(cc => cc.Dependencies).ToList();
                req.DB.ContractDependencies.UpsertRange(deps)
                    .On(d => new { d.ContractDependencyId })
                    .Run();

                req.SaveChangesIgnoreConcurrencyFailures();
                return HttpStatusCode.OK;
            }
        }

        public class ShipmentResult
        {
            public string ContractLocalId { get; set; }
            public string PartnerCompanyCode { get; set; }
            public string ShipmentItemId { get; set; }
            public string PartnerUserName { get; set; }

            public bool IsTracked { get; set; } = false;
            public string Location { get; set; }
            public Flight Flight { get; set; }

            public DateTime LastUpdateTime { get; set; }
        }

        private const string PickupPartySearch = "PROVIDER";
        private const string PickupTypeSearch = "SHIPMENT_PICKUP";
        private const string PickupStatusSearch = "FULFILLED";

        private Response GetShipments(string UserName = null)
        {
            if (String.IsNullOrWhiteSpace(UserName))
            {
                UserName = Request.GetUserName();
            }
            else
            {
                UserName = UserName.ToUpper();
            }

            if (!Auth.CanSeeData(Request.GetUserName(), UserName, Auth.PrivacyType.Contracts))
            {
                return HttpStatusCode.Unauthorized;
            }

            using (var DB = PRUNDataContext.GetNewContext())
            {
                // Limit to UserName's active contracts
                long nowEpochMs = DateTime.UtcNow.ToEpochMs();
                var shipmentContracts = DB.Contracts
                    .AsNoTracking()
                    .Where(c => c.UserNameSubmitted.ToUpper() == UserName && (c.DueDateEpochMs != null || c.CanExtend == true) && (nowEpochMs < (long)c.DueDateEpochMs || (c.ExtensionDeadlineEpochMs != null && nowEpochMs < c.ExtensionDeadlineEpochMs) ))
                    .Include(cm => cm.Conditions)
                        .ThenInclude(cond => cond.Dependencies)
                    // Find contracts where a condition has Party as "PROVIDER" and Type as "SHIPMENT_DELIVERY"
                    // - This means that I'm the person awaiting a delivery of one of my shipments
                    .Where(c => c.Conditions.Any(cond => cond.Party == PickupPartySearch && cond.Type == PickupTypeSearch && cond.Status == PickupStatusSearch))
                    .ToList();

                List<ShipmentResult> res = new List<ShipmentResult>();
                foreach( var shipmentContract in shipmentContracts )
                {
                    // Find the condition where Party is PROVIDER and Type is SHIPMENT_DELIVERY
                    var pickupCondition = shipmentContract.Conditions.Where(cond => cond.Party == PickupPartySearch && cond.Type == PickupTypeSearch && cond.Status == PickupStatusSearch).FirstOrDefault();
                    if (pickupCondition != null)
                    {
                        // We have the pickup condition where the other user has picked up the package. From here, we need to find the condition which depends on *this* one
                        var deliveryCondition = shipmentContract.Conditions.Where(cond => cond.Dependencies.Count == 1 && cond.Dependencies[0].Dependency == pickupCondition.ConditionId && cond.ShipmentItemId != null).FirstOrDefault();
                        if (deliveryCondition != null && ((deliveryCondition.DeadlineEpochMs != null && nowEpochMs < deliveryCondition.DeadlineEpochMs) || shipmentContract.CanExtend ))
                        {
                            ShipmentResult sr = new ShipmentResult();

                            sr.ContractLocalId = shipmentContract.ContractLocalId;
                            sr.PartnerCompanyCode = shipmentContract.PartnerCompanyCode;
                            sr.ShipmentItemId = deliveryCondition.ShipmentItemId;

                            // Now, let's look for the other user by company code
                            var OtherUser = DB.UserData.Where(ud => ud.CompanyCode.ToUpper() == sr.PartnerCompanyCode.ToUpper()).Select(cd => cd.UserName).FirstOrDefault();
                            if ( OtherUser != null )
                            {
                                sr.PartnerUserName = OtherUser;
                                if (Auth.CanSeeData(UserName, OtherUser, Auth.PrivacyType.ShipmentTracking))
                                {
                                    // Find the item
                                    var item = DB.StorageItems.Where(si => si.MaterialId == sr.ShipmentItemId).FirstOrDefault();
                                    if (item != null )
                                    {
                                        var storage = item.Storage;
                                        if (storage.Type == "SHIP_STORE")
                                        {
                                            var ship = DB.Ships.Where(s => s.ShipId == storage.AddressableId).FirstOrDefault();
                                            if (ship != null)
                                            {
                                                var flight = DB.Flights.Where(f => f.ShipId == ship.ShipId).FirstOrDefault();
                                                if ( flight != null)
                                                {
                                                    sr.IsTracked = true;
                                                    sr.Location = null;
                                                    sr.Flight = flight;
                                                    sr.LastUpdateTime = flight.Timestamp;
                                                }
                                            }
                                        }
                                        else if (storage.Type == "WAREHOUSE_STORE")
                                        {
                                            var warehouse = DB.Warehouses.Where(wm => wm.WarehouseId.StartsWith(storage.AddressableId) && wm.UserNameSubmitted == storage.UserNameSubmitted).FirstOrDefault();
                                            if ( warehouse != null )
                                            {
                                                sr.IsTracked = true;
                                                sr.Location = (warehouse.LocationName != warehouse.LocationNaturalId) ? $"{warehouse.LocationName} ({warehouse.LocationNaturalId})" : warehouse.LocationNaturalId;
                                                sr.LastUpdateTime = warehouse.Timestamp;
                                            }
                                        }
                                        else if (storage.Type == "INVENTORY" || storage.Type == "STORE")
                                        {
                                            var site = DB.Sites
                                                .AsNoTracking()
                                                .Include(s => s.Buildings)
                                                    .ThenInclude(b => b.ReclaimableMaterials)
                                                .Include(s => s.Buildings)
                                                    .ThenInclude(b => b.RepairMaterials)
                                                .Where(s => s.SiteId == storage.AddressableId)
                                                .AsSplitQuery()
                                                .FirstOrDefault();
                                            if (site != null)
                                            {
                                                sr.IsTracked = true;
                                                sr.Location = (site.PlanetName != site.PlanetIdentifier) ? $"{site.PlanetName} ({site.PlanetIdentifier})" : site.PlanetIdentifier;
                                                sr.LastUpdateTime = storage.Timestamp;
                                            }
                                        }
                                    }
                                }
                            }

                            res.Add(sr);
                        }
                    }
                }

                return JsonConvert.SerializeObject(res);
            }
        }

        public class TacoResult
        {
            public string BuyerCompanyCode { get; set; }
            public int TacosConsumed { get; set; }
        }

        private Response GetTaco()
        {
            string EatTacos88 = "EATTACOS88";
            if (Auth.CanSeeData(Request.GetUserName(), EatTacos88, Auth.PrivacyType.Contracts))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var resDict = new Dictionary<string, int>();
                    var allRATContracts = DB.Contracts
                        .AsNoTracking()
                        .Where(c => c.UserNameSubmitted.ToUpper() == EatTacos88 && c.Party == "PROVIDER" && c.Status == "FULFILLED")
                        .Include(cm => cm.Conditions)
                        .ToList();
                    foreach (var contract in allRATContracts)
                    {
                        if (contract.Conditions.Count == 2 && contract.Conditions[1].MaterialTicker == "RAT" && contract.PartnerCompanyCode != null)
                        {
                            if (resDict.ContainsKey(contract.PartnerCompanyCode))
                            {
                                resDict[contract.PartnerCompanyCode] += (int)contract.Conditions[1].MaterialAmount;
                            }
                            else
                            {
                                resDict.Add(contract.PartnerCompanyCode, (int)contract.Conditions[1].MaterialAmount);
                            }
                        }
                    }

                    var res = new List<TacoResult>();
                    foreach (var kvp in resDict)
                    {
                        res.Add(new TacoResult
                        {
                            BuyerCompanyCode = kvp.Key,
                            TacosConsumed = kvp.Value
                        });
                    }

                    res = res.OrderByDescending(r => r.TacosConsumed).ToList();
                    return JsonConvert.SerializeObject(res);
                }
            }
            else
            {
                return HttpStatusCode.Unauthorized;
            }
        }
    }
}
#endif // WITH_MODULES