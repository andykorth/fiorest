﻿#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules.FIOWeb.GroupHub
{
    public class FIOWebGroupHubModule : NancyModule
    {
        public FIOWebGroupHubModule() : base("/fioweb/grouphub")
        {
            this.EnforceReadAuth();

            Get("/{group_name}", parameters =>
            {
                return GetGroupHubData(parameters.group_name);
            });

            Post("/", _ =>
            {
                return PostGetGroupHubData();
            });
        }

        private Response GetGroupHubData(string GroupName)
        {
            // @TODO: This should replace PostGetGroupHubData
            return null;
        }

        private Response PostGetGroupHubData()
        {
            using (var req = new FIORequest<List<string>>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                string ProxyRequesterUserName = null;

                var ProxyRequesterDiscordId = Request.Headers.Keys.Contains("ProxyRequesterDiscordId") ? Request.Headers["ProxyRequesterDiscordId"].FirstOrDefault() : null;
                if (ProxyRequesterDiscordId != null)
                {
                    ProxyRequesterUserName = req.DB.AuthenticationModels
                        .AsNoTracking()
                        .Where(am => am.DiscordId.ToUpper() == ProxyRequesterDiscordId.ToUpper())
                        .Select(am => am.UserName)
                        .FirstOrDefault();
                }

                string RequesterUserName = Request.GetUserName();
                if (RequesterUserName == "FIDO" && ProxyRequesterUserName == null)
                {
                    // Don't allow general fido requests
                    return HttpStatusCode.Unauthorized;
                }

                var materials = req.DB.Materials
                    .AsNoTracking()
                    .ToList();
                var buildings = req.DB.Buildings
                    .AsNoTracking()
                    .ToList();
                if (materials.Count == 0 || buildings.Count == 0)
                {
                    Response noMatOrBuiDataResponse = (Response)"Unable to find MAT/BUI data";
                    noMatOrBuiDataResponse.StatusCode = HttpStatusCode.BadRequest;
                    noMatOrBuiDataResponse.ContentType = "text/plain";
                    return noMatOrBuiDataResponse;
                }

                List<string> UserNames = req.JsonPayload;
                if (UserNames == null || UserNames.Count == 0)
                {

                    Response noUserResponse = UserNames == null ? (Response)"Payload is null" : (Response)"Payload has no UserName entries";
                    noUserResponse.StatusCode = HttpStatusCode.BadRequest;
                    noUserResponse.ContentType = "text/plain";
                    return noUserResponse;
                }

                var sfVolume = req.DB.Materials
                    .AsNoTracking()
                    .Where(mm => mm.Ticker == "SF")
                    .Select(mm => mm.Volume)
                    .FirstOrDefault();
                var ffVolume = req.DB.Materials
                    .AsNoTracking()
                    .Where(mm => mm.Ticker == "FF")
                    .Select(mm => mm.Volume)
                    .FirstOrDefault();

                if (sfVolume == default(double) || ffVolume == default(double))
                {
                    Response noVolumeDataResponse = (Response)"Unable to find SF/FF Volume Data";
                    noVolumeDataResponse.StatusCode = HttpStatusCode.BadRequest;
                    noVolumeDataResponse.ContentType = "text/plain";
                    return noVolumeDataResponse;
                }

                var cxLocations = req.DB.ComexExchanges
                    .AsNoTracking()
                    .ToList();
                if (cxLocations.Count == 0)
                {
                    Response noCxDataResponse = (Response)"Unable to find CX location data";
                    noCxDataResponse.StatusCode = HttpStatusCode.BadRequest;
                    noCxDataResponse.ContentType = "text/plain";
                    return noCxDataResponse;
                }

                GroupHubModel result = new GroupHubModel();
                foreach (var UserName in UserNames)
                {
                    StringBuilder errorSb = new StringBuilder();

                    Dictionary<string, PermissionAllowance> RequesterPermissions = Caches.PermissionAllowancesCache.GetOrCache(RequesterUserName, req.DB);
                    Dictionary<string, PermissionAllowance> ProxyRequesterPermissions = ProxyRequesterUserName != null ? Caches.PermissionAllowancesCache.GetOrCache(ProxyRequesterUserName, req.DB) : null;

                    bool RequesterHasUserNameKey = RequesterPermissions.ContainsKey(UserName);

                    bool HasStoragePermissions = RequesterHasUserNameKey && RequesterPermissions[UserName].StorageData;
                    bool HasBuildingPermissions = RequesterHasUserNameKey && RequesterPermissions[UserName].BuildingData;
                    bool HasFlightPermissions = RequesterHasUserNameKey && RequesterPermissions[UserName].FlightData;
                    bool HasContractPermissions = RequesterHasUserNameKey && RequesterPermissions[UserName].ContractData;
                    bool HasProductionPermissions = RequesterHasUserNameKey && RequesterPermissions[UserName].ProductionData;

                    if (ProxyRequesterUserName != null)
                    {
                        bool ProxyRequesterHasUserNameKey = ProxyRequesterPermissions.ContainsKey(UserName);

                        HasStoragePermissions &= ProxyRequesterHasUserNameKey && ProxyRequesterPermissions[UserName].StorageData;
                        HasBuildingPermissions &= ProxyRequesterHasUserNameKey && ProxyRequesterPermissions[UserName].BuildingData;
                        HasFlightPermissions &= ProxyRequesterHasUserNameKey && ProxyRequesterPermissions[UserName].FlightData;
                        HasContractPermissions &= ProxyRequesterHasUserNameKey && ProxyRequesterPermissions[UserName].ContractData;
                        HasProductionPermissions &= ProxyRequesterHasUserNameKey && ProxyRequesterPermissions[UserName].ProductionData;
                    }

                    if (!HasStoragePermissions || !HasBuildingPermissions || !HasFlightPermissions || !HasContractPermissions || !HasProductionPermissions)
                    {
                        errorSb.Append($"Do not have the following permissions for '{UserName}': ");
                        if (!HasStoragePermissions)
                        {
                            errorSb.Append("Storage,");
                        }
                        if (!HasBuildingPermissions)
                        {
                            errorSb.Append("Building,");
                        }
                        if (!HasFlightPermissions)
                        {
                            errorSb.Append("Flight,");
                        }
                        if (!HasContractPermissions)
                        {
                            errorSb.Append("Contract,");
                        }
                        if (!HasProductionPermissions)
                        {
                            errorSb.Append("Production,");
                        }

                        // Remove last comma
                        errorSb.Remove(errorSb.Length - 1, 1);
                        result.Failures.Add(errorSb.ToString());
                    }

                    List<Warehouse> warehouses = null;
                    if (HasStoragePermissions)
                    {
                        warehouses = req.DB.Warehouses
                            .AsNoTracking()
                            .Where(war => war.UserNameSubmitted.ToUpper() == UserName.ToUpper())
                            .ToList();
                    }

                    List<Site> sites = null;
                    if (HasBuildingPermissions)
                    {
                        sites = req.DB.Sites
                            .AsNoTracking()
                            .Include(s => s.Buildings)
                                .ThenInclude(b => b.ReclaimableMaterials)
                            .Include(s => s.Buildings)
                                .ThenInclude(b => b.RepairMaterials)
                            .Where(sites => sites.UserNameSubmitted.ToUpper() == UserName.ToUpper())
                            .AsSplitQuery()
                            .ToList();
                    }

                    List<Storage> storages = null;
                    if (HasStoragePermissions)
                    {
                        storages = req.DB.Storages
                            .AsNoTracking()
                            .Where(store => store.UserNameSubmitted.ToUpper() == UserName.ToUpper())
                            .Include(s => s.StorageItems)
                            .AsSplitQuery()
                            .ToList();
                    }

                    List<Ship> ships = null;
                    List<Flight> flights = null;
                    if (HasFlightPermissions)
                    {
                        ships = req.DB.Ships
                            .AsNoTracking()
                            .Include(s => s.RepairMaterials)
                            .Include(s => s.AddressLines)
                            .Where(ships => ships.UserNameSubmitted.ToUpper() == UserName.ToUpper())
                            .AsSplitQuery()
                            .ToList();

                        flights = req.DB.Flights
                            .AsNoTracking()
                            .Where(fm => fm.UserNameSubmitted.ToUpper() == UserName.ToUpper())
                            .Include(f => f.Segments)
                                .ThenInclude(f => f.OriginLines)
                             .Include(f => f.Segments)
                                .ThenInclude(f => f.DestinationLines)
                            .AsSplitQuery()
                            .ToList();
                    }

                    UserData userData = null;
                    if (HasContractPermissions)
                    {
                        userData = req.DB.UserData
                            .AsNoTracking()
                            .Where(cdm => cdm.UserName.ToUpper() == UserName.ToUpper())
                            .Include(cdm => cdm.Balances)
                            .Include(cdm => cdm.Planets)
                            .Include(cdm => cdm.Offices)
                            .AsSplitQuery()
                            .FirstOrDefault();
                    }

                    List<ProductionLine> productionLines = null;
                    if (HasProductionPermissions)
                    {
                        productionLines = req.DB.ProductionLines
                            .AsNoTracking()
                            .Where(plm => plm.UserNameSubmitted.ToUpper() == UserName.ToUpper())
                            .Include(pl => pl.Orders)
                                .ThenInclude(plo => plo.Inputs)
                            .Include(pl => pl.Orders)
                                .ThenInclude(plo => plo.Outputs)
                            .AsSplitQuery()
                            .ToList();
                    }
                    else
                    {
                        productionLines = new List<ProductionLine>();
                    }

                    // CXWarehouses
                    if (warehouses != null && storages != null)
                    {
                        foreach (var warehouse in warehouses)
                        {
                            if (!cxLocations.Any(cxl => cxl.LocationNaturalId == warehouse.LocationNaturalId))
                            {
                                // This isn't a CX
                                continue;
                            }

                            var existingWarehouse = result.CXWarehouses.Where(cxw => cxw.WarehouseLocationNaturalId == warehouse.LocationNaturalId).FirstOrDefault();
                            bool bAddWarehouse = (existingWarehouse == null);
                            if (bAddWarehouse)
                            {
                                existingWarehouse = new GroupCXWarehouse();
                                existingWarehouse.WarehouseLocationName = warehouse.LocationName;
                                existingWarehouse.WarehouseLocationNaturalId = warehouse.LocationNaturalId;
                            }

                            var warehouseStore = storages.Where(store => warehouse.WarehouseId.StartsWith(store.AddressableId) && store.UserNameSubmitted == warehouse.UserNameSubmitted).FirstOrDefault();
                            if (warehouseStore != null)
                            {
                                var playerStorage = new PlayerStorage();

                                playerStorage.PlayerName = UserName;
                                playerStorage.StorageType = warehouseStore.Type;

                                foreach (var storageItem in warehouseStore.StorageItems)
                                {
                                    var groupHubItem = new GroupHubInventoryItem();

                                    groupHubItem.MaterialName = storageItem.MaterialName;
                                    groupHubItem.MaterialTicker = storageItem.MaterialTicker;
                                    groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == storageItem.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                    groupHubItem.Units = storageItem.MaterialAmount;

                                    playerStorage.Items.Add(groupHubItem);
                                }

                                playerStorage.LastUpdated = warehouseStore.Timestamp;
                                existingWarehouse.PlayerCXWarehouses.Add(playerStorage);
                            }

                            if (bAddWarehouse)
                            {
                                result.CXWarehouses.Add(existingWarehouse);
                            }
                        }
                    }

                    // PlayerShipsInFlight & PlayerStationaryShips
                    if (ships != null && flights != null && storages != null)
                    {
                        foreach(var ship in ships)
                        {
                            var flight = flights
                                .Where(f => f.ShipId == ship.ShipId)
                                .FirstOrDefault();
                            bool bIsInFlight = flight != null;

                            var playerShip = new PlayerShip();
                            playerShip.PlayerName = UserName;
                            playerShip.ShipName = ship.Name;
                            playerShip.ShipRegistration = ship.Registration;
                            playerShip.AddressLines = ship.AddressLines
                                .Select(al => new ShipAddressLine
                                {
                                    LineId = al.LineId,
                                    LineType = al.LineType,
                                    NaturalId = al.NaturalId,
                                    Name = al.Name
                                })
                                .ToList();

                            if (bIsInFlight)
                            {
                                playerShip.Location = $"En route to {flight.Destination}";
                                playerShip.Destination = flight.Destination;
                                playerShip.LocationETA = Utils.FromUnixTime(flight.ArrivalTimeEpochMs);
                                playerShip.Flight = new ShipFlight();
                                playerShip.Flight.FlightId = flight.FlightId;
                                playerShip.Flight.ShipId = flight.ShipId;
                                playerShip.Flight.Origin = flight.Origin;
                                playerShip.Flight.Destination = flight.Destination;
                                playerShip.Flight.DepartureTimeEpochMs = flight.DepartureTimeEpochMs;
                                playerShip.Flight.ArrivalTimeEpochMs = flight.ArrivalTimeEpochMs;
                                playerShip.Flight.CurrentSegmentIndex = flight.CurrentSegmentIndex;
                                playerShip.Flight.StlDistance = flight.StlDistance;
                                playerShip.Flight.FtlDistance = flight.FtlDistance;
                                playerShip.Flight.IsAborted = flight.IsAborted;
                                playerShip.Flight.Timestamp = flight.Timestamp;
                                playerShip.Flight.Segments = flight.Segments
                                    .Select(s => new ShipFlightSegment
                                    {
                                        Type = s.Type,
                                        DepartureTimeEpochMs = s.DepartureTimeEpochMs,
                                        ArrivalTimeEpochMs = s.ArrivalTimeEpochMs,
                                        StlDistance = s.StlDistance,
                                        StlFuelConsumption = s.StlFuelConsumption,
                                        FtlDistance = s.FtlDistance,
                                        FtlFuelConsumption = s.FtlFuelConsumption,
                                        Origin = s.Origin,
                                        Destination = s.Destination,
                                        OriginLines = s.OriginLines
                                            .Select(o => new ShipFlightLine
                                            {
                                                Type = o.Type,
                                                LineId = o.LineId,
                                                LineNaturalId = o.LineNaturalId,
                                                LineName = o.LineName
                                            })
                                            .ToList(),
                                        DestinationLines = s.DestinationLines
                                            .Select(d => new ShipFlightLine
                                            {
                                                Type = d.Type,
                                                LineId = d.LineId,
                                                LineNaturalId = d.LineNaturalId,
                                                LineName = d.LineName
                                            })
                                            .ToList()
                                    })
                                    .ToList();
                            }
                            else
                            {
                                playerShip.Location = ship.Location;
                            }

                            var stlFuelStore = storages.Where(s => s.StorageId == ship.StlFuelStoreId).FirstOrDefault();
                            if (stlFuelStore != null)
                            {
                                var sfAmount = stlFuelStore.StorageItems.Where(si => si.MaterialTicker == "SF").Select(si => si.MaterialAmount).FirstOrDefault();
                                playerShip.Fuel.CurrentSF = sfAmount;
                                playerShip.Fuel.MaxSF = (int)(stlFuelStore.VolumeCapacity / sfVolume);
                            }

                            var ftlFuelStore = storages.Where(s => s.StorageId == ship.FtlFuelStoreId).FirstOrDefault();
                            if (ftlFuelStore != null)
                            {
                                var ffAmount = ftlFuelStore.StorageItems.Where(si => si.MaterialTicker == "FF").Select(si => si.MaterialAmount).FirstOrDefault();
                                playerShip.Fuel.CurrentFF = ffAmount;
                                playerShip.Fuel.MaxFF = (int)(ftlFuelStore.VolumeCapacity / ffVolume);
                            }

                            playerShip.Condition = ship.Condition;
                            playerShip.RepairMaterials = ship.RepairMaterials
                                .Select(srm => new ShipRepairMaterial
                                {
                                    MaterialTicker = srm.MaterialTicker,
                                    Amount = srm.Amount
                                })
                                .ToList();

                            var shipStore = storages.Where(s => ship.ShipId == s.AddressableId && s.Type == "SHIP_STORE").FirstOrDefault();
                            if (shipStore != null)
                            {
                                playerShip.Cargo = new PlayerStorage();
                                playerShip.Cargo.PlayerName = UserName;
                                playerShip.Cargo.StorageType = shipStore.Type;

                                foreach (var shipStoreItem in shipStore.StorageItems)
                                {
                                    var groupHubItem = new GroupHubInventoryItem();

                                    groupHubItem.MaterialName = shipStoreItem.MaterialName;
                                    groupHubItem.MaterialTicker = shipStoreItem.MaterialTicker;
                                    groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == shipStoreItem.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                    groupHubItem.Units = shipStoreItem.MaterialAmount;

                                    playerShip.Cargo.Items.Add(groupHubItem);
                                }

                                playerShip.Cargo.LastUpdated = shipStore.Timestamp;
                            }

                            playerShip.LastUpdated = ship.Timestamp;

                            if (bIsInFlight)
                            {
                                result.PlayerShipsInFlight.Add(playerShip);
                            }
                            else
                            {
                                result.PlayerStationaryShips.Add(playerShip);
                            }
                        }
                    }

                    // PlayerModels
                    {
                        var playerModel = new PlayerModel();
                        playerModel.UserName = UserName;

                        if (userData != null && userData.Balances != null)
                        {
                            foreach (var balance in userData.Balances)
                            {
                                if (balance != null && balance.Amount > 0.0)
                                {
                                    var playerCurrency = new PlayerCurrency();

                                    playerCurrency.CurrencyName = balance.Currency;
                                    playerCurrency.Amount = balance.Amount;
                                    playerCurrency.LastUpdated = userData.Timestamp;

                                    playerModel.Currencies.Add(playerCurrency);
                                }
                            }
                        }

                        HashSet<string> naturalIds = new HashSet<string>();
                        if (sites != null)
                        {
                            naturalIds.UnionWith(sites.Select(s => s.PlanetIdentifier).ToHashSet());
                        }

                        if (warehouses != null)
                        {
                            naturalIds.UnionWith(warehouses.Select(w => w.LocationNaturalId).ToHashSet());
                        }

                        foreach (var naturalId in naturalIds)
                        {
                            var warehouse = warehouses.FirstOrDefault(w => w.LocationNaturalId.ToUpper() == naturalId.ToUpper());
                            var site = sites.FirstOrDefault(s => s.PlanetIdentifier.ToUpper() == naturalId.ToUpper());

                            PlayerLocation playerLocation = null;
                            if (warehouse != null || site != null)
                            {
                                playerLocation = new PlayerLocation();
                            }

                            if (warehouse != null)
                            {
                                playerLocation.LocationIdentifier = warehouse.LocationNaturalId;
                                playerLocation.LocationName = warehouse.LocationName;

                                var warehouseStorage = storages.Where(storage => storage.StorageId == warehouse.StoreId).FirstOrDefault();
                                if (warehouseStorage != null)
                                {
                                    playerLocation.WarehouseStorage = new PlayerStorage();

                                    playerLocation.WarehouseStorage.PlayerName = UserName;
                                    playerLocation.WarehouseStorage.StorageType = warehouseStorage.Type;

                                    foreach (var warItem in warehouseStorage.StorageItems)
                                    {
                                        var groupHubItem = new GroupHubInventoryItem();

                                        groupHubItem.MaterialName = warItem.MaterialName;
                                        groupHubItem.MaterialTicker = warItem.MaterialTicker;
                                        groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == warItem.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                        groupHubItem.Units = warItem.MaterialAmount;

                                        playerLocation.WarehouseStorage.Items.Add(groupHubItem);
                                    }
                                    playerLocation.WarehouseStorage.LastUpdated = warehouseStorage.Timestamp;
                                }
                            }
                            
                            if (site != null)
                            {
                                playerLocation.LocationIdentifier = site.PlanetIdentifier;
                                playerLocation.LocationName = site.PlanetName;

                                playerLocation.Buildings = sites
                                    .Where(s => s.PlanetIdentifier == playerLocation.LocationIdentifier)
                                    .SelectMany(s => s.Buildings)
                                    .Select(b =>
                                        new PlayerBuilding
                                        {
                                            BuildingName = b.BuildingName,
                                            BuildingTicker = b.BuildingTicker,
                                            Condition = b.Condition,
                                            RepairMaterials = b.RepairMaterials.Select(rm =>
                                                new GroupHubInventoryItem
                                                {
                                                    MaterialTicker = rm.MaterialTicker,
                                                    MaterialName = rm.MaterialName,
                                                    Units = rm.MaterialAmount
                                                }).ToList(),
                                            ReclaimableMaterials = b.ReclaimableMaterials.Select(rm =>
                                                new GroupHubInventoryItem
                                                {
                                                    MaterialTicker = rm.MaterialTicker,
                                                    MaterialName = rm.MaterialName,
                                                    Units = rm.MaterialAmount
                                                }).ToList()
                                        })
                                    .ToList();

                                var prodLines = productionLines
                                    .Where(pl => pl.PlanetNaturalId == site.PlanetIdentifier);
                                foreach (var prodLine in prodLines)
                                {
                                    var playerProductionLine = new PlayerProductionLine();

                                    playerProductionLine.BuildingName = prodLine.Type;
                                    playerProductionLine.BuildingTicker = buildings.Where(b => b.Name == prodLine.Type).Select(b => b.Ticker).FirstOrDefault();
                                    playerProductionLine.Capacity = prodLine.Capacity;
                                    playerProductionLine.Efficiency = prodLine.Efficiency;
                                    playerProductionLine.Condition = prodLine.Condition;

                                    foreach (var prodOrder in prodLine.Orders)
                                    {
                                        var playerProductionOrder = new PlayerProductionOrder();

                                        playerProductionOrder.Started = (prodOrder.CompletionEpochMs != null);
                                        playerProductionOrder.Halted = prodOrder.IsHalted;
                                        playerProductionOrder.Recurring = prodOrder.Recurring;

                                        playerProductionOrder.OrderDuration = prodOrder.DurationMs != null ? TimeSpan.FromMilliseconds((double)prodOrder.DurationMs) : TimeSpan.FromMilliseconds(0.0);

                                        if (prodOrder.CompletionEpochMs != null)
                                        {
                                            playerProductionOrder.TimeCompletion = Utils.FromUnixTime((long)prodOrder.CompletionEpochMs);
                                        }

                                        foreach (var prodInput in prodOrder.Inputs)
                                        {
                                            var groupHubItem = new GroupHubInventoryItem();

                                            groupHubItem.MaterialName = prodInput.MaterialName;
                                            groupHubItem.MaterialTicker = prodInput.MaterialTicker;
                                            groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == prodInput.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                            groupHubItem.Units = prodInput.MaterialAmount;

                                            playerProductionOrder.Inputs.Add(groupHubItem);
                                        }

                                        foreach (var prodOutput in prodOrder.Outputs)
                                        {
                                            var groupHubItem = new GroupHubInventoryItem();

                                            groupHubItem.MaterialName = prodOutput.MaterialName;
                                            groupHubItem.MaterialTicker = prodOutput.MaterialTicker;
                                            groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == prodOutput.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                            groupHubItem.Units = prodOutput.MaterialAmount;

                                            playerProductionOrder.Outputs.Add(groupHubItem);
                                        }

                                        playerProductionOrder.LastUpdated = prodLine.Timestamp;

                                        playerProductionLine.ProductionOrders.Add(playerProductionOrder);
                                    }


                                    playerLocation.ProductionLines.Add(playerProductionLine);
                                }

                                // BaseStorage
                                if (storages != null)
                                {
                                    var baseStorage = storages.Where(storage => storage.AddressableId == site.SiteId).FirstOrDefault();
                                    if (baseStorage != null)
                                    {
                                        var basePlayerStorage = new PlayerStorage();

                                        basePlayerStorage.PlayerName = UserName;
                                        basePlayerStorage.StorageType = baseStorage.Type;
                                        foreach (var baseItem in baseStorage.StorageItems)
                                        {
                                            var groupHubItem = new GroupHubInventoryItem();

                                            groupHubItem.MaterialName = baseItem.MaterialName;
                                            groupHubItem.MaterialTicker = baseItem.MaterialTicker;
                                            groupHubItem.MaterialCategoryName = materials.Where(mm => mm.Ticker == baseItem.MaterialTicker).Select(mm => mm.CategoryName).FirstOrDefault();
                                            groupHubItem.Units = baseItem.MaterialAmount;

                                            basePlayerStorage.Items.Add(groupHubItem);
                                        }
                                        basePlayerStorage.LastUpdated = baseStorage.Timestamp;

                                        playerLocation.BaseStorage = basePlayerStorage;
                                    }
                                }
                            }

                            // StationaryPlayerShips
                            foreach (var stationaryShip in result.PlayerStationaryShips)
                            {
                                var locationNaturalId = stationaryShip.AddressLines.Select(al => al.NaturalId).LastOrDefault();
                                if (locationNaturalId != null && locationNaturalId == playerLocation.LocationIdentifier)
                                {
                                    playerLocation.StationaryPlayerShips.Add(stationaryShip);
                                }
                            }

                            if (playerLocation != null)
                            {
                                playerModel.Locations.Add(playerLocation);
                            }
                        }

                        result.PlayerModels.Add(playerModel);
                    }
                }

                return JsonConvert.SerializeObject(result);
            }
        }
    }
}
#endif // WITH_MODULES