﻿using System;
using System.Collections.Generic;

namespace FIORest.Modules.FIOWeb.Burn
{
    public class BurnPayload
    {
        public string RequesterUserName { get; set; }
        public string UserName { get; set; }
        public string Error { get; set; }

        public string PlanetId { get; set; }
        public string PlanetName { get; set; }
        public string PlanetNaturalId { get; set; }

        public DateTime? PlanetConsumptionTime { get; set; }

        public DateTime LastUpdate { get; set; } = DateTime.MinValue;
        public string LastUpdateCause { get; set; }

        public List<BurnInventoryItem> Inventory { get; set; } = new List<BurnInventoryItem>();

        public List<BurnConsumptionProduction> WorkforceConsumption { get; set; } = new List<BurnConsumptionProduction>();
        public List<BurnConsumptionProduction> OrderConsumption { get; set; } = new List<BurnConsumptionProduction>();
        public List<BurnConsumptionProduction> OrderProduction { get; set; } = new List<BurnConsumptionProduction>();
    }

    public class BurnInventoryItem
    {
        public string MaterialId { get; set; }
        public string MaterialTicker { get; set; }
        public int MaterialAmount { get; set; }
    }

    public class BurnConsumptionProduction
    {
        public string MaterialId { get; set; }
        public string MaterialTicker { get; set; }
        public double DailyAmount { get; set; } = 0.0;
    }
}
