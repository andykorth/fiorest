#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;
using Nancy;

using Csv = CsvHelper.Configuration.Attributes;

namespace FIORest.Modules
{
	public class CSVModule : NancyModule
	{
		public CSVModule() : base("/csv")
		{
			Get("/buildings", _ =>
			{
				this.Cacheable();
				return GetBuildings();
			});

			Get("/buildingcosts", _ =>
			{
				this.Cacheable();
				return GetBuildingCosts();
			});

			Get("/buildingworkforces", _ =>
			{
				this.Cacheable();
				return GetBuildingWorkforces();
			});

			Get("/buildingrecipes", _ =>
			{
				this.Cacheable();
				return GetBuildingRecipes();
			});

			Get("/materials", _ =>
			{
				this.Cacheable();
				return GetMaterials();
			});

			Get("/prices", _ =>
			{
				this.Cacheable(10 * 60); // 10 minutes
				return GetPrices();
			});

			Get("/prices/experimental", _ =>
			{
				this.Cacheable(10 * 60);
				return GetPricesExperimental();
			});

			Get("/prices/condensed", _ =>
			{
				this.Cacheable(10 * 60); // 10 minutes
				return GetPricesCondensed();
			});

			Get("/orders", _ =>
			{
				this.Cacheable(10 * 60);
				return GetOrders();
			});

			Get("/bids", _ =>
			{
				this.Cacheable(10 * 60);
				return GetBids();
			});

			Get("/recipeinputs", _ =>
			{
				this.Cacheable();
				return GetRecipeInputs();
			});

			Get("/recipeoutputs", _ =>
			{
				this.Cacheable();
				return GetRecipeOutputs();
			});

			Get("/planets", _ =>
			{
				this.Cacheable();
				return GetPlanets();
			});

			Get("/planetresources", _ =>
			{
				this.Cacheable();
				return GetPlanetResources();
			});

			Get("/planetproductionfees", _ =>
			{
				this.Cacheable();
				return GetPlanetProductionFees();
			});

			Get("/planetdetail", _ =>
			{
				this.Cacheable();
				return GetPlanetDetail();
			});

			Get("/systems", _ =>
			{
				this.Cacheable();
				return GetSystems();
			});

			Get("/systemlinks", _ =>
			{
				this.Cacheable();
				return GetSystemLinks();
			});

			Get("/systemplanets", _ =>
			{
				this.Cacheable();
				return GetSystemPlanets();
			});

			Get("/workforceneeds", _ =>
			{
				this.Cacheable();
				return GetWorkforceNeeds();
			});

			Get("/infrastructure/report/{planet}", parameters =>
			{
				this.Cacheable();
				return GetInfrastructureReport(planet: parameters.planet);
			});

			Get("/infrastructure/report/{planet}/latest", parameters =>
			{
				this.Cacheable();
				return GetInfrastructureReport(planet: parameters.planet, bOnlyLatest: true);
			});

			Get("/infrastructure/allreports", _ =>
			{
				this.Cacheable();
				return GetInfrastructureReport();
			});

			Get("/infrastructure/allreports/latest", _ =>
			{
				this.Cacheable();
				return GetInfrastructureReport(bOnlyLatest: true);
			});

			Get("/infrastructure/infos/{planet}", parameters =>
			{
				this.Cacheable();
				return GetInfrastructureInfo(parameters.planet);
			});

			Get("/infrastructure/allinfos", _ =>
			{
				this.Cacheable();
				return GetInfrastructureInfo();
			});

			Get("/localmarket/buy/{planet}", parameters =>
			{
				this.Cacheable();
				return GetLocalMarketBuys(parameters.planet);
			});

			Get("/localmarket/sell/{planet}", parameters =>
			{
				this.Cacheable();
				return GetLocalMarketSells(parameters.planet);
			});

			Get("/localmarket/ship/{planet}", parameters =>
			{
				this.Cacheable();
				return GetLocalMarketShipments(parameters.planet);
			});

			Get("/cxpc/{ticker}", parameters =>
			{
				this.Cacheable();
				return GetCXPC(parameters.ticker);
			});

			Get("/inventory", _ =>
			{
				return GetInventory(this.Request.Query);
			});

			Get("/burnrate", _ =>
			{
				return GetBurnRate(this.Request.Query);
			});

			Get("/sites", _ =>
			{
				return GetSites(this.Request.Query);
			});

			Get("/reclaimables", _ =>
			{
				return GetReclaimables(this.Request.Query);
			});

			Get("/repairables", _ =>
			{
				return GetRepairables(this.Request.Query);
			});

			Get("/workforce", _ =>
			{
				return GetWorkforce(this.Request.Query);
			});

			Get("/cxos", _ =>
			{
				return GetCXOS(this.Request.Query);
			});

			Get("/burn", _ =>
			{
				return GetBurn(this.Request.Query);
			});

			Get("/balances", _ =>
			{
				return GetBalances(this.Request.Query);
			});
		}

		public class CsvBuilding
		{
			[Csv.Index(0)]
			public string Ticker { get; set; }

			[Csv.Index(1)]
			public string Name { get; set; }

			[Csv.Index(2)]
			public int Area { get; set; }

			[Csv.Index(3)]
			public string Expertise { get; set; }
		}

		public Response GetBuildings()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var buildings = DB.Buildings.AsNoTracking();
				var res = from b in buildings
						  orderby b.Ticker
						  select new CsvBuilding
						  {
							  Ticker = b.Ticker,
							  Name = b.Name,
							  Area = b.AreaCost,
							  Expertise = b.Expertise
						  };

				return res.ToList().GetCSVResponse();
			}
		}

		public class CsvBuildingCost
		{
			[Csv.Index(0)]
			public string Key { get; set; }

			[Csv.Index(1)]
			public string Building { get; set; }

			[Csv.Index(2)]
			public string Material { get; set; }

			[Csv.Index(3)]
			public int Amount { get; set; }
		}

		private Response GetBuildingCosts()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var buildingCosts = new List<CsvBuildingCost>();

				var buildings = DB.Buildings
					.AsNoTracking()
					.Include(b => b.BuildingCosts)
					.OrderBy(b => b.Ticker)
					.ToList();
				foreach (var building in buildings)
				{
					foreach (var buildingCost in building.BuildingCosts.OrderBy(bc => bc.CommodityTicker))
					{
						var cbc = new CsvBuildingCost();

						cbc.Key = $"{building.Ticker}-{buildingCost.CommodityTicker}";
						cbc.Building = building.Ticker;
						cbc.Material = buildingCost.CommodityTicker;
						cbc.Amount = buildingCost.Amount;

						buildingCosts.Add(cbc);
					}
				}

				return buildingCosts.GetCSVResponse();
			}
		}

		public class CsvBuildingWorkforce
		{
			[Csv.Index(0)]
			public string Key { get; set; } // BUI-WORKER

			[Csv.Index(1)]
			public string Building { get; set; }

			[Csv.Index(2)]
			public string Level { get; set; } // WORKER

			[Csv.Index(3)]
			public int Capacity { get; set; }
		}

		private void PopulateWorkforce(ref List<CsvBuildingWorkforce> buildingWorkforces, int workforceCount, string buildingTicker, string workforceType)
		{
			if (workforceCount > 0)
			{
				var cbw = new CsvBuildingWorkforce();

				cbw.Key = $"{buildingTicker}-{workforceType}";
				cbw.Building = buildingTicker;
				cbw.Level = workforceType;
				cbw.Capacity = workforceCount;

				buildingWorkforces.Add(cbw);
			}
		}

		private Response GetBuildingWorkforces()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var buildingWorkforces = new List<CsvBuildingWorkforce>();

				var buildings = DB.Buildings
					.AsNoTracking()
					.OrderBy(b => b.Ticker)
					.ToList();
				foreach (var building in buildings)
				{
					// Rain keeps them in alphabetical order, so do that
					PopulateWorkforce(ref buildingWorkforces, building.Engineers, building.Ticker, "ENGINEER");
					PopulateWorkforce(ref buildingWorkforces, building.Pioneers, building.Ticker, "PIONEER");
					PopulateWorkforce(ref buildingWorkforces, building.Scientists, building.Ticker, "SCIENTIST");
					PopulateWorkforce(ref buildingWorkforces, building.Settlers, building.Ticker, "SETTLER");
					PopulateWorkforce(ref buildingWorkforces, building.Technicians, building.Ticker, "TECHNICIAN");
				}

				return buildingWorkforces.GetCSVResponse();
			}
		}

		public class CsvBuildingRecipe
		{
			[Csv.Index(0)]
			public string Key { get; set; } // BUI-OUTPUT

			[Csv.Index(1)]
			public string Building { get; set; }

			[Csv.Index(2)]
			public long Duration { get; set; }
		}

		private Response GetBuildingRecipes()
		{
			var buildingRecipes = new List<CsvBuildingRecipe>();
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var buildings = DB.Buildings
					.AsNoTracking()
					.OrderBy(b => b.Ticker)
					.Include(b => b.Recipes)
						.ThenInclude(r => r.Outputs.OrderBy(o => o.CommodityTicker))
					.Include(b => b.Recipes)
						.ThenInclude(r => r.Inputs.OrderBy(i => i.CommodityTicker))
					.AsSplitQuery()
					.ToList();

				foreach (var building in buildings)
				{
					foreach (var recipe in building.Recipes)
					{
						var cbr = new CsvBuildingRecipe();
						cbr.Key = recipe.GetRecipeDescription();
						cbr.Building = building.Ticker;
						cbr.Duration = recipe.DurationMs / 1000;

						buildingRecipes.Add(cbr);
					}
				}
			}

			return buildingRecipes.GetCSVResponse();
		}

		public class CsvMaterial
		{
			[Csv.Index(0)]
			public string Ticker { get; set; }

			[Csv.Index(1)]
			public string Name { get; set; }

			[Csv.Index(2)]
			public string Category { get; set; }

			[Csv.Index(3)]
			public double Weight { get; set; } // 3 decimal points

			[Csv.Index(4)]
			public double Volume { get; set; } // 3 decimal points
		}

		private Response GetMaterials()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var res = from m in DB.Materials.AsNoTracking()
						  orderby m.Ticker
						  select new CsvMaterial
						  {
							  Ticker = m.Ticker,
							  Name = m.Name,
							  Category = m.CategoryName,
							  Weight = m.Weight.RoundToDecimalPlaces(3),
							  Volume = m.Volume.RoundToDecimalPlaces(3)
						  };

				return res.ToList().GetCSVResponse();
			}
		}

		private class CsvPrices
		{
			[Csv.Index(0)]
			public string Ticker { get; set; }
			[Csv.Index(1)]
			public double? MMBuy { get; set; }
			[Csv.Index(2)]
			public double? MMSell { get; set; }

			[Csv.Index(3)]
			[Csv.Name("AI1-Average")]
			public double? AI1_Average { get; set; }
			[Csv.Index(4)]
			[Csv.Name("AI1-AskAmt")]
			public int? AI1_AskAmt { get; set; }
			[Csv.Index(5)]
			[Csv.Name("AI1-AskPrice")]
			public double? AI1_AskPrice { get; set; }
			[Csv.Index(6)]
			[Csv.Name("AI1-AskAvail")]
			public int? AI1_AskAvail { get; set; }
			[Csv.Index(7)]
			[Csv.Name("AI1-BidAmt")]
			public int? AI1_BidAmt { get; set; }
			[Csv.Index(8)]
			[Csv.Name("AI1-BidPrice")]
			public double? AI1_BidPrice { get; set; }
			[Csv.Index(9)]
			[Csv.Name("AI1-BidAvail")]
			public int? AI1_BidAvail { get; set; }

			[Csv.Index(10)]
			[Csv.Name("CI1-Average")]
			public double? CI1_Average { get; set; }
			[Csv.Index(11)]
			[Csv.Name("CI1-AskAmt")]
			public int? CI1_AskAmt { get; set; }
			[Csv.Index(12)]
			[Csv.Name("CI1-AskPrice")]
			public double? CI1_AskPrice { get; set; }
			[Csv.Index(13)]
			[Csv.Name("CI1-AskAvail")]
			public int? CI1_AskAvail { get; set; }
			[Csv.Index(14)]
			[Csv.Name("CI1-BidAmt")]
			public int? CI1_BidAmt { get; set; }
			[Csv.Index(15)]
			[Csv.Name("CI1-BidPrice")]
			public double? CI1_BidPrice { get; set; }
			[Csv.Index(16)]
			[Csv.Name("CI1-BidAvail")]
			public int? CI1_BidAvail { get; set; }

			[Csv.Index(17)]
			[Csv.Name("CI2-Average")]
			public double? CI2_Average { get; set; }
			[Csv.Index(18)]
			[Csv.Name("CI2-AskAmt")]
			public int? CI2_AskAmt { get; set; }
			[Csv.Index(19)]
			[Csv.Name("CI2-AskPrice")]
			public double? CI2_AskPrice { get; set; }
			[Csv.Index(20)]
			[Csv.Name("CI2-AskAvail")]
			public int? CI2_AskAvail { get; set; }
			[Csv.Index(21)]
			[Csv.Name("CI2-BidAmt")]
			public int? CI2_BidAmt { get; set; }
			[Csv.Index(22)]
			[Csv.Name("CI2-BidPrice")]
			public double? CI2_BidPrice { get; set; }
			[Csv.Index(23)]
			[Csv.Name("CI2-BidAvail")]
			public int? CI2_BidAvail { get; set; }

			[Csv.Index(24)]
			[Csv.Name("NC1-Average")]
			public double? NC1_Average { get; set; }
			[Csv.Index(25)]
			[Csv.Name("NC1-AskAmt")]
			public int? NC1_AskAmt { get; set; }
			[Csv.Index(26)]
			[Csv.Name("NC1-AskPrice")]
			public double? NC1_AskPrice { get; set; }
			[Csv.Index(27)]
			[Csv.Name("NC1-AskAvail")]
			public int? NC1_AskAvail { get; set; }
			[Csv.Index(28)]
			[Csv.Name("NC1-BidAmt")]
			public int? NC1_BidAmt { get; set; }
			[Csv.Index(29)]
			[Csv.Name("NC1-BidPrice")]
			public double? NC1_BidPrice { get; set; }
			[Csv.Index(30)]
			[Csv.Name("NC1-BidAvail")]
			public int? NC1_BidAvail { get; set; }

			[Csv.Index(31)]
			[Csv.Name("NC2-Average")]
			public double? NC2_Average { get; set; }
			[Csv.Index(32)]
			[Csv.Name("NC2-AskAmt")]
			public int? NC2_AskAmt { get; set; }
			[Csv.Index(33)]
			[Csv.Name("NC2-AskPrice")]
			public double? NC2_AskPrice { get; set; }
			[Csv.Index(34)]
			[Csv.Name("NC2-AskAvail")]
			public int? NC2_AskAvail { get; set; }
			[Csv.Index(35)]
			[Csv.Name("NC2-BidAmt")]
			public int? NC2_BidAmt { get; set; }
			[Csv.Index(36)]
			[Csv.Name("NC2-BidPrice")]
			public double? NC2_BidPrice { get; set; }
			[Csv.Index(37)]
			[Csv.Name("NC2-BidAvail")]
			public int? NC2_BidAvail { get; set; }

			[Csv.Index(38)]
			[Csv.Name("IC1-Average")]
			public double? IC1_Average { get; set; }
			[Csv.Index(39)]
			[Csv.Name("IC1-AskAmt")]
			public int? IC1_AskAmt { get; set; }
			[Csv.Index(40)]
			[Csv.Name("IC1-AskPrice")]
			public double? IC1_AskPrice { get; set; }
			[Csv.Index(41)]
			[Csv.Name("IC1-AskAvail")]
			public int? IC1_AskAvail { get; set; }
			[Csv.Index(42)]
			[Csv.Name("IC1-BidAmt")]
			public int? IC1_BidAmt { get; set; }
			[Csv.Index(43)]
			[Csv.Name("IC1-BidPrice")]
			public double? IC1_BidPrice { get; set; }
			[Csv.Index(44)]
			[Csv.Name("IC1-BidAvail")]
			public int? IC1_BidAvail { get; set; }

			public object this[string propertyName]
			{
				get
				{
					// probably faster without reflection:
					// like:  return Properties.Settings.Default.PropertyValues[propertyName] 
					// instead of the following
					Type myType = typeof(CsvPrices);
					PropertyInfo myPropInfo = myType.GetProperty(propertyName);
					return myPropInfo.GetValue(this, null);
				}
				set
				{
					Type myType = typeof(CsvPrices);
					PropertyInfo myPropInfo = myType.GetProperty(propertyName);
					myPropInfo.SetValue(this, value, null);

				}
			}
		}

        private class CsvPricesExperimental
        {
            [Csv.Index(0)]
            public string Ticker { get; set; }
            [Csv.Index(1)]
            public double? MMBuy { get; set; }
            [Csv.Index(2)]
            public double? MMSell { get; set; }

            [Csv.Index(3)]
            [Csv.Name("AI1-Average")]
            public double? AI1_Average { get; set; }
			[Csv.Index(4)]
			[Csv.Name("AI1-Previous")]
			public double? AI1_Previous { get; set; }
            [Csv.Index(5)]
            [Csv.Name("AI1-AskAmt")]
            public int? AI1_AskAmt { get; set; }
            [Csv.Index(6)]
            [Csv.Name("AI1-AskPrice")]
            public double? AI1_AskPrice { get; set; }
            [Csv.Index(7)]
            [Csv.Name("AI1-AskAvail")]
            public int? AI1_AskAvail { get; set; }
            [Csv.Index(8)]
            [Csv.Name("AI1-BidAmt")]
            public int? AI1_BidAmt { get; set; }
            [Csv.Index(9)]
            [Csv.Name("AI1-BidPrice")]
            public double? AI1_BidPrice { get; set; }
            [Csv.Index(10)]
            [Csv.Name("AI1-BidAvail")]
            public int? AI1_BidAvail { get; set; }

            [Csv.Index(11)]
            [Csv.Name("CI1-Average")]
            public double? CI1_Average { get; set; }
            [Csv.Index(12)]
            [Csv.Name("CI1-Previous")]
            public double? CI1_Previous { get; set; }
            [Csv.Index(13)]
            [Csv.Name("CI1-AskAmt")]
            public int? CI1_AskAmt { get; set; }
            [Csv.Index(14)]
            [Csv.Name("CI1-AskPrice")]
            public double? CI1_AskPrice { get; set; }
            [Csv.Index(15)]
            [Csv.Name("CI1-AskAvail")]
            public int? CI1_AskAvail { get; set; }
            [Csv.Index(16)]
            [Csv.Name("CI1-BidAmt")]
            public int? CI1_BidAmt { get; set; }
            [Csv.Index(17)]
            [Csv.Name("CI1-BidPrice")]
            public double? CI1_BidPrice { get; set; }
            [Csv.Index(18)]
            [Csv.Name("CI1-BidAvail")]
            public int? CI1_BidAvail { get; set; }

            [Csv.Index(19)]
            [Csv.Name("CI2-Average")]
            public double? CI2_Average { get; set; }
            [Csv.Index(20)]
            [Csv.Name("CI2-Previous")]
            public double? CI2_Previous { get; set; }
            [Csv.Index(21)]
            [Csv.Name("CI2-AskAmt")]
            public int? CI2_AskAmt { get; set; }
            [Csv.Index(22)]
            [Csv.Name("CI2-AskPrice")]
            public double? CI2_AskPrice { get; set; }
            [Csv.Index(23)]
            [Csv.Name("CI2-AskAvail")]
            public int? CI2_AskAvail { get; set; }
            [Csv.Index(24)]
            [Csv.Name("CI2-BidAmt")]
            public int? CI2_BidAmt { get; set; }
            [Csv.Index(25)]
            [Csv.Name("CI2-BidPrice")]
            public double? CI2_BidPrice { get; set; }
            [Csv.Index(26)]
            [Csv.Name("CI2-BidAvail")]
            public int? CI2_BidAvail { get; set; }

            [Csv.Index(27)]
            [Csv.Name("NC1-Average")]
            public double? NC1_Average { get; set; }
            [Csv.Index(28)]
            [Csv.Name("NC1-Previous")]
            public double? NC1_Previous { get; set; }
            [Csv.Index(29)]
            [Csv.Name("NC1-AskAmt")]
            public int? NC1_AskAmt { get; set; }
            [Csv.Index(30)]
            [Csv.Name("NC1-AskPrice")]
            public double? NC1_AskPrice { get; set; }
            [Csv.Index(31)]
            [Csv.Name("NC1-AskAvail")]
            public int? NC1_AskAvail { get; set; }
            [Csv.Index(32)]
            [Csv.Name("NC1-BidAmt")]
            public int? NC1_BidAmt { get; set; }
            [Csv.Index(33)]
            [Csv.Name("NC1-BidPrice")]
            public double? NC1_BidPrice { get; set; }
            [Csv.Index(34)]
            [Csv.Name("NC1-BidAvail")]
            public int? NC1_BidAvail { get; set; }

            [Csv.Index(35)]
            [Csv.Name("NC2-Average")]
            public double? NC2_Average { get; set; }
            [Csv.Index(36)]
            [Csv.Name("NC21-Previous")]
            public double? NC2_Previous { get; set; }
            [Csv.Index(37)]
            [Csv.Name("NC2-AskAmt")]
            public int? NC2_AskAmt { get; set; }
            [Csv.Index(38)]
            [Csv.Name("NC2-AskPrice")]
            public double? NC2_AskPrice { get; set; }
            [Csv.Index(39)]
            [Csv.Name("NC2-AskAvail")]
            public int? NC2_AskAvail { get; set; }
            [Csv.Index(40)]
            [Csv.Name("NC2-BidAmt")]
            public int? NC2_BidAmt { get; set; }
            [Csv.Index(41)]
            [Csv.Name("NC2-BidPrice")]
            public double? NC2_BidPrice { get; set; }
            [Csv.Index(42)]
            [Csv.Name("NC2-BidAvail")]
            public int? NC2_BidAvail { get; set; }

            [Csv.Index(43)]
            [Csv.Name("IC1-Average")]
            public double? IC1_Average { get; set; }
            [Csv.Index(44)]
            [Csv.Name("IC1-Previous")]
            public double? IC1_Previous { get; set; }
            [Csv.Index(45)]
            [Csv.Name("IC1-AskAmt")]
            public int? IC1_AskAmt { get; set; }
            [Csv.Index(46)]
            [Csv.Name("IC1-AskPrice")]
            public double? IC1_AskPrice { get; set; }
            [Csv.Index(47)]
            [Csv.Name("IC1-AskAvail")]
            public int? IC1_AskAvail { get; set; }
            [Csv.Index(48)]
            [Csv.Name("IC1-BidAmt")]
            public int? IC1_BidAmt { get; set; }
            [Csv.Index(49)]
            [Csv.Name("IC1-BidPrice")]
            public double? IC1_BidPrice { get; set; }
            [Csv.Index(50)]
            [Csv.Name("IC1-BidAvail")]
            public int? IC1_BidAvail { get; set; }

            public object this[string propertyName]
            {
                get
                {
                    // probably faster without reflection:
                    // like:  return Properties.Settings.Default.PropertyValues[propertyName] 
                    // instead of the following
                    Type myType = typeof(CsvPrices);
                    PropertyInfo myPropInfo = myType.GetProperty(propertyName);
                    return myPropInfo.GetValue(this, null);
                }
                set
                {
                    Type myType = typeof(CsvPrices);
                    PropertyInfo myPropInfo = myType.GetProperty(propertyName);
                    myPropInfo.SetValue(this, value, null);

                }
            }
        }

        private class CXPricePiece
		{
			[Csv.Index(0)]
			public string Ticker { get; set; }

			[Csv.Index(1)]
			public string CXCode { get; set; }

			[Csv.Index(2)]
			public double? MMBuy { get; set; }

			[Csv.Index(3)]
			public double? MMSell { get; set; }

			[Csv.Index(4)]
			public double? Average { get; set; }

			[Csv.Index(5)]
			public int? AskAmt { get; set; }

			[Csv.Index(6)]
			public double? AskPrice { get; set; }

			[Csv.Index(7)]
			public int? AskAvail { get; set; }

			[Csv.Index(8)]
			public int? BidAmt { get; set; }

			[Csv.Index(9)]
			public double? BidPrice { get; set; }

			[Csv.Index(10)]
			public int? BidAvail { get; set; }
		}

		private CsvPrices createPriceFor(String matname, List<CXPricePiece> cxData)
		{
			var cxPrices = cxData.FindAll(p => p.Ticker == matname).ToList();
			var price = new CsvPrices
			{
				Ticker = matname,
				MMBuy = cxPrices.First().MMBuy,
				MMSell = cxPrices.First().MMSell,
			};
			foreach (var entry in cxPrices)
			{
				var cxid = entry.CXCode;
				price[$"{cxid}_Average"] = entry.Average;
				price[$"{cxid}_AskAmt"] = entry.AskAmt;
				price[$"{cxid}_AskPrice"] = entry.AskPrice;
				price[$"{cxid}_AskAvail"] = entry.AskAvail;
				price[$"{cxid}_BidAmt"] = entry.BidAmt;
				price[$"{cxid}_BidPrice"] = entry.BidPrice;
				price[$"{cxid}_BidAvail"] = entry.BidAvail;
			}
			return price;
		}

		private Response GetPrices()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				List<CXPricePiece> cxData = DB.CXDataModels
					.AsNoTracking()
					.Select(cx => new CXPricePiece
					{
						Ticker = cx.MaterialTicker,
						CXCode = cx.ExchangeCode,
						MMBuy = cx.MMBuy,
						MMSell = cx.MMSell,
						Average = cx.PriceAverage,
						AskAmt = cx.AskCount,
						AskPrice = cx.Ask,
						AskAvail = cx.Supply,
						BidAmt = cx.BidCount,
						BidPrice = cx.Bid,
						BidAvail = cx.Demand,
					})
					.ToList();
				List<string> cxs = cxData.Select(p => p.CXCode).Distinct().ToList();
				var matlist = cxData.Select(dm => dm.Ticker).Distinct().ToList();
				var prices = matlist.Select((matname) => createPriceFor(matname, cxData));
				var res = prices.OrderBy(r => r.Ticker).ToList();
				return res.GetCSVResponse();
			};
		}

        private class CXPricePieceExperimental
        {
            [Csv.Index(0)]
            public string Ticker { get; set; }

            [Csv.Index(1)]
            public string CXCode { get; set; }

            [Csv.Index(2)]
            public double? MMBuy { get; set; }

            [Csv.Index(3)]
            public double? MMSell { get; set; }

            [Csv.Index(4)]
            public double? Average { get; set; }

			[Csv.Index(5)]
			public double? Previous { get; set; }

            [Csv.Index(6)]
            public int? AskAmt { get; set; }

            [Csv.Index(7)]
            public double? AskPrice { get; set; }

            [Csv.Index(8)]
            public int? AskAvail { get; set; }

            [Csv.Index(9)]
            public int? BidAmt { get; set; }

            [Csv.Index(10)]
            public double? BidPrice { get; set; }

            [Csv.Index(11)]
            public int? BidAvail { get; set; }
        }

        private CsvPricesExperimental createPriceForExperimental(String matname, List<CXPricePieceExperimental> cxData)
        {
            var cxPrices = cxData.FindAll(p => p.Ticker == matname).ToList();
            var price = new CsvPricesExperimental
            {
                Ticker = matname,
                MMBuy = cxPrices.First().MMBuy,
                MMSell = cxPrices.First().MMSell,
            };
            foreach (var entry in cxPrices)
            {
                var cxid = entry.CXCode;
                price[$"{cxid}_Average"] = entry.Average;
				price[$"{cxid}_Previous"] = entry.Previous;
                price[$"{cxid}_AskAmt"] = entry.AskAmt;
                price[$"{cxid}_AskPrice"] = entry.AskPrice;
                price[$"{cxid}_AskAvail"] = entry.AskAvail;
                price[$"{cxid}_BidAmt"] = entry.BidAmt;
                price[$"{cxid}_BidPrice"] = entry.BidPrice;
                price[$"{cxid}_BidAvail"] = entry.BidAvail;
            }
            return price;
        }

        private Response GetPricesExperimental()
		{
            using (var DB = PRUNDataContext.GetNewContext())
            {
                List<CXPricePieceExperimental> cxData = DB.CXDataModels
                    .AsNoTracking()
                    .Select(cx => new CXPricePieceExperimental
                    {
                        Ticker = cx.MaterialTicker,
                        CXCode = cx.ExchangeCode,
                        MMBuy = cx.MMBuy,
                        MMSell = cx.MMSell,
                        Average = cx.PriceAverage,
						Previous = cx.Price,
                        AskAmt = cx.AskCount,
                        AskPrice = cx.Ask,
                        AskAvail = cx.Supply,
                        BidAmt = cx.BidCount,
                        BidPrice = cx.Bid,
                        BidAvail = cx.Demand,
                    })
                    .ToList();
                List<string> cxs = cxData.Select(p => p.CXCode).Distinct().ToList();
                var matlist = cxData.Select(dm => dm.Ticker).Distinct().ToList();
                var prices = matlist.Select((matname) => createPriceForExperimental(matname, cxData));
                var res = prices.OrderBy(r => r.Ticker).ToList();
                return res.GetCSVResponse();
            };
        }

        // Id,Ticker,CX,Type,Value
        private class CsvPricesCondensed
		{
			[Csv.Index(0)]
			public string Id { get; set; }

			[Csv.Index(1)]
			public string Ticker { get; set; }

			[Csv.Index(2)]
			public string CX { get; set; }

			[Csv.Index(3)]
			public string Type { get; set; }

			[Csv.Index(4)]
			public double? Value { get; set; }
		}

		private Response GetPricesCondensed()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var rawCXData = DB.CXDataModels
					.AsNoTracking()
					.OrderBy(cx => cx.MaterialTicker)
					.ToList();
				var rainPricesCondensed = rawCXData.SelectMany(cx => new[]
					{
						new CsvPricesCondensed
						{
							Id = $"{cx.MaterialTicker}-{cx.ExchangeCode}-ask",
							Ticker = cx.MaterialTicker,
							CX = cx.ExchangeCode,
							Type = "ask",
							Value = cx.Ask
						},
						new CsvPricesCondensed
						{
							Id = $"{cx.MaterialTicker}-{cx.ExchangeCode}-avg",
							Ticker = cx.MaterialTicker,
							CX = cx.ExchangeCode,
							Type = "avg",
							Value = cx.PriceAverage
						},
						new CsvPricesCondensed
						{
							Id = $"{cx.MaterialTicker}-{cx.ExchangeCode}-bid",
							Ticker = cx.MaterialTicker,
							CX = cx.ExchangeCode,
							Type = "bid",
							Value = cx.Bid
						},
						new CsvPricesCondensed
						{
							Id = $"{cx.MaterialTicker}-{cx.ExchangeCode}-mm-buy",
							Ticker = cx.MaterialTicker,
							CX = cx.ExchangeCode,
							Type = "mm-buy",
							Value = cx.MMBuy
						},
						new CsvPricesCondensed
						{
							Id = $"{cx.MaterialTicker}-{cx.ExchangeCode}-mm-sell",
							Ticker = cx.MaterialTicker,
							CX = cx.ExchangeCode,
							Type = "mm-sell",
							Value = cx.MMSell
						}
					}).ToList();

				return rainPricesCondensed.GetCSVResponse();
			}
		}

		public class CsvOrderOrBid
		{
			[Csv.Index(0)]
			public string MaterialTicker { get; set; }

			[Csv.Index(1)]
			public string ExchangeCode { get; set; }

			[Csv.Index(2)]
			public string CompanyId { get; set; }

			[Csv.Index(3)]
			public string CompanyName { get; set; }

			[Csv.Index(4)]
			public string CompanyCode { get; set; }

			[Csv.Index(5)]
			public int ItemCount { get; set; } // -1 = infinite

			[Csv.Index(6)]
			public double ItemCost { get; set; }
		}

		private Response GetOrders()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var allData = DB.CXDataModels
					.AsNoTracking()
					.Include(cxdm => cxdm.BuyingOrders)
					.Include(cxdm => cxdm.SellingOrders)
					.OrderBy(cxdm => cxdm.MaterialTicker)
						.ThenBy(cxdm => cxdm.ExchangeCode)
					.AsSplitQuery()
					.ToList();

				var orders = new List<CsvOrderOrBid>();
				foreach (var data in allData)
				{
					foreach (var entry in data.SellingOrders)
					{
						orders.Add(new CsvOrderOrBid
						{
							MaterialTicker = data.MaterialTicker,
							ExchangeCode = data.ExchangeCode,
							CompanyId = entry.CompanyId,
							CompanyName = entry.CompanyName,
							CompanyCode = entry.CompanyCode,
							ItemCount = (entry.ItemCount != null) ? (int)entry.ItemCount : -1,
							ItemCost = entry.ItemCost
						});
					}
				}

				return orders.GetCSVResponse();
			}
		}

		private Response GetBids()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var allData = DB.CXDataModels
					.AsNoTracking()
					.Include(cxdm => cxdm.BuyingOrders)
					.Include(cxdm => cxdm.SellingOrders)
					.OrderBy(cxdm => cxdm.MaterialTicker)
						.ThenBy(cxdm => cxdm.ExchangeCode)
					.AsSplitQuery()
					.ToList();

				var orders = new List<CsvOrderOrBid>();
				foreach (var data in allData)
				{
					foreach (var entry in data.BuyingOrders)
					{
						orders.Add(new CsvOrderOrBid
						{
							MaterialTicker = data.MaterialTicker,
							ExchangeCode = data.ExchangeCode,
							CompanyId = entry.CompanyId,
							CompanyName = entry.CompanyName,
							CompanyCode = entry.CompanyCode,
							ItemCount = (entry.ItemCount != null) ? (int)entry.ItemCount : -1,
							ItemCost = entry.ItemCost
						});
					}
				}

				return orders.GetCSVResponse();
			}
		}

		public class CsvRecipeInput
		{
			[Csv.Index(0)]
			public string Key { get; set; } // BUI-OUTPUT

			[Csv.Index(1)]
			public string Material { get; set; }

			[Csv.Index(2)]
			public int Amount { get; set; }
		}

		private Response GetRecipeInputs()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var recipeInputs = new List<CsvRecipeInput>();
				var buildings = DB.Buildings
					.AsNoTracking()
					.OrderBy(b => b.Ticker)
					.Include(b => b.Recipes)
						.ThenInclude(r => r.Outputs.OrderBy(o => o.CommodityTicker))
					.Include(b => b.Recipes)
						.ThenInclude(r => r.Inputs.OrderBy(i => i.CommodityTicker))
					.AsSplitQuery()
					.ToList();

				foreach (var building in buildings)
				{
					foreach (var recipe in building.Recipes.Where(r => r.Outputs.Count > 0))
					{
						string RecipeKey = recipe.GetRecipeDescription();

						foreach (var input in recipe.Inputs)
						{
							var cri = new CsvRecipeInput();
							cri.Key = RecipeKey;
							cri.Material = input.CommodityTicker;
							cri.Amount = input.Amount;

							recipeInputs.Add(cri);
						}
					}
				}

				return recipeInputs.GetCSVResponse();
			}
		}

		public class CsvRecipeOutput
		{
			[Csv.Index(0)]
			public string Key { get; set; } // BUI-OUTPUT

			[Csv.Index(1)]
			public string Material { get; set; } // Ticker

			[Csv.Index(2)]
			public int Amount { get; set; }
		}

		private Response GetRecipeOutputs()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var recipeOutputs = new List<CsvRecipeOutput>();
				var buildings = DB.Buildings
					.AsNoTracking()
					.OrderBy(b => b.Ticker)
					.Include(b => b.Recipes)
						.ThenInclude(r => r.Outputs.OrderBy(o => o.CommodityTicker))
					.Include(b => b.Recipes)
						.ThenInclude(r => r.Inputs.OrderBy(i => i.CommodityTicker))
					.AsSplitQuery()
					.ToList();

				foreach (var building in buildings)
				{
					foreach (var recipe in building.Recipes.Where(r => r.Outputs.Count > 0))
					{
						string RecipeKey = recipe.GetRecipeDescription();

						foreach (var output in recipe.Outputs)
						{
							var cro = new CsvRecipeOutput();
							cro.Key = RecipeKey;
							cro.Material = output.CommodityTicker;
							cro.Amount = output.Amount;

							recipeOutputs.Add(cro);
						}
					}
				}

				return recipeOutputs.GetCSVResponse();
			}
		}

		public class CsvPlanet
		{
			[Csv.Index(0)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(1)]
			public string PlanetName { get; set; }
		}

		private Response GetPlanets()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var allPlanets = new List<CsvPlanet>();
				var planets = DB.PlanetDataModels
					.AsNoTracking()
					.OrderBy(pdm => pdm.PlanetNaturalId)
					.AsSplitQuery()
					.ToList();

				foreach (var p in planets)
				{
					var planet = new CsvPlanet();

					planet.PlanetNaturalId = p.PlanetNaturalId;
					planet.PlanetName = p.PlanetName;

					allPlanets.Add(planet);
				}

				return allPlanets.GetCSVResponse();
			}
		}

		public class CsvPlanetResource
		{
			[Csv.Index(0)]
			public string Key { get; set; } // PlanetNaturalId-MAT

			[Csv.Index(1)]
			public string Planet { get; set; }

			[Csv.Index(2)]
			public string Ticker { get; set; }

			[Csv.Index(3)]
			public string Type { get; set; }

			[Csv.Index(4)]
			public double Factor { get; set; }
		}

		private Response GetPlanetResources()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var allPlanetResources = new List<CsvPlanetResource>();
				var planets = DB.PlanetDataModels
					.AsNoTracking()
					.OrderBy(pdm => pdm.PlanetNaturalId)
					.Include(pdm => pdm.Resources)
					.AsSplitQuery()
					.ToList();
				var planetMaterialList = planets
					.SelectMany(p => p.Resources.Select(r => r.MaterialId))
					.Distinct()
					.ToList();
				var materials = DB.Materials
					.AsNoTracking()
					.Where(m => planetMaterialList.Contains(m.MaterialId))
					.ToList();
				foreach (var planet in planets)
				{
					var planetResources = new List<CsvPlanetResource>();
					foreach (var resource in planet.Resources)
					{
						var planetResource = new CsvPlanetResource();

						planetResource.Planet = planet.PlanetNaturalId;
						planetResource.Ticker = materials.Where(m => m.MaterialId == resource.MaterialId).FirstOrDefault().Ticker;
						planetResource.Type = resource.ResourceType;
						planetResource.Factor = resource.Factor;
						planetResource.Key = $"{planetResource.Planet}-{planetResource.Ticker}";
						planetResources.Add(planetResource);
					}

					planetResources = planetResources.OrderBy(pr => pr.Ticker).ToList();
					allPlanetResources.AddRange(planetResources);
				}

				return allPlanetResources.GetCSVResponse();
			}
		}

		public class CsvPlanetProductionFee
		{
			[Csv.Index(0)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(1)]
			public string PlanetName { get; set; }

			[Csv.Index(2)]
			public string Category { get; set; }

			[Csv.Index(3)]
			public string WorkforceLevel { get; set; }

			[Csv.Index(4)]
			public double FeeAmount { get; set; }

			[Csv.Index(5)]
			public string FeeCurrency { get; set; }
		}

		private Response GetPlanetProductionFees()
		{
			var allPlanetProductionFees = new List<CsvPlanetProductionFee>();
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var populatedPlanetIds = DB.PlanetSites
					.AsNoTracking()
					.GroupBy(ps => ps.PlanetId)
					.Select(gr => new
					{
						PlanetId = gr.Key,
						Count = gr.Count()
					})
					.Where(ps => ps.Count > 1)
					.Select(ps => ps.PlanetId)
					.ToList();

				var planets = DB.PlanetDataModels
					.AsNoTracking()
					.OrderBy(pdm => pdm.PlanetNaturalId)
					.Include(pdm => pdm.ProductionFees)
					.Where(pdm => populatedPlanetIds.Contains(pdm.PlanetId))
					.AsSplitQuery()
					.ToList();
				
				foreach (var planet in planets)
				{
					foreach (var productionFee in planet.ProductionFees)
					{
						var planetProductionFee = new CsvPlanetProductionFee();
						planetProductionFee.PlanetNaturalId = planet.PlanetNaturalId;
						planetProductionFee.PlanetName = planet.PlanetName;
						planetProductionFee.Category = productionFee.Category;
						planetProductionFee.WorkforceLevel = productionFee.WorkforceLevel;
						planetProductionFee.FeeAmount = productionFee.FeeAmount;
						planetProductionFee.FeeCurrency = productionFee.FeeCurrency;
						allPlanetProductionFees.Add(planetProductionFee);
					}
				}
			}

			allPlanetProductionFees = allPlanetProductionFees.OrderBy(appf => appf.PlanetNaturalId).ToList();
			return allPlanetProductionFees.GetCSVResponse();
		}

		public class CsvPlanetDetail
		{
			[Csv.Index(0)]
			public string PlanetId { get; set; }

			[Csv.Index(1)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(2)]
			public string PlanetName { get; set; }

			[Csv.Index(3)]
			public string Namer { get; set; }

			[Csv.Index(4)]
			public bool Nameable { get; set; }

			[Csv.Index(5)]
			public string SystemId { get; set; }

			[Csv.Index(6)]
			public double Gravity { get; set; }

			[Csv.Index(7)]
			public double MagneticField { get; set; }

			[Csv.Index(8)]
			public double Mass { get; set; }

			[Csv.Index(9)]
			public double OrbitSemiMajorAxis { get; set; }

			[Csv.Index(10)]
			public double OrbitEccentricity { get; set; }

			[Csv.Index(11)]
			public int OrbitIndex { get; set; }

			[Csv.Index(12)]
			public double Pressure { get; set; }

			[Csv.Index(13)]
			public double Radius { get; set; }

			[Csv.Index(14)]
			public double Sunlight { get; set; }

			[Csv.Index(15)]
			public bool Surface { get; set; }

			[Csv.Index(16)]
			public double Temperature { get; set; }

			[Csv.Index(17)]
			public double Fertility { get; set; }

			[Csv.Index(18)]
			public bool HasLocalMarket { get; set; }

			[Csv.Index(19)]
			public bool HasChamberOfCommerce { get; set; }

			[Csv.Index(20)]
			public bool HasWarehouse { get; set; }

			[Csv.Index(21)]
			public bool HasAdministrationCenter { get; set; }

			[Csv.Index(22)]
			public bool HasShipyard { get; set; }

			[Csv.Index(23)]
			public string FactionCode { get; set; }

			[Csv.Index(24)]
			public string FactionName { get; set; }

			[Csv.Index(25)]
			public string GovernorId { get; set; }

			[Csv.Index(26)]
			public string GovernorUserName { get; set; }

			[Csv.Index(27)]
			public string GovernorCorporationId { get; set; }

			[Csv.Index(28)]
			public string GovernorCorporationName { get; set; }

			[Csv.Index(29)]
			public string GovernorCorporationCode { get; set; }

			[Csv.Index(30)]
			public string CurrencyName { get; set; }

			[Csv.Index(31)]
			public string CurrencyCode { get; set; }

			[Csv.Index(32)]
			public string CollectorId { get; set; }

			[Csv.Index(33)]
			public string CollectorName { get; set; }

			[Csv.Index(34)]
			public string CollectorCode { get; set; }

			[Csv.Index(35)]
			public double? BaseLocalMarketFee { get; set; }

			[Csv.Index(36)]
			public double? WarehouseFee { get; set; }

			[Csv.Index(37)]
			public string PopulationId { get; set; }

			[Csv.Index(38)]
			public string COGCProgramStatus { get; set; }

			[Csv.Index(39)]
			public int PlanetTier { get; set; }

			[Csv.Index(40)]
			public long EpochTimestampMs { get; set; }
		}

		private Response GetPlanetDetail()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var allPlanets = DB.PlanetDataModels
					.AsNoTracking()
					.OrderBy(pdm => pdm.PlanetNaturalId)
					.Select(pdm => new CsvPlanetDetail
					{
						PlanetId = pdm.PlanetId,
						PlanetNaturalId = pdm.PlanetNaturalId,
						PlanetName = pdm.PlanetName,
						Namer = pdm.Namer,
						Nameable = pdm.Nameable,
						SystemId = pdm.SystemId,
						Gravity = pdm.Gravity,
						MagneticField = pdm.MagneticField,
						Mass = pdm.Mass,
						OrbitSemiMajorAxis = pdm.OrbitSemiMajorAxis,
						OrbitEccentricity = pdm.OrbitEccentricity,
						OrbitIndex = pdm.OrbitIndex,
						Pressure = pdm.Pressure,
						Radius = pdm.Radius,
						Sunlight = pdm.Sunlight,
						Surface = pdm.Surface,
						Temperature = pdm.Temperature,
						Fertility = pdm.Fertility,
						HasLocalMarket = pdm.HasLocalMarket,
						HasChamberOfCommerce = pdm.HasChamberOfCommerce,
						HasWarehouse = pdm.HasWarehouse,
						HasAdministrationCenter = pdm.HasAdministrationCenter,
						HasShipyard = pdm.HasShipyard,
						FactionCode = pdm.FactionCode,
						FactionName = pdm.FactionName,
						GovernorId = pdm.GovernorId,
						GovernorUserName = pdm.GovernorUserName,
						GovernorCorporationId = pdm.GovernorCorporationId,
						GovernorCorporationName = pdm.GovernorCorporationName,
						GovernorCorporationCode = pdm.GovernorCorporationCode,
						CurrencyName = pdm.CurrencyName,
						CurrencyCode = pdm.CurrencyCode,
						CollectorId = pdm.CollectorId,
						CollectorName = pdm.CollectorName,
						CollectorCode = pdm.CollectorCode,
						BaseLocalMarketFee = pdm.BaseLocalMarketFee,
						WarehouseFee = pdm.WarehouseFee,
						PopulationId = pdm.PopulationId,
						COGCProgramStatus = pdm.COGCProgramStatus,
						PlanetTier = pdm.PlanetTier,
						EpochTimestampMs = pdm.Timestamp.ToEpochMs(),
					})
					.ToList();

				return allPlanets.GetCSVResponse();
			}
		}

		public class CsvSystem
		{
			[Csv.Index(0)]
			public string NaturalId { get; set; }

			[Csv.Index(1)]
			public string Name { get; set; }

			[Csv.Index(2)]
			public string Type { get; set; }

			[Csv.Index(3)]
			public double PositionX { get; set; }

			[Csv.Index(4)]
			public double PositionY { get; set; }

			[Csv.Index(5)]
			public double PositionZ { get; set; }

			[Csv.Index(6)]
			public string SectorId { get; set; }

			[Csv.Index(7)]
			public string SubSectorId { get; set; }
		}

		private Response GetSystems()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var res = DB.SystemStars
					.AsNoTracking()
					.OrderBy(ssm => ssm.SystemNaturalId)
					.Select(ssm => new CsvSystem
					{
						NaturalId = ssm.SystemNaturalId,
						Name = ssm.SystemName,
						Type = ssm.Type,
						PositionX = ssm.PositionX,
						PositionY = ssm.PositionY,
						PositionZ = ssm.PositionZ,
						SectorId = ssm.SectorId,
						SubSectorId = ssm.SubSectorId
					})
					.ToList();

				return res.GetCSVResponse();
			}
		}

		public class CsvSystemLink
		{
			[Csv.Index(0)]
			public string Left { get; set; }

			[Csv.Index(1)]
			public string Right { get; set; }
		}

		private Response GetSystemLinks()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var systemLinks = new List<CsvSystemLink>();
				var systems = DB.Systems
					.AsNoTracking()
					.OrderBy(ssm => ssm.NaturalId)
					.Include(ssm => ssm.Connections)
					.ToList();
				var otherSystemIds = systems
					.SelectMany(s => s.Connections.Select(c => c.SystemId).ToList())
					.Distinct()
					.ToList();
				var connectedSystems = DB.SystemStars
					.AsNoTracking()
					.Where(ossm => otherSystemIds.Contains(ossm.SystemId))
					.Select(ossm => new { ossm.SystemId, ossm.SystemNaturalId })
					.ToList();
				foreach (var system in systems)
				{
					var RainConnections = new List<string>();
					foreach (var connection in system.Connections)
					{
						var otherSystem = connectedSystems.Find(cs => cs.SystemId == connection.SystemId);
						if (otherSystem != null)
						{
							RainConnections.Add(otherSystem.SystemNaturalId);
						}
					}

					RainConnections.Sort();
					foreach (var RainConnection in RainConnections)
					{
						var link = new CsvSystemLink();

						link.Left = system.NaturalId;
						link.Right = RainConnection;

						systemLinks.Add(link);
					}
				}

				return systemLinks.GetCSVResponse();
			}
		}

		public class CsvSystemPlanet
		{
			[Csv.Index(0)]
			public string NaturalId { get; set; }

			[Csv.Index(1)]
			public string Name { get; set; }

			[Csv.Index(2)]
			public double Radiation { get; set; }

			[Csv.Index(3)]
			public double Pressure { get; set; }

			[Csv.Index(4)]
			public int OrbitIndex { get; set; }

			[Csv.Index(5)]
			public double Fertility { get; set; } // -1 = infertile

			[Csv.Index(6)]
			public double Sunlight { get; set; }

			[Csv.Index(7)]
			public bool Surface { get; set; }

			[Csv.Index(8)]
			public double Gravity { get; set; }

			[Csv.Index(9)]
			public double Radius { get; set; }

			[Csv.Index(10)]
			public double Temperature { get; set; }

			[Csv.Index(11)]
			public double Mass { get; set; }

			[Csv.Index(12)]
			public double MagneticField { get; set; }

			[Csv.Index(13)]
			public double MassEarth { get; set; }
		}

		private Response GetSystemPlanets()
		{
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var res = DB.PlanetDataModels
					.AsNoTracking()
					.OrderBy(pdm => pdm.PlanetNaturalId)
					.Select(pdm => new CsvSystemPlanet
					{
						NaturalId = pdm.PlanetNaturalId,
						Name = pdm.PlanetName,
						Radiation = pdm.Radiation,
						Pressure = pdm.Pressure,
						OrbitIndex = pdm.OrbitIndex,
						Fertility = pdm.Fertility,
						Sunlight = pdm.Sunlight,
						Surface = pdm.Surface,
						Gravity = pdm.Gravity,
						Radius = pdm.Radius,
						Temperature = pdm.Temperature,
						Mass = pdm.Mass,
						MagneticField = pdm.MagneticField,
						MassEarth = pdm.MassEarth
					}).ToList();

				return res.GetCSVResponse();
			}
		}

		public class CsvWorkforceNeed
		{
			[Csv.Index(0)]
			public string WorkforceType { get; set; }

			[Csv.Index(1)]
			public string MaterialTicker { get; set; }

			[Csv.Index(2)]
			public string MaterialCategory { get; set; }

			[Csv.Index(3)]
			public double AmountPerOneHundred { get; set; }
		}

		private Response GetWorkforceNeeds()
		{
			var csvNeedsResult = new List<CsvWorkforceNeed>();
			using (var DB = PRUNDataContext.GetNewContext())
			{
				var allWorkforceTypes = DB.WorkforcePerOneHundreds
					.AsNoTracking()
					.Include(wpoh => wpoh.Needs)
					.OrderBy(wpoh => wpoh.WorkforceType)
					.ToList();
				foreach (var type in allWorkforceTypes)
				{
					var needs = type.Needs
						.Select(n => new CsvWorkforceNeed
						{
							WorkforceType = type.WorkforceType,
							MaterialTicker = n.MaterialTicker,
							MaterialCategory = n.MaterialCategory,
							AmountPerOneHundred = n.Amount,
						})
						.OrderBy(n => n.MaterialTicker)
						.ToList();

					csvNeedsResult.AddRange(needs);
				}

				return csvNeedsResult.GetCSVResponse();
			}
		}

		public class CsvInfrastructureReport
		{
			[Csv.Index(1)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(2)]
			public string PlanetName { get; set; }

			[Csv.Index(3)]
			public bool ExplorersGraceEnabled { get; set; }

			[Csv.Index(4)]
			public int SimulationPeriod { get; set; }

			[Csv.Index(5)]
			public long TimestampMs { get; set; }

			[Csv.Index(6)]
			public int NextPopulationPioneer { get; set; }

			[Csv.Index(7)]
			public int NextPopulationSettler { get; set; }

			[Csv.Index(8)]
			public int NextPopulationTechnician { get; set; }

			[Csv.Index(9)]
			public int NextPopulationEngineer { get; set; }

			[Csv.Index(10)]
			public int NextPopulationScientist { get; set; }

			[Csv.Index(11)]
			public int PopulationDifferencePioneer { get; set; }

			[Csv.Index(12)]
			public int PopulationDifferenceSettler { get; set; }

			[Csv.Index(13)]
			public int PopulationDifferenceTechnician { get; set; }

			[Csv.Index(14)]
			public int PopulationDifferenceEngineer { get; set; }

			[Csv.Index(15)]
			public int PopulationDifferenceScientist { get; set; }

			[Csv.Index(16)]
			public double AverageHappinessPioneer { get; set; }

			[Csv.Index(17)]
			public double AverageHappinessSettler { get; set; }

			[Csv.Index(18)]
			public double AverageHappinessTechnician { get; set; }

			[Csv.Index(19)]
			public double AverageHappinessEngineer { get; set; }

			[Csv.Index(20)]
			public double AverageHappinessScientist { get; set; }

			[Csv.Index(21)]
			public double UnemploymentRatePioneer { get; set; }

			[Csv.Index(22)]
			public double UnemploymentRateSettler { get; set; }

			[Csv.Index(23)]
			public double UnemploymentRateTechnician { get; set; }

			[Csv.Index(24)]
			public double UnemploymentRateEngineer { get; set; }

			[Csv.Index(25)]
			public double UnemploymentRateScientist { get; set; }

			[Csv.Index(26)]
			public float OpenJobsPioneer { get; set; }

			[Csv.Index(27)]
			public float OpenJobsSettler { get; set; }

			[Csv.Index(28)]
			public float OpenJobsTechnician { get; set; }

			[Csv.Index(29)]
			public float OpenJobsEngineer { get; set; }

			[Csv.Index(30)]
			public float OpenJobsScientist { get; set; }

			[Csv.Index(31)]
			public double NeedFulfillmentLifeSupport { get; set; }

			[Csv.Index(32)]
			public double NeedFulfillmentSafety { get; set; }

			[Csv.Index(33)]
			public double NeedFulfillmentHealth { get; set; }

			[Csv.Index(34)]
			public double NeedFulfillmentComfort { get; set; }

			[Csv.Index(35)]
			public double NeedFulfillmentCulture { get; set; }

			[Csv.Index(36)]
			public double NeedFulfillmentEducation { get; set; }
		}

		private Response GetInfrastructureReport(string planet = null, bool bOnlyLatest = false)
		{
			var infrastructureReports = new List<CsvInfrastructureReport>();

			using (var DB = PRUNDataContext.GetNewContext())
			{
				var worlds = DB.PlanetDataModels
					.ToList();

				foreach (var w in worlds)
				{
					var inf = DB.Infrastructures
						.AsNoTracking()
						.Where(i => i.InfrastructureId == w.PopulationId)
						.Include(i => i.InfrastructureReports)
						.AsSplitQuery()
						.FirstOrDefault();
					if (inf?.InfrastructureReports?.Count > 0)
					{
						infrastructureReports.AddRange(
							inf.InfrastructureReports
							.OrderBy(ir => ir.SimulationPeriod)
							.Select(r => new CsvInfrastructureReport
							{
								PlanetName = w.PlanetName,
								PlanetNaturalId = w.PlanetNaturalId,
								ExplorersGraceEnabled = r.ExplorersGraceEnabled,
								SimulationPeriod = r.SimulationPeriod,
								TimestampMs = r.TimestampMs,
								NextPopulationPioneer = r.NextPopulationPioneer,
								NextPopulationSettler = r.NextPopulationSettler,
								NextPopulationTechnician = r.NextPopulationTechnician,
								NextPopulationEngineer = r.NextPopulationEngineer,
								NextPopulationScientist = r.NextPopulationScientist,
								PopulationDifferencePioneer = r.PopulationDifferencePioneer,
								PopulationDifferenceSettler = r.PopulationDifferenceSettler,
								PopulationDifferenceTechnician = r.PopulationDifferenceTechnician,
								PopulationDifferenceEngineer = r.PopulationDifferenceEngineer,
								PopulationDifferenceScientist = r.PopulationDifferenceScientist,
								AverageHappinessPioneer = r.AverageHappinessPioneer,
								AverageHappinessSettler = r.AverageHappinessSettler,
								AverageHappinessTechnician = r.AverageHappinessTechnician,
								AverageHappinessEngineer = r.AverageHappinessEngineer,
								AverageHappinessScientist = r.AverageHappinessScientist,
								UnemploymentRatePioneer = r.UnemploymentRatePioneer,
								UnemploymentRateSettler = r.UnemploymentRateSettler,
								UnemploymentRateTechnician = r.UnemploymentRateTechnician,
								UnemploymentRateEngineer = r.UnemploymentRateEngineer,
								UnemploymentRateScientist = r.UnemploymentRateScientist,
								OpenJobsPioneer = r.OpenJobsPioneer,
								OpenJobsSettler = r.OpenJobsSettler,
								OpenJobsTechnician = r.OpenJobsTechnician,
								OpenJobsEngineer = r.OpenJobsEngineer,
								OpenJobsScientist = r.OpenJobsScientist,
								NeedFulfillmentLifeSupport = r.NeedFulfillmentLifeSupport,
								NeedFulfillmentSafety = r.NeedFulfillmentSafety,
								NeedFulfillmentHealth = r.NeedFulfillmentHealth,
								NeedFulfillmentComfort = r.NeedFulfillmentComfort,
								NeedFulfillmentCulture = r.NeedFulfillmentCulture,
								NeedFulfillmentEducation = r.NeedFulfillmentEducation,
							})
						);
					}
				}

				return infrastructureReports.GetCSVResponse();
			}
		}

		private Response GetInfrastructureReport(string planet)
		{
			var infrastructureReports = new List<CsvInfrastructureReport>();
			planet = planet.ToUpper();

			using (var DB = PRUNDataContext.GetNewContext())
			{
				var world = DB.PlanetDataModels
					.AsNoTracking()
					.Where(p => p.PlanetId.ToUpper() == planet || p.PlanetNaturalId.ToUpper() == planet || p.PlanetName.ToUpper() == planet)
					.FirstOrDefault();

				if (world != null)
				{
					var inf = DB.Infrastructures
						.AsNoTracking()
						.Where(i => i.InfrastructureId == world.PopulationId)
						.Include(i => i.InfrastructureReports)
						.AsSplitQuery()
						.FirstOrDefault();

					if (inf?.InfrastructureReports?.Count > 0)
					{
						infrastructureReports.AddRange(
							inf.InfrastructureReports
							.OrderBy(ir => ir.SimulationPeriod)
							.Select(r => new CsvInfrastructureReport
							{
								PlanetName = world.PlanetName,
								PlanetNaturalId = world.PlanetNaturalId,
								ExplorersGraceEnabled = r.ExplorersGraceEnabled,
								SimulationPeriod = r.SimulationPeriod,
								TimestampMs = r.TimestampMs,
								NextPopulationPioneer = r.NextPopulationPioneer,
								NextPopulationSettler = r.NextPopulationSettler,
								NextPopulationTechnician = r.NextPopulationTechnician,
								NextPopulationEngineer = r.NextPopulationEngineer,
								NextPopulationScientist = r.NextPopulationScientist,
								PopulationDifferencePioneer = r.PopulationDifferencePioneer,
								PopulationDifferenceSettler = r.PopulationDifferenceSettler,
								PopulationDifferenceTechnician = r.PopulationDifferenceTechnician,
								PopulationDifferenceEngineer = r.PopulationDifferenceEngineer,
								PopulationDifferenceScientist = r.PopulationDifferenceScientist,
								AverageHappinessPioneer = r.AverageHappinessPioneer,
								AverageHappinessSettler = r.AverageHappinessSettler,
								AverageHappinessTechnician = r.AverageHappinessTechnician,
								AverageHappinessEngineer = r.AverageHappinessEngineer,
								AverageHappinessScientist = r.AverageHappinessScientist,
								UnemploymentRatePioneer = r.UnemploymentRatePioneer,
								UnemploymentRateSettler = r.UnemploymentRateSettler,
								UnemploymentRateTechnician = r.UnemploymentRateTechnician,
								UnemploymentRateEngineer = r.UnemploymentRateEngineer,
								UnemploymentRateScientist = r.UnemploymentRateScientist,
								OpenJobsPioneer = r.OpenJobsPioneer,
								OpenJobsSettler = r.OpenJobsSettler,
								OpenJobsTechnician = r.OpenJobsTechnician,
								OpenJobsEngineer = r.OpenJobsEngineer,
								OpenJobsScientist = r.OpenJobsScientist,
								NeedFulfillmentLifeSupport = r.NeedFulfillmentLifeSupport,
								NeedFulfillmentSafety = r.NeedFulfillmentSafety,
								NeedFulfillmentHealth = r.NeedFulfillmentHealth,
								NeedFulfillmentComfort = r.NeedFulfillmentComfort,
								NeedFulfillmentCulture = r.NeedFulfillmentCulture,
								NeedFulfillmentEducation = r.NeedFulfillmentEducation,
							})
							);
					}
				}
			}

			return infrastructureReports.GetCSVResponse();
		}

		public class CsvInfrastructureInfo
		{
			[Csv.Index(1)]
			public string PlanetName { get; set; }

			[Csv.Index(2)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(3)]
			public string Type { get; set; }

			[Csv.Index(4)]
			public string Ticker { get; set; }

			[Csv.Index(5)]
			public string Name { get; set; }

			[Csv.Index(6)]
			public string ProjectId { get; set; }

			[Csv.Index(7)]
			public int Level { get; set; }

			[Csv.Index(8)]
			public int ActiveLevel { get; set; }

			[Csv.Index(9)]
			public int CurrentLevel { get; set; }

			[Csv.Index(10)]
			public double UpkeepStatus { get; set; }

			[Csv.Index(11)]
			public double UpgradeStatus { get; set; }
		}

		private Response GetInfrastructureInfo(string planet = null, bool bOnlyLatest = false)
		{
			var infrastructureInfos = new List<CsvInfrastructureInfo>();

			List<PlanetDataModel> worlds = null;
			using (var DB = PRUNDataContext.GetNewContext())
			{
				if (planet != null)
				{
					planet = planet.ToUpper();
					worlds = DB.PlanetDataModels
						.AsNoTracking()
						.Where(p => p.PlanetId.ToUpper() == planet || p.PlanetNaturalId.ToUpper() == planet || p.PlanetName.ToUpper() == planet)
						.ToList();
				}
				else
				{
					worlds = DB.PlanetDataModels.ToList();
				}

				foreach (var w in worlds)
				{
					var inf = DB.Infrastructures
						.AsNoTracking()
						.Where(i => i.InfrastructureId == w.PopulationId)
						.Include(i => i.InfrastructureProjects)
						.FirstOrDefault();

					if (inf != null)
					{
						infrastructureInfos.AddRange(
							inf.InfrastructureProjects
							.Where(ir => (ir.Level > 0 || ir.UpgradeStatus > 0.0))
							.OrderBy(ir => ir.Ticker)
							.Select(ir => new CsvInfrastructureInfo
							{
								PlanetName = w.PlanetName,
								PlanetNaturalId = w.PlanetNaturalId,
								Ticker = ir.Ticker,
								Name = ir.Name,
								ProjectId = ir.InfrastructureProjectId,
								Level = ir.Level,
								ActiveLevel = ir.ActiveLevel,
								CurrentLevel = ir.CurrentLevel,
								UpkeepStatus = ir.UpkeepStatus,
								UpgradeStatus = ir.UpgradeStatus
							}));
					}
				}

				return infrastructureInfos.GetCSVResponse();
			}
		}

		public class CsvLMBuySell
		{
			[Csv.Index(0)]
			public int ContractNaturalId { get; set; }

			[Csv.Index(1)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(2)]
			public string PlanetName { get; set; }

			[Csv.Index(3)]
			public string CreatorCompanyName { get; set; }

			[Csv.Index(4)]
			public string CreatorCompanyCode { get; set; }

			[Csv.Index(5)]
			public string MaterialName { get; set; }

			[Csv.Index(6)]
			public string MaterialTicker { get; set; }

			[Csv.Index(7)]
			public string MaterialCategory { get; set; }

			[Csv.Index(8)]
			public double MaterialWeight { get; set; }

			[Csv.Index(9)]
			public double MaterialVolume { get; set; }

			[Csv.Index(10)]
			public int MaterialAmount { get; set; }

			[Csv.Index(11)]
			public double Price { get; set; }

			[Csv.Index(12)]
			public string PriceCurrency { get; set; }

			[Csv.Index(13)]
			public int DeliveryTime { get; set; }

			[Csv.Index(14)]
			public long CreationTimeEpochMs { get; set; }

			[Csv.Index(15)]
			public long ExpiryTimeEpochMs { get; set; }

			[Csv.Index(16)]
			public string MinimumRating { get; set; }
		}

		private Response GetLocalMarketBuys(string planet)
		{
			planet = planet.ToUpper();

			var buyAds = new List<CsvLMBuySell>();

			using (var DB = PRUNDataContext.GetNewContext())
			{
				var res = DB.BuyingAds
					.AsNoTracking()
					.Where(a => a.PlanetId.ToUpper() == planet || a.PlanetNaturalId.ToUpper() == planet || a.PlanetName.ToUpper() == planet)
					.ToList();

				foreach (var r in res)
				{
					var buy = new CsvLMBuySell();
					buy.ContractNaturalId = r.ContractNaturalId;
					buy.PlanetNaturalId = r.PlanetNaturalId;
					buy.PlanetName = r.PlanetName;
					buy.CreatorCompanyName = r.CreatorCompanyName;
					buy.CreatorCompanyCode = r.CreatorCompanyCode;
					buy.MaterialName = r.MaterialName;
					buy.MaterialTicker = r.MaterialTicker;
					buy.MaterialCategory = r.MaterialCategory;
					buy.MaterialWeight = r.MaterialWeight;
					buy.MaterialVolume = r.MaterialVolume;
					buy.MaterialAmount = r.MaterialAmount;
					buy.Price = r.Price;
					buy.PriceCurrency = r.PriceCurrency;
					buy.DeliveryTime = r.DeliveryTime;
					buy.CreationTimeEpochMs = r.CreationTimeEpochMs;
					buy.ExpiryTimeEpochMs = r.ExpiryTimeEpochMs;
					buy.MinimumRating = r.MinimumRating;

					buyAds.Add(buy);
				}
			}

			return buyAds.GetCSVResponse();
		}

		private Response GetLocalMarketSells(string planet)
		{
			planet = planet.ToUpper();

			var sellAds = new List<CsvLMBuySell>();

			using (var DB = PRUNDataContext.GetNewContext())
			{
				var res = DB.SellingAds
					.AsNoTracking()
					.Where(a => a.PlanetId.ToUpper() == planet || a.PlanetNaturalId.ToUpper() == planet || a.PlanetName.ToUpper() == planet)
					.ToList();

				foreach (var r in res)
				{
					var sell = new CsvLMBuySell();
					sell.ContractNaturalId = r.ContractNaturalId;
					sell.PlanetNaturalId = r.PlanetNaturalId;
					sell.PlanetName = r.PlanetName;
					sell.CreatorCompanyName = r.CreatorCompanyName;
					sell.CreatorCompanyCode = r.CreatorCompanyCode;
					sell.MaterialName = r.MaterialName;
					sell.MaterialTicker = r.MaterialTicker;
					sell.MaterialCategory = r.MaterialCategory;
					sell.MaterialWeight = r.MaterialWeight;
					sell.MaterialVolume = r.MaterialVolume;
					sell.MaterialAmount = r.MaterialAmount;
					sell.Price = r.Price;
					sell.PriceCurrency = r.PriceCurrency;
					sell.DeliveryTime = r.DeliveryTime;
					sell.CreationTimeEpochMs = r.CreationTimeEpochMs;
					sell.ExpiryTimeEpochMs = r.ExpiryTimeEpochMs;
					sell.MinimumRating = r.MinimumRating;

					sellAds.Add(sell);
				}
			}

			return sellAds.GetCSVResponse();
		}

		public class CsvLMShipment
		{
			[Csv.Index(0)]
			public int ContractNaturalId { get; set; }

			[Csv.Index(1)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(2)]
			public string PlanetName { get; set; }

			[Csv.Index(3)]
			public string OriginPlanetNaturalId { get; set; }

			[Csv.Index(4)]
			public string OriginPlanetName { get; set; }

			[Csv.Index(5)]
			public string DestinationPlanetNaturalId { get; set; }

			[Csv.Index(6)]
			public string DestinationPlanetName { get; set; }

			[Csv.Index(7)]
			public double CargoWeight { get; set; }

			[Csv.Index(8)]
			public double CargoVolume { get; set; }

			[Csv.Index(9)]
			public string CreatorCompanyName { get; set; }

			[Csv.Index(10)]
			public string CreatorCompanyCode { get; set; }

			[Csv.Index(11)]
			public double PayoutPrice { get; set; }

			[Csv.Index(12)]
			public string PayoutCurrency { get; set; }

			[Csv.Index(13)]
			public int DeliveryTime { get; set; }

			[Csv.Index(14)]
			public long CreationTimeEpochMs { get; set; }

			[Csv.Index(15)]
			public long ExpiryTimeEpochMs { get; set; }

			[Csv.Index(16)]
			public string MinimumRating { get; set; }
		}

		private Response GetLocalMarketShipments(string planet)
		{
			planet = planet.ToUpper();

			var shippingAds = new List<CsvLMShipment>();

			using (var DB = PRUNDataContext.GetNewContext())
			{
				var res = DB.ShippingAds
					.AsNoTracking()
					.Where(a => a.PlanetId.ToUpper() == planet || a.PlanetNaturalId.ToUpper() == planet || a.PlanetName.ToUpper() == planet)
					.ToList();

				foreach (var r in res)
				{
					var ship = new CsvLMShipment();
					ship.ContractNaturalId = r.ContractNaturalId;
					ship.PlanetNaturalId = r.PlanetNaturalId;
					ship.PlanetName = r.PlanetName;
					ship.OriginPlanetNaturalId = r.OriginPlanetNaturalId;
					ship.OriginPlanetName = r.OriginPlanetName;
					ship.DestinationPlanetNaturalId = r.DestinationPlanetNaturalId;
					ship.DestinationPlanetName = r.DestinationPlanetName;
					ship.CargoWeight = r.CargoWeight;
					ship.CargoVolume = r.CargoVolume;
					ship.CreatorCompanyName = r.CreatorCompanyName;
					ship.CreatorCompanyCode = r.CreatorCompanyCode;
					ship.PayoutPrice = r.PayoutPrice;
					ship.PayoutCurrency = r.PayoutCurrency;
					ship.DeliveryTime = r.DeliveryTime;
					ship.CreationTimeEpochMs = r.CreationTimeEpochMs;
					ship.ExpiryTimeEpochMs = r.ExpiryTimeEpochMs;
					ship.MinimumRating = r.MinimumRating;

					shippingAds.Add(ship);
				}
			}

			return shippingAds.GetCSVResponse();
		}

		public class CsvCXPC
		{
			[Csv.Index(0)]
			public long TimeEpochMs { get; set; }

			[Csv.Index(1)]
			public double Open { get; set; }

			[Csv.Index(2)]
			public double Close { get; set; }

			[Csv.Index(3)]
			public double Volume { get; set; }

			[Csv.Index(4)]
			public int Traded { get; set; }
		}

		private Response GetCXPC(string materialTicker)
		{
			materialTicker = materialTicker.ToUpper();
			var parts = materialTicker.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
			if (parts.Length != 2)
			{
				return HttpStatusCode.BadRequest;
			}

			var mat = parts[0];
			var code = parts[1];

			using (var DB = PRUNDataContext.GetNewContext())
			{
				var res = DB.CXPCData
					.AsNoTracking()
					.Where(cxpc => cxpc.MaterialTicker == mat && cxpc.ExchangeCode == code)
					.SelectMany(cxpc => cxpc.Entries)
					.OrderByDescending(e => e.TimeEpochMs)
					.Select(e => new CsvCXPC
					{
						TimeEpochMs = e.TimeEpochMs,
						Open = e.Open,
						Close = e.Close,
						Volume = e.Volume,
						Traded = e.Traded
					})
					.AsSplitQuery()
					.ToList();

				return res.GetCSVResponse();
			}
		}

		private bool ParseParameters(DynamicDictionary query, out List<string> usernames, out string apikey)
		{
			usernames = new List<string>();
			apikey = null;

			if (query.ContainsKey("apikey"))
			{
				apikey = query["apikey"].ToString().ToUpper();
				if (!Guid.TryParse(apikey, out Guid temp))
				{
					apikey = null;
				}
			}
			else
			{
				return false;
			}

			if (query.ContainsKey("username"))
			{
				string username = query["username"].ToString().ToUpper();
				usernames.Add(username);
			}

			if (query.ContainsKey("group"))
			{
				string groupStr = query["group"].ToString().ToUpper();
				if (int.TryParse(groupStr, out int groupid))
				{
					using (var DB = PRUNDataContext.GetNewContext())
					{
						var group = DB.GroupModels.FirstOrDefault(gm => gm.GroupModelId == groupid);
						if (group != null)
						{
							var groupUsernames = group.GroupUsers.Select(gu => gu.GroupUserName).ToList();
							usernames.AddRange(groupUsernames);
						}
					}
				}
			}

			return (!String.IsNullOrEmpty(apikey) && usernames.Count > 0);
		}

		public class CsvUserInventory
		{
			[Csv.Index(0)]
			public string Username { get; set; }

			[Csv.Index(1)]
			public string NaturalId { get; set; }

			[Csv.Index(2)]
			public string Name { get; set; }

			[Csv.Index(3)]
			public string StorageType { get; set; }

			[Csv.Index(4)]
			public string Ticker { get; set; }

			[Csv.Index(5)]
			public int Amount { get; set; }
		}

		private Response GetInventory(DynamicDictionary query)
		{
			if (ParseParameters(query, out List<string> usernames, out string apikey))
			{
				var requestingUsername = Auth.GetUserNameFromAPIKey(apikey);
				usernames = usernames
					.Where(u => Auth.CanSeeData(requestingUsername, u, Auth.PrivacyType.Storage))
					.ToList();
				if (usernames.Count == 0)
				{
					return HttpStatusCode.Unauthorized;
				}

				var allStorage = new List<CsvUserInventory>();

				using (var DB = PRUNDataContext.GetNewContext())
				{
					foreach (var username in usernames)
					{
						var model = DB.Storages
							.AsNoTracking()
							.Where(s => s.UserNameSubmitted.ToUpper() == username && s.StorageId != null)
							.Include(sm => sm.StorageItems)
							.ToList();
						List<string> storageNames = model
							.FindAll(sm => sm.Type != "STORE" && sm.Name != null)
							.Select(sm => sm.Name.ToUpper())
							.ToList();
						var ships = DB.Ships
							.AsNoTracking()
							.Where(ss => storageNames.Contains(ss.Registration.ToUpper()))
							.ToList();
						var warehouses = DB.Warehouses
							.AsNoTracking()
							.Where(w => w.UserNameSubmitted.ToUpper() == username)
							.ToList();

						foreach (var storageModel in model)
						{
							var site = DB.Sites
								.AsNoTracking()
								.Include(s => s.Buildings)
									.ThenInclude(b => b.ReclaimableMaterials)
								.Include(s => s.Buildings)
									.ThenInclude(b => b.RepairMaterials)
								.Where(site => site.SiteId == storageModel.AddressableId)
								.AsSplitQuery()
								.FirstOrDefault();
							if (site != null)
							{
								foreach (var item in storageModel.StorageItems)
								{
									if (item.Type == "SHIPMENT" || item.Type == "BLOCKED")
										continue;

									var s = new CsvUserInventory();
									s.Username = username;
									s.NaturalId = site.PlanetIdentifier;
									s.Name = site.PlanetName;
									s.StorageType = storageModel.Type;
									s.Ticker = item.MaterialTicker;
									s.Amount = item.MaterialAmount;

									allStorage.Add(s);
								}
							}

							var war = DB.Warehouses
								.AsNoTracking()
								.Where(w => w.WarehouseId.StartsWith(storageModel.AddressableId) && w.UserNameSubmitted == storageModel.UserNameSubmitted)
								.FirstOrDefault();
							if (war != null)
                            {
								foreach (var item in storageModel.StorageItems)
								{
									if (item.Type == "SHIPMENT" || item.Type == "BLOCKED")
										continue;

									var s = new CsvUserInventory();
									s.Username = username;
									s.NaturalId = war.LocationNaturalId;
									s.Name = war.LocationName;
									s.StorageType = storageModel.Type;
									s.Ticker = item.MaterialTicker;
									s.Amount = item.MaterialAmount;

									allStorage.Add(s);
								}
							}

							if (storageModel.Type != "STORE")
							{
								foreach (var item in storageModel.StorageItems)
								{
									if (item.Type == "SHIPMENT" || item.Type == "BLOCKED" || storageModel.Name == null)
										continue;

									var s = new CsvUserInventory();
									s.Username = username;
									s.NaturalId = storageModel.Name;
									var ship = ships.Find(s => s.Registration.ToUpper() == storageModel.Name.ToUpper());
									if (ship != null && ship.Name != null)
									{
										s.Name = ship.Name;
									}
									s.StorageType = storageModel.Type;
									s.Ticker = item.MaterialTicker;
									s.Amount = item.MaterialAmount;

									allStorage.Add(s);
								}
							}
						}
					}
				}

				return allStorage
					.OrderBy(s => s.Username)
						.ThenBy(s => s.NaturalId)
							.ThenBy(s => s.Ticker)
					.ToList()
					.GetCSVResponse();
			}

			return HttpStatusCode.BadRequest;
		}

		public class CsvUserBurnRate
		{
			[Csv.Index(0)]
			public string Username { get; set; }

			[Csv.Index(1)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(2)]
			public string PlanetName { get; set; }

			[Csv.Index(3)]
			public string Ticker { get; set; }

			[Csv.Index(4)]
			public double DailyConsumption { get; set; }

			[Csv.Index(5)]
			public bool Essential { get; set; }
		}

		private Response GetBurnRate(DynamicDictionary query)
		{
			if (ParseParameters(query, out List<string> usernames, out string apikey))
			{
				var requestingUsername = Auth.GetUserNameFromAPIKey(apikey);
				usernames = usernames
					.Where(u => Auth.CanSeeData(requestingUsername, u, Auth.PrivacyType.Workforce))
					.ToList();
				if (usernames.Count == 0)
				{
					return HttpStatusCode.Unauthorized;
				}

				var burnRates = new List<CsvUserBurnRate>();
				using (var DB = PRUNDataContext.GetNewContext())
				{
					foreach (var username in usernames)
					{
						var models = DB.Workforces
							.AsNoTracking()
							.Include(w => w.Workforces)
								.ThenInclude(w => w.WorkforceNeeds)
							.Where(s => s.UserNameSubmitted.ToUpper() == username)
							.AsSplitQuery()
							.ToList();

						foreach (var model in models)
						{
							foreach (var workforce in model.Workforces)
							{
								var workforceNeeds = workforce.WorkforceNeeds
									.Where(wn => wn.UnitsPerInterval > 0.0)
									.Select(wn => new CsvUserBurnRate
									{
										Username = username,
										PlanetNaturalId = model.PlanetNaturalId,
										PlanetName = model.PlanetName,
										Ticker = wn.MaterialTicker,
										DailyConsumption = wn.UnitsPerInterval.RoundToDecimalPlaces(4),
                                        Essential = wn.Essential
									}).ToList();

								burnRates.AddRange(workforceNeeds);
							}
						}
					}
				}

				return burnRates
					.OrderBy(br => br.Username)
						.ThenBy(br => br.PlanetNaturalId)
							.ThenBy(br => br.Ticker)
					.ToList()
					.GetCSVResponse();
			}

			return HttpStatusCode.BadRequest;
		}

		public class CsvUserSitesData
		{
			[Csv.Index(0)]
			public string Username { get; set; }

			[Csv.Index(1)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(2)]
			public string PlanetName { get; set; }

            [Csv.Index(3)]
			public string BuildingId { get; set; }

			[Csv.Index(4)]
			public string BuildingTicker { get; set; }

			[Csv.Index(5)]
			public double BuildingCondition { get; set; }

			[Csv.Index(6)]
			public long BuildingLastRepairEpochMs { get; set; }

			[Csv.Index(7)]
			public double BuildingEfficiency { get; set; }
		}

		private Response GetSites(DynamicDictionary query)
		{
			if (ParseParameters(query, out List<string> usernames, out string apikey))
			{
				var requestingUsername = Auth.GetUserNameFromAPIKey(apikey);
				usernames = usernames
					.Where(u => Auth.CanSeeData(requestingUsername, u, Auth.PrivacyType.Building))
					.ToList();
				if (usernames.Count == 0)
				{
					return HttpStatusCode.Unauthorized;
				}

				var csvSites = new List<CsvUserSitesData>();
				using (var DB = PRUNDataContext.GetNewContext())
				{
					foreach (var username in usernames)
					{
						var sites = DB.Sites
							.AsNoTracking()
							.Include(s => s.Buildings)
								.ThenInclude(b => b.ReclaimableMaterials)
							.Include(s => s.Buildings)
								.ThenInclude(b => b.RepairMaterials)
							.Where(s => s.UserNameSubmitted.ToUpper() == username)
							.AsSplitQuery()
							.ToList();

						var prodInfos = DB.ProductionLines
							.AsNoTracking()
							.Where(pl => pl.UserNameSubmitted.ToUpper() == username)
							.Select(pl => new
							{
								SiteId = pl.SiteId,
								BuildingName = pl.Type,
								Efficiency = pl.Efficiency
							})
							.ToList();

						foreach (var site in sites)
						{
							var PlanetNaturalId = site.PlanetIdentifier;
							var PlanetName = site.PlanetName;

							var buildingSites = site.Buildings.Select(s => new CsvUserSitesData
							{
								Username = username,
								PlanetNaturalId = PlanetNaturalId,
								PlanetName = PlanetName,
								BuildingId = s.BuildingId.ToUpper().Substring(0,8),
								BuildingTicker = s.BuildingTicker,
								BuildingCondition = s.Condition,
								BuildingLastRepairEpochMs = s.BuildingLastRepair != null ? (long)s.BuildingLastRepair : 0,
								BuildingEfficiency = prodInfos.FirstOrDefault(pi => pi.SiteId == site.SiteId && pi.BuildingName == s.BuildingName) != null ? prodInfos.FirstOrDefault(pi => pi.SiteId == site.SiteId && pi.BuildingName == s.BuildingName).Efficiency : -1.0
							});

							csvSites.AddRange(buildingSites);
						}
					}
				}

				return csvSites
					.OrderBy(s => s.Username)
						.ThenBy(s => s.PlanetNaturalId)
							.ThenBy(s => s.BuildingTicker)
					.ToList()
					.GetCSVResponse();
			}

			return HttpStatusCode.BadRequest;
		}

		public class CsvRepairReclaimables
        {
            [Csv.Index(0)]
			public string UserName { get; set; }

            [Csv.Index(1)]
			public string PlanetNaturalId { get; set; }

            [Csv.Index(2)]
			public string PlanetName { get; set; }

            [Csv.Index(3)]
			public string BuildingId { get; set; }

            [Csv.Index(4)]
			public string BuildingTicker { get; set; }

            [Csv.Index(5)]
			public string Material { get; set; }

            [Csv.Index(6)]
			public int Amount { get; set; }
        }
		
		private Response GetReclaimables(DynamicDictionary query)
        {
			if (ParseParameters(query, out List<string> usernames, out string apikey))
            {
				var requestingUsername = Auth.GetUserNameFromAPIKey(apikey);
				usernames = usernames.Where(u => Auth.CanSeeData(requestingUsername, u, Auth.PrivacyType.Building)).ToList();
				if (usernames.Count == 0)
				{
					return HttpStatusCode.Unauthorized;
				}

				var reclaimables = new List<CsvRepairReclaimables>();
				using (var DB = PRUNDataContext.GetNewContext())
                {
					var sites = DB.Sites
						.AsNoTracking()
						.Where(s => usernames.Contains(s.UserNameSubmitted))
						.Include(s => s.Buildings)
							.ThenInclude(b => b.ReclaimableMaterials)
						.Include(s => s.Buildings)
							.ThenInclude(b => b.RepairMaterials)
						.OrderBy(s => s.UserNameSubmitted)
							.ThenBy(s => s.PlanetIdentifier)
						.AsSplitQuery()
						.ToList();

					foreach (var site in sites)
                    {
						var buildings = site.Buildings
							.OrderBy(b => b.BuildingTicker)
								.ThenBy(b => b.BuildingId)
							.ToList();
						foreach (var building in buildings)
						{
							var buildingReclaimables = building.ReclaimableMaterials
								.OrderBy(rm => rm.MaterialTicker)
								.Select(rm => new CsvRepairReclaimables
								{
									UserName = site.UserNameSubmitted,
									PlanetNaturalId = site.PlanetIdentifier,
									PlanetName = site.PlanetName,
									BuildingId = building.BuildingId.ToUpper().Substring(0, 8),
									BuildingTicker = building.BuildingTicker,
									Material = rm.MaterialTicker,
									Amount = rm.MaterialAmount
								})
								.ToList();

							reclaimables.AddRange(buildingReclaimables);
						}
                    }
                }

				return reclaimables.GetCSVResponse();
			}

			return HttpStatusCode.BadRequest;
		}

		private Response GetRepairables(DynamicDictionary query)
		{
			if (ParseParameters(query, out List<string> usernames, out string apikey))
			{
				var requestingUsername = Auth.GetUserNameFromAPIKey(apikey);
				usernames = usernames.Where(u => Auth.CanSeeData(requestingUsername, u, Auth.PrivacyType.Building)).ToList();
				if (usernames.Count == 0)
				{
					return HttpStatusCode.Unauthorized;
				}

				var repairables = new List<CsvRepairReclaimables>();
				using (var DB = PRUNDataContext.GetNewContext())
				{
					var sites = DB.Sites
						.AsNoTracking()
						.Where(s => usernames.Contains(s.UserNameSubmitted))
						.Include(s => s.Buildings)
							.ThenInclude(b => b.ReclaimableMaterials)
						.Include(s => s.Buildings)
							.ThenInclude(b => b.RepairMaterials)
						.OrderBy(s => s.UserNameSubmitted)
							.ThenBy(s => s.PlanetIdentifier)
						.AsSplitQuery()
						.ToList();

					foreach (var site in sites)
					{
						var buildings = site.Buildings
							.OrderBy(b => b.BuildingTicker)
								.ThenBy(b => b.BuildingId)
							.ToList();
						foreach (var building in buildings)
						{
							var buildingReclaimables = building.RepairMaterials
								.OrderBy(rm => rm.MaterialTicker)
								.Select(rm => new CsvRepairReclaimables
								{
									UserName = site.UserNameSubmitted,
									PlanetNaturalId = site.PlanetIdentifier,
									PlanetName = site.PlanetName,
									BuildingId = building.BuildingId.ToUpper().Substring(0, 8),
									BuildingTicker = building.BuildingTicker,
									Material = rm.MaterialTicker,
									Amount = rm.MaterialAmount
								})
								.ToList();

							repairables.AddRange(buildingReclaimables);
						}
					}
				}

				return repairables.GetCSVResponse();
			}

			return HttpStatusCode.BadRequest;
		}

		public class CsvWorkforceData
		{
			[Csv.Index(0)]
			public string Username { get; set; }

			[Csv.Index(1)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(2)]
			public string PlanetName { get; set; }

			[Csv.Index(3)]
			public string MaterialTicker { get; set; }

			[Csv.Index(4)]
			public double DailyAmount { get; set; }
		}

		private Response GetWorkforce(DynamicDictionary query)
		{
			if (ParseParameters(query, out List<string> usernames, out string apikey))
			{
				var requestingUsername = Auth.GetUserNameFromAPIKey(apikey);
				usernames = usernames.Where(u => Auth.CanSeeData(requestingUsername, u, Auth.PrivacyType.Building)).ToList();
				if (usernames.Count == 0)
				{
					return HttpStatusCode.Unauthorized;
				}

				var workforceData = new List<CsvWorkforceData>();
				using (var DB = PRUNDataContext.GetNewContext())
				{
					var workforces = DB.Workforces
						.AsNoTracking()
						.Include(wm => wm.Workforces)
							.ThenInclude(w => w.WorkforceNeeds)
						.Where(wm => usernames.Contains(wm.UserNameSubmitted.ToUpper()))
						.AsSplitQuery()
						.ToList();

					foreach (var workforce in workforces)
					{
						var workforceNeeds = workforce.Workforces
							.SelectMany(w => w.WorkforceNeeds)
							.GroupBy(wn => wn.MaterialTicker)
							.Select(g => new CsvWorkforceData
							{
								Username = workforce.UserNameSubmitted,
								PlanetNaturalId = workforce.PlanetNaturalId,
								PlanetName = workforce.PlanetName,
								MaterialTicker = g.First().MaterialTicker,
								DailyAmount = g.Sum(s => s.UnitsPerInterval)
							})
							.Where(wd => wd.DailyAmount > 0.0)
							.ToList();
						workforceData.AddRange(workforceNeeds);
					}
				}

				return workforceData
					.OrderBy(s => s.Username)
						.ThenBy(s => s.PlanetNaturalId)
							.ThenBy(s => s.MaterialTicker)
					.ToList()
					.GetCSVResponse();
			}

			return HttpStatusCode.BadRequest;
		}

		public class CsvCXOSOrder
		{
			[Csv.Index(0)]
			public string Username { get; set; }

			[Csv.Index(1)]
			public string OrderId { get; set; }

			[Csv.Index(2)]
			public string ExchangeCode { get; set; }

			[Csv.Index(3)]
			public string OrderType { get; set; }

			[Csv.Index(4)]
			public string MaterialTicker { get; set; }

			[Csv.Index(5)]
			public int Amount { get; set; }

			[Csv.Index(6)]
			public int InitialAmount { get; set; }

			[Csv.Index(7)]
			public double Limit { get; set; }

			[Csv.Index(8)]
			public string Currency { get; set; }

			[Csv.Index(9)]
			public string Status { get; set; }

			[Csv.Index(10)]
			public long CreatedEpochMs { get; set; }
		}

		private Response GetCXOS(DynamicDictionary query)
		{
			if (ParseParameters(query, out List<string> usernames, out string apikey))
			{
				var requestingUsername = Auth.GetUserNameFromAPIKey(apikey);
				usernames = usernames
					.Where(u => Auth.CanSeeData(requestingUsername, u, Auth.PrivacyType.Building))
					.ToList();
				if (usernames.Count == 0)
				{
					return HttpStatusCode.Unauthorized;
				}

				var csvOrders = new List<CsvCXOSOrder>();
				using (var DB = PRUNDataContext.GetNewContext())
				{
					var cxosOrders = DB.CXOSTradeOrders
						.AsNoTracking()
						.Where(to => usernames.Contains(to.UserNameSubmitted.ToUpper()))
						.Include(to => to.Trades)
						.OrderBy(to => to.UserNameSubmitted)
							.ThenBy(to => to.CreatedEpochMs)
						.AsSplitQuery()
						.ToList();

					foreach(var cxosOrder in cxosOrders)
                    {
						csvOrders.Add(new CsvCXOSOrder
						{
							Username = cxosOrder.UserNameSubmitted,
							OrderId = $"ORDER-{cxosOrder.MaterialTicker}.{cxosOrder.ExchangeCode}-{cxosOrder.CreatedEpochMs}",
							ExchangeCode = cxosOrder.ExchangeCode,
							OrderType = cxosOrder.OrderType,
							MaterialTicker = cxosOrder.MaterialTicker,
							Amount = cxosOrder.Amount,
							InitialAmount = cxosOrder.InitialAmount,
							Limit = cxosOrder.Limit,
							Currency = cxosOrder.LimitCurrency,
							Status = cxosOrder.Status,
							CreatedEpochMs = cxosOrder.CreatedEpochMs,
						});

						var trades = cxosOrder.Trades
							.Select(trade => new CsvCXOSOrder
							{
								Username = cxosOrder.UserNameSubmitted,
								OrderId = trade.CXOSTradeId,
								ExchangeCode = cxosOrder.ExchangeCode,
								OrderType = cxosOrder.OrderType,
								MaterialTicker = cxosOrder.MaterialTicker,
								Amount = trade.Amount,
								InitialAmount = cxosOrder.InitialAmount,
								Limit = cxosOrder.Limit,
								Currency = cxosOrder.LimitCurrency,
								Status = cxosOrder.Status,
								CreatedEpochMs = cxosOrder.CreatedEpochMs
							})
							.ToList();

						csvOrders.AddRange(trades);
                    }
				}

				return csvOrders.GetCSVResponse();
			}

			return HttpStatusCode.BadRequest;
		}

		public class CsvBurn
		{
			[Csv.Index(0)]
			public string Username { get; set; }

			[Csv.Index(1)]
			public string PlanetNaturalId { get; set; }

			[Csv.Index(2)]
			public string PlanetName { get; set; }

			[Csv.Index(3)]
			public string MaterialTicker { get; set; }

			[Csv.Index(4)]
			public double DailyWorkforceConsumption { get; set; }

			[Csv.Index(5)]
			public double DailyOrderConsumption { get; set; }

			[Csv.Index(6)]
			public double DailyOrderProduction { get; set; }
		}

		private Response GetBurn(DynamicDictionary query)
		{
			if (ParseParameters(query, out List<string> usernames, out string apikey))
			{
				var requestingUsername = Auth.GetUserNameFromAPIKey(apikey);
				usernames = usernames
					.Where(u => Auth.CanSeeData(requestingUsername, u, Auth.PrivacyType.Building))
					.ToList();
				if (usernames.Count == 0)
				{
					return HttpStatusCode.Unauthorized;
				}

				var results = FIOWeb.Burn.BurnModule.GetBurnForListOfUserNames(requestingUsername, usernames);
				if (results == null)
				{
					return HttpStatusCode.Unauthorized;
				}

				var burnResults = new List<CsvBurn>();
				foreach (var result in results)
				{
					// Get a list of tickers
					var workforceConsumptionTickers = result.WorkforceConsumption.Select(wc => wc.MaterialTicker);
					var orderConsumptionTickers = result.OrderConsumption.Select(oc => oc.MaterialTicker);
					var orderProductionTickers = result.OrderProduction.Select(op => op.MaterialTicker);

					// Union them all
					var allTickers = workforceConsumptionTickers.Union(orderConsumptionTickers.Union(orderProductionTickers)).ToList();
					allTickers.Sort();

					foreach (var ticker in allTickers)
					{
						var burnEntry = new CsvBurn();
						burnEntry.Username = result.UserName;
						burnEntry.PlanetNaturalId = result.PlanetNaturalId;
						burnEntry.PlanetName = result.PlanetName;
						burnEntry.MaterialTicker = ticker;

						var wc = result.WorkforceConsumption.FirstOrDefault(wc => wc.MaterialTicker == ticker);
						if (wc != null)
						{
							burnEntry.DailyWorkforceConsumption = wc.DailyAmount;
						}

						var oc = result.OrderConsumption.FirstOrDefault(oc => oc.MaterialTicker == ticker);
						if (oc != null)
						{
							burnEntry.DailyOrderConsumption = oc.DailyAmount;
						}

						var op = result.OrderProduction.FirstOrDefault(op => op.MaterialTicker == ticker);
						if (op != null)
						{
							burnEntry.DailyOrderProduction = op.DailyAmount;
						}

						burnResults.Add(burnEntry);
					}
				}

				return burnResults.GetCSVResponse();
			}

			return HttpStatusCode.BadRequest;
		}

		public class CsvBalance
		{
			[Csv.Index(0)]
			public string Username { get; set; }

			[Csv.Index(1)]
			public string Currency { get; set; }

			[Csv.Index(2)]
			public double Amount { get; set; }
		}

		private Response GetBalances(DynamicDictionary query)
        {
			if (ParseParameters(query, out List<string> usernames, out string apikey))
			{
				var requestingUsername = Auth.GetUserNameFromAPIKey(apikey);
				usernames = usernames
					.Where(u => Auth.CanSeeData(requestingUsername, u, Auth.PrivacyType.Contracts))
					.ToList();
				if (usernames.Count == 0)
				{
					return HttpStatusCode.Unauthorized;
				}

				var balances = new List<CsvBalance>();
				using (var DB = PRUNDataContext.GetNewContext())
				{
					var allUserData = DB.UserData
						.AsNoTracking()
						.Include(ud => ud.Balances)
						.Where(ud => usernames.Contains(ud.UserName.ToUpper()))
						.OrderBy(ud => ud.UserName)
						.ToList();

					foreach (var userData in allUserData)
                    {
						foreach(var balance in userData.Balances.OrderBy(b => b.Currency))
                        {
							balances.Add(new CsvBalance
							{
								Username = userData.UserName,
								Currency = balance.Currency,
								Amount = balance.Amount
							});
                        }
                    }
				}

				return balances.GetCSVResponse();
			}

			return HttpStatusCode.BadRequest;
		}
	}
}
#endif // WITH_MODULES